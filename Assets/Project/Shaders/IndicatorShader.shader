﻿Shader "Unlit/IndicatorShader"
{
	//https://github.com/vux427/ForceFieldFX/blob/master/ForceFieldFX/Assets/Shader/ShieldFX.shader

	Properties
	{
		//_MainTex ("Texture", 2D) = "white" {}
		_Color("Color",Color) = (0.0,0.0,0.0,1.0)
		_Fresnel("Fresnel Intensity", Range(0,200)) = 3.0
		_FresnelWidth("Fresnel Width", Range(0,2)) = 3.0
		_IntersectionThreshold("Highlight of intersection threshold", range(0,1)) = .1 //Max difference for intersections
	}
		SubShader
	{
		Tags{ "Queue" = "Overlay" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		// show on top
		ZTest Always
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		LOD 100

		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
		// make fog work
#pragma multi_compile_fog

#include "UnityCG.cginc"

		struct appdata
	{
		float4 vertex : POSITION;
		fixed4 normal : NORMAL;
		//float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		//float2 uv : TEXCOORD0;
		UNITY_FOG_COORDS(1)
			float4 vertex : SV_POSITION;
		fixed3 rimColor : TEXCOORD1;
		fixed4 screenPos : TEXCOORD2;
	};

	//sampler2D _MainTex;
	//float4 _MainTex_ST;

	sampler2D _CameraDepthTexture;
	float4 _Color;
	fixed _Fresnel, _FresnelWidth, _IntersectionThreshold;

	v2f vert(appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
		UNITY_TRANSFER_FOG(o,o.vertex);

		//fresnel 
		fixed3 viewDir = normalize(ObjSpaceViewDir(v.vertex));
		fixed dotProduct = 1 - saturate(dot(v.normal, viewDir));
		o.rimColor = smoothstep(1 - _FresnelWidth, 1.0, dotProduct) * .5f;
		o.screenPos = ComputeScreenPos(o.vertex);
		COMPUTE_EYEDEPTH(o.screenPos.z);//eye space depth of the vertex

		return o;
	}



	fixed4 frag(v2f i, fixed face : VFACE) : SV_Target
	{
		// sample the texture
		//fixed4 col = tex2D(_MainTex, i.uv);
		fixed4 col = _Color;
	fixed3 color = _Color.rgb;

	// apply fog
	UNITY_APPLY_FOG(i.fogCoord, col);

	//intersection
	fixed intersect = saturate((abs(LinearEyeDepth(tex2Dproj(_CameraDepthTexture, i.screenPos).r) - i.screenPos.z)) / _IntersectionThreshold);

	//intersect hightlight
	i.rimColor *= intersect * clamp(0, 1, face);
	//i.rimColor *= clamp(0, 1, face);
	color *= _Color * pow(_Fresnel, i.rimColor);

	//lerp distort color & fresnel color
	//main = lerp(distortColor, main, i.rimColor.r);
	color += (1 - intersect) * (face > 0 ? .03 : .3) * _Color * _Fresnel;

	return fixed4(color, col.a);
	}
		ENDCG
	}
	}
}
