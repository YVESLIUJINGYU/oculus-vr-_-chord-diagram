﻿Shader "Unlit/InsideOutTunnelingShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color",Color) = (0.0,0.0,0.0,1.0)
		_Intensity ("Tunneling Intensity", Range(0,1)) = 0
	}
		SubShader
	{
		Tags{ "Queue" = "Overlay" "RenderType" = "Transparent" }

		// No culling or depth
		Cull Front  ZTest Always

		CGPROGRAM
		 
		#pragma surface surf Lambert alpha vertex:vert
		sampler2D _MainTex;
		fixed4 _Color;
		float _Intensity;

		void vert(inout appdata_full v)
		{
			v.normal.xyz = v.normal * -1;
		}


		struct Input {
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			
			half4 c = tex2D(_MainTex, IN.uv_MainTex);

			o.Albedo = _Color;

			//0.5是起始变化的alpha值 0.5-0.37的大小决定了最终可视范围的大小
			float threshold = 0.5 - _Intensity * 0.37;

			if (c.a >= threshold)
			{
				o.Alpha = 1;
			}
			//0.1决定了从黑到透明 渐变的宽度
			else if (c.a<threshold && c.a>threshold - 0.1)
			{
				o.Alpha = (c.a - threshold + 0.1) / 0.1;
			}
			else {

				o.Alpha = 0;
			}

		}

		ENDCG

	}
		Fallback "Diffuse"

}