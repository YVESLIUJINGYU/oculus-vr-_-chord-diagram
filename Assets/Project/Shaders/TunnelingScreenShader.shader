﻿Shader "Unlit/TunnelingScreenShader"
{
	Properties
	{
		_Color("Color",Color) = (0.0,0.0,0.0,1.0)
		_Intensity("Tunneling Intensity", Range(0,1)) = 0
	}
		SubShader
	{
		Tags{ "Queue" = "Overlay" "RenderType" = "Transparent" }

		// No culling or depth
		Cull Back ZWrite On ZTest Always

		CGPROGRAM

#pragma surface surf Lambert alpha
		fixed4 _Color;
	float _Intensity;



	struct Input {
		float2 uv_MainTex;
		float3 worldPos;
	};

	void surf(Input IN, inout SurfaceOutput o) {

		o.Albedo = _Color.rgb;
		o.Alpha = 0;


	}

	ENDCG

	}
		Fallback "Diffuse"

}