﻿Shader "Unlit/InsideOutShader"
{

	Properties{
		_Color("Color",Color) = (1.0,1.0,1.0,1.0)
	}

	SubShader{

		Tags{ "Queue" = "Overlay" "RenderType" = "Transparent" }

		Cull Front

		ZTest Always

		CGPROGRAM

#pragma surface surf Lambert alpha vertex:vert
		fixed4 _Color;

		void vert(inout appdata_full v)
	{
		v.normal.xyz = v.normal * -1;
	}

	
	struct Input {
		float2 uv_MainTex;
	};

	void surf(Input IN, inout SurfaceOutput o) {
		o.Albedo = _Color.rgb;
		o.Alpha = _Color.a;
	}

	ENDCG

	}

		Fallback "Diffuse"

}
