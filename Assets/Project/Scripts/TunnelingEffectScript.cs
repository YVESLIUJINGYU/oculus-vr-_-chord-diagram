﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TunnelingEffectScript : MonoBehaviour {

    public float intensity = 0;

    private Material material;
    

	// Use this for initialization
	void Start () {

        this.material = new Material(Shader.Find("Hidden/TunnelingShader"));

	}
	
	// Update is called once per frame
	void Update () {

        

	}

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (this.intensity == 0)
        {
            Graphics.Blit(source, destination);
            return;
        }

        this.material.SetFloat("_intensity", this.intensity);
        Graphics.Blit(source,destination,material);
    }

}
