﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class VehicleBase : MonoBehaviour {

    [Header("Base Vehicle Setup")]
    public bool isUsingGravity = true;
    public bool shouldAccelerate = false;//should vehicle be able to accelerate
    [Range(0.1f, 0.9f)]
    public float thumbstickSensitiveThreshold = 0.3f;//thumbstick threshold

    [Header("Steer")]
    public bool shouldSteer = false;//should vehicle have the ability to turn around
    public float SteerSpeed = 0f;
    public float SteerPower = 0f;
    public float steerSensitiveThreshold = 0.8f;//thumbstick threshold

    [Header("Vehicle Speed/Power")]
    public float speed2D = 0f;
    public float speedY = 0f;
    public float power2D = 0f;
    public float powerY = 0f;

    [HideInInspector]
    public Vector2 currentAxis = Vector2.zero;
    [HideInInspector]
    public float currentAscend = 0;
    [HideInInspector]
    public float currentDescend = 0;
    private float currentY = 0;

    //rig
    [HideInInspector]
    public Rigidbody rig;
    [HideInInspector]
    public Collider col;

    private void Awake()
    {
        this.rig = gameObject.GetComponent<Rigidbody>();
        this.col = gameObject.GetComponent<Collider>();
    }

    // Use this for initialization
    void Start () {

        this.rig.useGravity = this.isUsingGravity;
        this.rig.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        this.currentY = this.currentAscend + this.currentDescend;

        this.Move();
    }

    //movement.call these at FixedUpdate
    public virtual void Move()
    {
        if (this.shouldSteer)
        {
            this.MoveWithSteer();
        }
        else
        {
            this.MoveWithoutSteer();
        }

        if (this.currentAxis.magnitude <= this.thumbstickSensitiveThreshold)
        {
            this.rig.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
        }

        
    }

    public virtual void MoveWithSteer()
    {
        var zAxis = this.currentAxis.y >= 0 ? this.currentAxis.magnitude : -this.currentAxis.magnitude;
        zAxis = Mathf.Abs(this.currentAxis.y) > this.steerSensitiveThreshold ? zAxis : 0;//either speed or rotate/steer,can not both

        if (this.shouldAccelerate)
        {
            var force = new Vector3(0, this.currentY * this.powerY, zAxis * this.power2D);
            this.rig.AddRelativeForce(force, ForceMode.Force);

            if (Mathf.Abs(this.currentAxis.y) <= this.steerSensitiveThreshold && Mathf.Abs(this.currentAxis.x) > this.steerSensitiveThreshold)
            {
                var ratio = this.currentAxis.x > 0 ? 1 : -1;

                this.rig.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                var angleForce = new Vector3(0, ratio * this.SteerPower, 0);
                this.rig.AddRelativeTorque(angleForce, ForceMode.Force);
            }
            else
            {
                this.rig.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
            }
 
        }
        else
        {
            

            var velocity = new Vector3(0, this.currentY * this.speedY, zAxis * this.speed2D);
            velocity = transform.TransformDirection(velocity);
            this.rig.velocity = velocity;

            if (Mathf.Abs(this.currentAxis.y) <= this.steerSensitiveThreshold && Mathf.Abs(this.currentAxis.x) > this.steerSensitiveThreshold)
            {
                //var ratio = this.currentAxis.x > 0 ? (1 - this.currentAxis.x) / (1 - this.steerSensitiveThreshold): (1 + this.currentAxis.x) / (1 - this.steerSensitiveThreshold);
                var ratio = this.currentAxis.x >0 ? 1 : -1;

                this.rig.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                var angleVelocity = new Vector3(0, ratio * this.SteerSpeed * Mathf.Deg2Rad, 0);
                this.rig.angularVelocity = angleVelocity;
            }
            else
            {
                this.rig.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
            }

            
        }
    }

    public virtual void MoveWithoutSteer()
    {

        if (this.shouldAccelerate)
        {
            var force = new Vector3(this.currentAxis.x * this.power2D, this.currentY * this.powerY, this.currentAxis.y * this.power2D);
            this.rig.AddRelativeForce(force, ForceMode.Force);
        }
        else
        {
            var velocity = new Vector3(this.currentAxis.x * this.speed2D, this.currentY * this.speedY, this.currentAxis.y * this.speed2D);
            velocity = transform.TransformDirection(velocity);
            this.rig.velocity = velocity;
            //this.rig.AddRelativeForce(velocity, ForceMode.VelocityChange);
        }
    }

    //brake
    public virtual void Brake()
    {

    }

}
