﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemAudoManager : MonoBehaviour {

    public GameObject headset;
    public AudioClip offScreenIndicatorTransitionAudioClip;
    public AudioClip backgroundAudioClip;
    public float backgroundAudioVolume = 1;
    public bool shouldPlayIndicatorAudio = true;
    public bool shouldPlayBackgroundAtStart = false;


    private AudioSource normalAudioSource;
    private AudioSource backgroundAudioSource;
    private GameObject speaker;

	// Use this for initialization
	void Start () {

        this.speaker = new GameObject();
        this.speaker.transform.parent = this.headset.transform;

        this.normalAudioSource = this.speaker.AddComponent<AudioSource>();
        this.normalAudioSource.playOnAwake = false;

        this.backgroundAudioSource = this.speaker.AddComponent<AudioSource>();
        this.backgroundAudioSource.playOnAwake = false;
        this.backgroundAudioSource.loop = true;
        this.backgroundAudioSource.volume = this.backgroundAudioVolume;

        if (this.shouldPlayBackgroundAtStart)
        {
            this.PlayBackgroundMusic();
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayOffScreenIndicatorTransitionAudio(float volume)
    {
        if (this.offScreenIndicatorTransitionAudioClip != null && this.shouldPlayIndicatorAudio)
        {
            this.normalAudioSource.PlayOneShot(this.offScreenIndicatorTransitionAudioClip,volume);
        }
    }

    public void PlayBackgroundMusic()
    {
        if (this.backgroundAudioClip != null)
        {
            this.backgroundAudioSource.clip = this.backgroundAudioClip;
            this.backgroundAudioSource.volume = this.backgroundAudioVolume;
            this.backgroundAudioSource.Play();
        }
    }
    public void SetBackgroundVolum(float volume)
    {
        if (this.backgroundAudioClip != null)
        {
            this.backgroundAudioSource.volume = volume;
        }
    }

}
