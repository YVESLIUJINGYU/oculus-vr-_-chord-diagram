﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorScript : MonoBehaviour {

    public ReflectionProbe probe1;
    public ReflectionProbe probe2;
    public ReflectionProbe probe3;

    private Renderer render;


    // Use this for initialization
    void Start () {

        this.render = gameObject.GetComponent<Renderer>();

    }
	
	// Update is called once per frame
	void Update () {

        //change color
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            render.sharedMaterials[0].color = Color.red;
            RefreshProbs();


        }
        else if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            render.sharedMaterials[0].color = Color.blue;
            RefreshProbs();
        }
        else if (OVRInput.GetDown(OVRInput.Button.Three))
        {
            render.sharedMaterials[0].color = Color.green;
            RefreshProbs();
        }
        else if (OVRInput.GetDown(OVRInput.Button.Four))
        {
            render.sharedMaterials[0].color = Color.white;
            RefreshProbs();
        }

    }

    void RefreshProbs()
    {
        probe1.RenderProbe();
        probe2.RenderProbe();
        probe3.RenderProbe();

    }

}
