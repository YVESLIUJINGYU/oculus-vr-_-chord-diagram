﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Offset Configuration",menuName ="Game/Configuration")]
public class OffsetConfiguration : ScriptableObject {

    public Vector3 offsetPosition = Vector3.zero;
    public Vector3 offsetAngel = Vector3.zero;

}
