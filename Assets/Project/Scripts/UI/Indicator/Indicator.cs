﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Indicator : MonoBehaviour {

    //indicator object
    public GameObject onScreenIndicator;
    public GameObject offScreenIndicator;

    //on screen indicator setup
    public Vector3 onScreenIndicatorOffset = Vector3.zero;
    public float onScreenIndicatorTransitionFadingTime = 1f;

    //status
    public bool isShowingOnScreenIndicator = true; 
    public bool isShowingOffScreenIndicator = true;
    [HideInInspector]
    public bool isInTransition = false;
    private bool isShowingOnScreenIndicatorCache = true;//cache for turn on and turn off
    private bool isShowingOffScreenIndicatorCache = true;
    private float originalIndicatorAlpha = 1;


    private GameManager gameManager = GameManager.Shared;

    private void Awake()
    {
        if (this.offScreenIndicator == null && this.isShowingOffScreenIndicator)
        {
            this.offScreenIndicator = GameObject.Instantiate(Resources.Load<GameObject>("Prefab/OffScreenIndicator"));
            this.offScreenIndicator.SetActive(false);

        }

        if (this.onScreenIndicator == null && this.isShowingOnScreenIndicator)
        {
            this.onScreenIndicator = GameObject.Instantiate(Resources.Load<GameObject>("Prefab/IndicatorContainer"));
            this.onScreenIndicator.transform.position = Vector3.zero;
            this.onScreenIndicator.transform.parent = this.gameObject.transform;
            this.onScreenIndicator.transform.localPosition = this.onScreenIndicatorOffset;
            this.onScreenIndicator.SetActive(false);

        }
    }

    // Use this for initialization
    void Start () {

        

        this.gameManager.indicatorsManager.activeIndicatorList.Add(this);

        

        //cache
        this.isShowingOffScreenIndicatorCache = this.isShowingOffScreenIndicator;
        this.isShowingOnScreenIndicatorCache = this.isShowingOnScreenIndicator;

        //cache alpha
        List<Renderer> rendersList = this.onScreenIndicator.GetComponentsInChildren<Renderer>().ToList();
        if (rendersList.Count > 0)
        {
            this.originalIndicatorAlpha = rendersList[0].material.color.a;

        }

    }

    private void OnDestroy()
    {
        if (this.gameManager.indicatorsManager != null)
        {
            this.gameManager.indicatorsManager.activeIndicatorList.Remove(this);
        }

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void TurnOffIndicator()
    {
        StartCoroutine(this.OnScreenIndicatorFadeout());

        this.isShowingOffScreenIndicator = false;
        this.isShowingOnScreenIndicator = false;

    }

    public void TurnOnIndicator()
    {
        this.enabled = true;

        this.isShowingOffScreenIndicator = this.isShowingOffScreenIndicatorCache;
        this.isShowingOnScreenIndicator = this.isShowingOnScreenIndicatorCache;

        if (this.onScreenIndicator != null && this.isShowingOnScreenIndicator)
        {
            this.onScreenIndicator.SetActive(true);
        }

        if (this.offScreenIndicator != null && this.isShowingOffScreenIndicator)
        {
            this.offScreenIndicator.SetActive(true);
        }


        StartCoroutine(this.OnScreenIndicatorFadein());
    }

    //animation
    IEnumerator OnScreenIndicatorFadein()
    {
        float time = 0;
        List<Renderer> rendersList = this.onScreenIndicator.GetComponentsInChildren<Renderer>().ToList();

        if (rendersList.Count > 0)
        {
            float endAlpha = this.originalIndicatorAlpha;

            while (time <= this.onScreenIndicatorTransitionFadingTime)
            {
                foreach (Renderer render in rendersList)
                {
                    Color c = render.material.color;
                    c.a = Mathf.Lerp(0, endAlpha, time / this.onScreenIndicatorTransitionFadingTime);
                    render.material.color = c;
                }

                time = time + Time.deltaTime;
                yield return null;

            }

            //
            foreach (Renderer render in rendersList)
            {
                Color c = render.material.color;
                c.a = endAlpha;
                render.material.color = c;


            }

        }

    }

    IEnumerator OnScreenIndicatorFadeout()
    {
        float time = 0;
        List<Renderer> rendersList = this.onScreenIndicator.GetComponentsInChildren<Renderer>().ToList();

        if (rendersList.Count > 0)
        {
            float originalAlpha = rendersList[0].material.color.a;

            while (time <= this.onScreenIndicatorTransitionFadingTime)
            {
                foreach (Renderer render in rendersList)
                {
                    Color c = render.material.color;
                    c.a = Mathf.Lerp(originalAlpha, 0, time / this.onScreenIndicatorTransitionFadingTime);
                    render.material.color = c;
                }

                time = time + Time.deltaTime;
                yield return null;

            }

            //
            foreach (Renderer render in rendersList)
            {
                Color c = render.material.color;
                c.a = 0;
                render.material.color = c;

                
            }

            //
            if (this.onScreenIndicator != null)
            {
                this.onScreenIndicator.SetActive(false);
            }

            if (this.offScreenIndicator != null)
            {
                this.offScreenIndicator.SetActive(false);
            }

            this.enabled = false;

        }

    }

}
