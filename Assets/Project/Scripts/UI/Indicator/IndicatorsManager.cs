﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorsManager : MonoBehaviour {

    public float screenDistance = 0.5f;
    public GameObject headset;
    public float offScreenIndicatorTransitionTime = 1f;
    public float offScreenIndicatorTransitionFadingOutTime = 1f;


    [HideInInspector]
    public List<Indicator> activeIndicatorList = new List<Indicator>();

    private GameManager gameManager = GameManager.Shared;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        this.activeIndicatorList.RemoveAll(item => item == null || (!item.isShowingOffScreenIndicator && !item.isShowingOnScreenIndicator));

        if (this.activeIndicatorList.Count > 0)
        {
            foreach (var indicator in this.activeIndicatorList)
            {
                if (indicator.isShowingOffScreenIndicator)
                {
                    this.DisplayOffScreenIndicator(indicator);
                }
            }
        }

	}

    void DisplayOffScreenIndicator(Indicator indicator)
    {

        bool isIndicatorInScreen = this.IsIndicatorInScreen(indicator);

        if (indicator.isShowingOffScreenIndicator && !isIndicatorInScreen && !indicator.isInTransition)
        {
            indicator.offScreenIndicator.SetActive(true);
            indicator.offScreenIndicator.transform.position = this.GetOffScreenIndicatorPosition(indicator);
            
            //back to lookat headset
            indicator.offScreenIndicator.transform.LookAt(2* indicator.offScreenIndicator.transform.position - this.headset.transform.position);

            //rotate angle
            indicator.offScreenIndicator.transform.Rotate(Vector3.forward, this.GetOffScreenIndicatorAngle(indicator),Space.Self);
        }
        else if (indicator.isShowingOnScreenIndicator && isIndicatorInScreen && !indicator.isInTransition)
        {
            if (indicator.offScreenIndicator != null && !indicator.isInTransition && indicator.offScreenIndicator.activeSelf)
            {
                indicator.isInTransition = true;
                StartCoroutine(this.TransitFromOffScreenToOnScreen(indicator));

            }

            if (indicator.onScreenIndicator != null)
            {
                indicator.onScreenIndicator.SetActive(true);
            }

        }
        else if (!indicator.isShowingOnScreenIndicator && isIndicatorInScreen && !indicator.isInTransition)
        {
            if (indicator.offScreenIndicator != null && !indicator.isInTransition && indicator.offScreenIndicator.activeSelf)
            {
                indicator.isInTransition = true;
                StartCoroutine(this.TransitFromOffScreenToOnScreen(indicator));
            }

            if (indicator.onScreenIndicator != null)
            {
                indicator.onScreenIndicator.SetActive(false);
            }
        }
    }

    bool IsIndicatorInScreen(Indicator indicator)
    {
        Vector3 positionInScreen = Camera.main.WorldToViewportPoint(indicator.gameObject.transform.position);

        if (positionInScreen.x > 0 && positionInScreen.x < 1 && positionInScreen.y > 0 && positionInScreen.y <1 && positionInScreen.z >0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    float GetOffScreenIndicatorAngle(Indicator indicator)
    {
        Vector3 screenCenter = new Vector3(0.5f, 0.5f, 0);
        Vector3 offScreenObjectOnScreen = this.GetOffScreenObjectOnScreen(indicator);

        Vector3 direction = (offScreenObjectOnScreen - screenCenter).normalized * 0.25f;

        float angle = Mathf.Atan2(direction.y , direction.x) * Mathf.Rad2Deg;

        return angle;

    }

    Vector3 GetOffScreenIndicatorPosition(Indicator indicator)
    {
        Vector3 screenCenter = new Vector3(0.5f, 0.5f, 0);
        Vector3 offScreenObjectOnScreen = this.GetOffScreenObjectOnScreen(indicator);

        Vector3 direction = (offScreenObjectOnScreen - screenCenter).normalized * 0.25f;

        Vector3 offScreenIndicatorOnScreen = screenCenter + direction;

        Vector3 offScreenIndicatorPosition = Camera.main.ViewportToWorldPoint(new Vector3(offScreenIndicatorOnScreen.x, offScreenIndicatorOnScreen.y, this.screenDistance));

        return offScreenIndicatorPosition;

    }

    Vector3 GetOffScreenObjectOnScreen(Indicator indicator)
    {
        Vector3 offScreenObjectOnScreen = Camera.main.WorldToViewportPoint(indicator.gameObject.transform.position);


        float z = offScreenObjectOnScreen.z;


        if (z < 0)
        {
            offScreenObjectOnScreen = new Vector3(1 - offScreenObjectOnScreen.x, -offScreenObjectOnScreen.y, 0);
        }
        else
        {
            offScreenObjectOnScreen = new Vector3(offScreenObjectOnScreen.x, offScreenObjectOnScreen.y, 0);
        }

        return offScreenObjectOnScreen;
    }

    IEnumerator TransitFromOffScreenToOnScreen(Indicator indicator)
    {
        //sound effect
        this.gameManager.systemAudioManager.PlayOffScreenIndicatorTransitionAudio(0.005f);

        //back to lookat headset
        //indicator.offScreenIndicator.transform.LookAt(2 * indicator.offScreenIndicator.transform.position - this.headset.transform.position);

        /*
        //angle
        if (indicator.onScreenIndicator.transform.position.y >= indicator.gameObject.transform.position.y)
        {
            indicator.offScreenIndicator.transform.Rotate(Vector3.forward, -90f, Space.Self);
        }
        else
        {
            indicator.offScreenIndicator.transform.Rotate(Vector3.forward, 90f, Space.Self);

        }
        */

        //change position
        float time = 0;
        Vector3 originalPosition = indicator.offScreenIndicator.transform.position;
        Vector3 destinationPosition = indicator.onScreenIndicator.transform.position;

        while (time <= this.offScreenIndicatorTransitionTime)
        {
            

            indicator.offScreenIndicator.transform.position = Vector3.Lerp(originalPosition, destinationPosition, time / this.offScreenIndicatorTransitionTime);

            time = time + Time.deltaTime;

            yield return null;
        }

        indicator.offScreenIndicator.transform.position = destinationPosition;

        //fade out
        time = 0;
        Material material = indicator.offScreenIndicator.GetComponent<Renderer>().material;

        while (time <= this.offScreenIndicatorTransitionFadingOutTime)
        {
            Color color = material.color;

            color.a = Mathf.Lerp(1, 0, time / this.offScreenIndicatorTransitionFadingOutTime);

            material.color = color;

            time = time + Time.deltaTime;

            yield return null;
        }

        //back to normal
        Color finalColor = material.color;
        finalColor.a = 1;
        material.color = finalColor;

        indicator.isInTransition = false;
        indicator.offScreenIndicator.SetActive(false);
    }

}
