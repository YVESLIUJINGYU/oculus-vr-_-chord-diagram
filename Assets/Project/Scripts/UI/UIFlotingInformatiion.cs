﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFlotingInformatiion : MonoBehaviour {

    [Header("Basic Setup")]
    public float dampingSpeed = 0.5f; 
    public bool shouldFollowHead = true;//true means floting ui will follow head ,false means floating ui will follow targetGameObject
    public Vector3 offset = Vector3.zero; // ui offset to head , if shouldFollowHead is false then this is offset to targetGameObject
    public bool shouldStickToHeadVertically = false;
    public float headRotationMaxAngle = 70f;
    public bool shouldShowLine = true;
    

    [Header("Line Setup")]
    public Transform targetGameobject;
    public Vector3 firstPoint = Vector3.zero;
    public Vector3 secondPoint = Vector3.zero;
    public Color lineColor = new Color32(0x4C, 0xFF, 0x70, 0x8F);//green;
    public float lineRendererSpeed = 0.5f;

    [Header("Content Refrence")]
    public TextMesh textMesh;
    public SpriteRenderer spriteRendererContent;
    public SpriteRenderer spriteRendererImage;
    public float uiFadingTime = 2f;

    private Transform head = null;
    private Transform followedGameobject = null;
    private Vector3 currentVelocity = Vector3.zero;
    
    //line render
    private LineRenderer indicatorLineRenderer = null;

    // Use this for initialization
    void Start () {

        if (this.head == null && Camera.main != null)
        {
            this.head = Camera.main.transform;
        }

        if (this.shouldFollowHead)
        {
            this.followedGameobject = this.head;
        }
        else if (this.targetGameobject != null)
        {
            this.followedGameobject = this.targetGameobject;
        }
        else
        {
            //throw error

        }

        //set original position
        Vector3 targetPosition = this.followedGameobject.TransformPoint(this.offset);
        this.transform.position = targetPosition;

    }
	
	// Update is called once per frame
	void Update () {

        if (this.shouldShowLine && this.targetGameobject != null)
        {
            this.ShowIndicatorLineRenderer(new List<Vector3> { this.transform.TransformPoint(this.firstPoint), this.transform.TransformPoint(this.secondPoint),this.targetGameobject.position });

            this.AnimateLinerenderer();
        }

	}

    private void LateUpdate()
    {
        this.UpdatePosition();
    }


    //helper
    private void UpdatePosition()
    {
        if (!this.shouldFollowHead || this.shouldStickToHeadVertically)
        {
            Vector3 targetPosition = this.followedGameobject.TransformPoint(this.offset);
            Vector3 newPosition = Vector3.SmoothDamp(this.transform.position, targetPosition, ref currentVelocity, this.dampingSpeed);
            this.transform.position = newPosition;

            //this.transform.LookAt(this.head);
            this.transform.LookAt(2 * this.transform.position - this.head.transform.position);
        }
        else
        {
            var headXAngle = this.head.transform.localEulerAngles.x;
            var headZAngle = this.head.transform.localEulerAngles.z;

            var xEulerAngle = this.head.transform.localEulerAngles.x;

            if (xEulerAngle >= this.headRotationMaxAngle && xEulerAngle <= 180)
            {
                xEulerAngle = this.headRotationMaxAngle;
            }
            else if (xEulerAngle <= 360 - this.headRotationMaxAngle && xEulerAngle > 180)
            {
                xEulerAngle = -this.headRotationMaxAngle;
            }

            this.head.transform.localEulerAngles = new Vector3(xEulerAngle, this.head.transform.localEulerAngles.y, 0);

            Vector3 targetPosition = this.head.transform.TransformPoint(this.offset);
            Vector3 newPosition = Vector3.SmoothDamp(this.transform.position, targetPosition, ref currentVelocity, this.dampingSpeed);
            this.transform.position = newPosition;

            this.transform.LookAt(2 * this.transform.position - this.head.transform.position);

            this.head.transform.localEulerAngles = new Vector3(headXAngle, this.head.transform.localEulerAngles.y, headZAngle);
        }
    }

    /** line renderer **/
    void ShowIndicatorLineRenderer(List<Vector3> positions)
    {
        if (this.indicatorLineRenderer == null)
        {
            this.indicatorLineRenderer = this.gameObject.AddComponent<LineRenderer>();
            this.indicatorLineRenderer.positionCount = positions.Count;
            this.indicatorLineRenderer.textureMode = LineTextureMode.Tile;

            this.indicatorLineRenderer.material = Resources.Load<Material>("Materials/TeleportationLineMaterial");
            this.indicatorLineRenderer.startColor = this.lineColor;
            this.indicatorLineRenderer.endColor = this.lineColor;


            this.indicatorLineRenderer.startWidth = 0.01f;
            this.indicatorLineRenderer.endWidth = 0.01f;
        }
        else
        {

            this.indicatorLineRenderer.positionCount = positions.Count;
        }


        //set positions
        this.indicatorLineRenderer.SetPositions(positions.ToArray());

        //scale texture
        this.indicatorLineRenderer.material.SetTextureScale("_MainTex", new Vector2(0.5f, 0));

    }

    private void AnimateLinerenderer()
    {

        if (this.indicatorLineRenderer != null)
        {
            float offset = -Time.time * this.lineRendererSpeed;
            this.indicatorLineRenderer.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));

        }

    }

    //public
    public void Fadein()
    {
        this.gameObject.SetActive(true);

        StartCoroutine(this.UIFadein());
    }

    public void Fadeout()
    {
        StartCoroutine(this.UIFadeout());
    }

    //animation
    IEnumerator UIFadein()
    {
        float time = 0;

        while (time <= this.uiFadingTime)
        {
            if (this.textMesh != null)
            {
                Color tc = this.textMesh.color;
                tc.a = Mathf.Lerp(0, 1, time / this.uiFadingTime);
                this.textMesh.color = tc;
            }


            if (this.spriteRendererContent != null)
            {
                Color sc = this.spriteRendererContent.color;
                sc.a = Mathf.Lerp(0, 1, time / this.uiFadingTime);
                this.spriteRendererContent.color = sc;
            }

            if (this.spriteRendererImage != null)
            {
                Color sc = this.spriteRendererImage.color;
                sc.a = Mathf.Lerp(0, 1, time / this.uiFadingTime);
                this.spriteRendererImage.color = sc;
            }

            time = time + Time.deltaTime;
            yield return null;

        }

        if (this.textMesh != null)
        {
            Color newc = this.textMesh.color;
            newc.a = 1;
            this.textMesh.color = newc;
        }

        if (this.spriteRendererContent != null)
        {
            Color newsc = this.spriteRendererContent.color;
            newsc.a = 1;
            this.spriteRendererContent.color = newsc;
        }

        if (this.spriteRendererImage != null)
        {
            Color newsc = this.spriteRendererImage.color;
            newsc.a = 1;
            this.spriteRendererImage.color = newsc;
        }

    }

    IEnumerator UIFadeout()
    {
        float time = 0;

        while (time <= this.uiFadingTime)
        {

            if (this.textMesh != null)
            {
                Color c = this.textMesh.color;
                c.a = Mathf.Lerp(1, 0, time / this.uiFadingTime);
                this.textMesh.color = c;
            }

            if (this.spriteRendererContent != null)
            {
                Color sc = this.spriteRendererContent.color;
                sc.a = Mathf.Lerp(1, 0, time / this.uiFadingTime);
                this.spriteRendererContent.color = sc;
            }

            if (this.spriteRendererImage != null)
            {
                Color sc = this.spriteRendererImage.color;
                sc.a = Mathf.Lerp(1, 0, time / this.uiFadingTime);
                this.spriteRendererImage.color = sc;
            }

            time = time + Time.deltaTime;
            yield return null;

        }

        if (this.textMesh != null)
        {
            Color newc = this.textMesh.color;
            newc.a = 0;
            this.textMesh.color = newc;
        }
        

        if (this.spriteRendererContent != null)
        {
            Color newsc = this.spriteRendererContent.color;
            newsc.a = 0;
            this.spriteRendererContent.color = newsc;
        }

        if (this.spriteRendererImage != null)
        {
            Color newsc = this.spriteRendererImage.color;
            newsc.a = 0;
            this.spriteRendererImage.color = newsc;
        }

        this.gameObject.SetActive(false);

    }

}
