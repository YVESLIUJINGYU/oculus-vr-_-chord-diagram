﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public static class VRHelper {

	public enum VRBrands
    {
        Unknow,
        OculusRift,
        HtcVive
    }

    private static string cachedHeadsetBrand = "";

    public static VRBrands GetHeadsetBrand()
    {
        VRBrands returnValue = VRBrands.Unknow;

        VRHelper.cachedHeadsetBrand = (VRHelper.cachedHeadsetBrand == "" ? UnityEngine.XR.XRDevice.model.Replace(" ", "").Replace(".", "").ToLowerInvariant() : VRHelper.cachedHeadsetBrand);

        if (VRHelper.cachedHeadsetBrand.Contains("rift"))
        {
            return VRBrands.OculusRift;
        }
        else if (VRHelper.cachedHeadsetBrand.Contains("vive"))
        {
            return VRBrands.HtcVive;
        }

        return returnValue;


    }

}
