﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

public class Utility {

    /// <summary>
    /// The CloneComponent method takes a source component and copies it to the given destination game object.
    /// </summary>
    /// <param name="source">The component to copy.</param>
    /// <param name="destination">The game object to copy the component to.</param>
    /// <param name="copyProperties">Determines whether the properties of the component as well as the fields should be copied.</param>
    /// <returns>The component that has been cloned onto the given game object.</returns>
    public static Component CloneComponent(Component source, GameObject destination, bool copyProperties = false)
    {
        Component tmpComponent = destination.gameObject.AddComponent(source.GetType());
        if (copyProperties)
        {
            foreach (PropertyInfo p in source.GetType().GetProperties())
            {
                if (p.CanWrite)
                {
                    p.SetValue(tmpComponent, p.GetValue(source, null), null);
                }
            }
        }

        foreach (FieldInfo f in source.GetType().GetFields())
        {
            f.SetValue(tmpComponent, f.GetValue(source));
        }
        return tmpComponent;
    }


    //so angel can have negative number
    public static float CaculateAngel(Vector3 from, Vector3 to)
    {
        float angel = Vector3.Angle(from, to);
        Vector3 cross = Vector3.Cross(from, to);

        if (cross.y > 0) angel = -angel;

        return angel;
    }


}
