﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRHeadset : MonoBehaviour {

    [HideInInspector]
    public Vector3 velocity = Vector3.zero;
    private Vector3? lastVelocity = null;
    private Vector3? lastLocation = null;

    [HideInInspector]
    public Vector3 acceleration = Vector3.zero;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (this.lastLocation .HasValue)
        {
            
            this.velocity = (this.gameObject.transform.position - this.lastLocation.Value)/Time.deltaTime;
        }

        if (this.lastVelocity.HasValue)
        {
            this.acceleration = (this.velocity - this.lastVelocity.Value) / Time.deltaTime;
        }

        this.lastVelocity = this.velocity;
        this.lastLocation = this.gameObject.transform.position;

        //
        //this.acceleration = OVRManager.display.acceleration;
        //this.velocity = OVRManager.display.velocity;

    }
}
