﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this class to init some vr configration.
/// </summary>
public class VRInit : MonoBehaviour {

    public IndicatorsManager indicatorsManager;
    public SystemAudoManager systemAudioManager;
    public VRInput vrInput;

    //fade in
    public float fadeDuration = 0.5f;
    public GameObject fadeSphere;

    private GameManager gameManager;

    private void Awake()
    {
        this.gameManager = GameManager.Shared;

        //set vrinput
        this.gameManager.vrInput = this.vrInput;

        //set indicators manager
        this.gameManager.indicatorsManager = this.indicatorsManager;

        //set audio manager
        this.gameManager.systemAudioManager = this.systemAudioManager;

        //set controllers
        this.gameManager.vrManager.SetControllersAndHeadsetAndPlayer();
    }

    // Use this for initialization
    void Start () {

        //set controllers
        this.gameManager.vrManager.SetControllersAndHeadsetAndPlayer();

        if (this.fadeSphere != null)
        {
            StartCoroutine(this.StartThisScene());
        }

    }
	
	// Update is called once per frame
	void Update () {

        //set controllers
        this.gameManager.vrManager.SetControllersAndHeadsetAndPlayer();

    }

    IEnumerator StartThisScene()
    {
        Material fadeMaterial = this.fadeSphere.GetComponent<Renderer>().sharedMaterials[0];
        fadeMaterial.color = Color.black;

        yield return new WaitForSeconds(3);

        yield return StartCoroutine(this.TeleportFadeIn());
    }

    protected IEnumerator TeleportFadeIn()
    {
        Material fadeMaterial = this.fadeSphere.GetComponent<Renderer>().sharedMaterials[0];
        fadeMaterial.color = Color.black;
        Color beginColor = fadeMaterial.color;

        for (float t = 0.0f; t <= this.fadeDuration; t = t + Time.deltaTime)
        {
            fadeMaterial.color = Color.Lerp(Color.black, Color.clear, t / this.fadeDuration);
            yield return null;
        }

        fadeMaterial.color = Color.clear;

    }
}
