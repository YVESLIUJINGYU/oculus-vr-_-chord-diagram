﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class VRInput : MonoBehaviour {

    //a input interface for all vr brand
    private IInput inputManager;

    //event
    public event Action OnLGrabDown;
    public event Action OnLGrabUp;
    public event Action<float> OnLGrabPressed;
    public event Action OnLIndexDown;
    public event Action OnLIndexUp;
    public event Action<float> OnLIndexPressed;
    public event Action<Vector2> OnLThumbstickMoved;
    public event Action<bool> OnLThumbstickTouched;
    public event Action<bool> OnLIndexTouched;
    public event Action<bool> OnButtonThreePressed;
    public event Action<bool> OnButtonFourPressed;

    public event Action OnRGrabDown;
    public event Action OnRGrabUp;
    public event Action<float> OnRGrabPressed;
    public event Action OnRIndexDown;
    public event Action OnRIndexUp;
    public event Action<float> OnRIndexPressed;
    public event Action<Vector2> OnRThumbstickMoved;
    public event Action<bool> OnRIndexTouched;
    public event Action<bool> OnRThumbstickTouched;
    public event Action<bool> OnButtonOnePressed;
    public event Action<bool> OnButtonTwoPressed;

    // Use this for initialization
    void Start () {

        this.inputManager = GameManager.Shared.inputManager;

#if VR_OCULUS
        //this.inputManager = OculusManager.Shared;

#elif VR_HTCVIVE
        //this.inputManager = HTCViveManager.Shared;

#endif

    }

    // Update is called once per frame
    void Update () {

        this.CheckInput();

	}

    private void CheckInput()
    {
        //left
        if (this.inputManager.LGrabDown())
        {
            if (this.OnLGrabDown != null)
            {
                this.OnLGrabDown();
            }
        }

        if (this.inputManager.LGrabUp())
        {
            if (this.OnLGrabUp != null)
            {
                this.OnLGrabUp();
            }
        }

        if (this.OnLGrabPressed != null)
        {
            this.OnLGrabPressed(this.inputManager.LGrabPressed());
        }

        if (this.inputManager.LIndexDown())
        {
            if (this.OnLIndexDown != null)
            {
                this.OnLIndexDown();
            }
        }

        if (this.inputManager.LIndexUp())
        {
            if (this.OnLIndexUp != null)
            {
                this.OnLIndexUp();
            }
        }

        if (this.inputManager.LIndexPressed() > 0)
        {
            if (this.OnLIndexPressed != null)
            {
                this.OnLIndexPressed(this.inputManager.LIndexPressed());
            }
        }

        //right
        if (this.inputManager.RGrabDown())
        {
            if (this.OnRGrabDown != null)
            {
                this.OnRGrabDown();
            }
        }

        if (this.inputManager.RGrabUp())
        {
            if (this.OnRGrabUp != null)
            {
                this.OnRGrabUp();
            }
        }

        if (this.OnRGrabPressed != null)
        {
            this.OnRGrabPressed(this.inputManager.RGrabPressed());
        }

        if (this.inputManager.RIndexDown())
        {
            if (this.OnRIndexDown != null)
            {
                this.OnRIndexDown();
            }
        }

        if (this.inputManager.RIndexUp())
        {
            if (this.OnRIndexUp != null)
            {
                this.OnRIndexUp();
            }
        }

        if (this.inputManager.RIndexPressed() > 0)
        {
            if (this.OnRIndexPressed != null)
            {
                this.OnRIndexPressed(this.inputManager.RIndexPressed());
            }
        }

        //only for oculus touch
        if (this.OnLIndexTouched != null)
        {
            this.OnLIndexTouched(this.inputManager.LIndexTouched());
        }

        if (this.OnRIndexTouched != null)
        {
            this.OnRIndexTouched(this.inputManager.RIndexTouched());
        }

        //thumbstick
        if (this.OnLThumbstickMoved != null)
        {
            this.OnLThumbstickMoved(this.inputManager.LThumbstickMoved());
        }

        if (this.OnRThumbstickMoved != null)
        {
            this.OnRThumbstickMoved(this.inputManager.RThumbstickMoved());
        }

        if (this.OnLThumbstickTouched != null)
        {
            this.OnLThumbstickTouched(this.inputManager.LThumbstickTouched());
        }

        if (this.OnRThumbstickTouched != null)
        {
            this.OnRThumbstickTouched(this.inputManager.RThumbstickTouched());
        }

        //button
        if (this.OnButtonOnePressed != null)
        {
            this.OnButtonOnePressed(this.inputManager.ButtonOnePressed());
        }

        if (this.OnButtonTwoPressed != null)
        {
            this.OnButtonTwoPressed(this.inputManager.ButtonTwoPressed());
        }

        if (this.OnButtonThreePressed != null)
        {
            this.OnButtonThreePressed(this.inputManager.ButtonThreePressed());
        }

        if (this.OnButtonFourPressed != null)
        {
            this.OnButtonFourPressed(this.inputManager.ButtonFourPressed());
        }

    }

}
