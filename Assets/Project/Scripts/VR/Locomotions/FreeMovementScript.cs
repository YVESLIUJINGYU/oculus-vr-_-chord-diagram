﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeMovementScript : LocomotionBase {

    public float speed = 3.0f;
    public float rotationSpeed = 90;
    public LayerMask movementLayer;
    public Color tunnelingColor = Color.black;
    public float tunnelingDuration = 0.5f;
    public bool isControllerRotationOn = true;//controller to rotate on/off
    public bool isAccelerationOn = false;//movement acceleration on/off

    //sphere
    public GameObject tunnelingSphere;
    private Material tunnelingSphereMaterial;
    private float tunnelingIntensity = 0;

    //last valid teleportation position: in case user end up in a invalid postion
    private Vector3 lastValidTeleportationPosition = Vector3.zero;
    private Vector3 lastValidTeleportationNormal = Vector3.up;

    //player position
    private Vector2 indicatorPosition;
    private bool isMoving = false;
    private bool isFromValidPosition = false; //if first movement is from a valid place
    private Vector3 indicatorObjectNormal = Vector3.up;


    // Use this for initialization
    protected override void Start() {

        base.Start();

        this.tunnelingSphereMaterial = this.tunnelingSphere.GetComponent<MeshRenderer>().material;
        this.tunnelingSphereMaterial.color = this.tunnelingColor;
	}
	
	// Update is called once per frame
	void Update () {


    }

    public override void OnThumbstickMoved(VRController controller, Vector2 thumbstickAxis2D)
    {
        base.OnThumbstickMoved(controller, thumbstickAxis2D);

        //move player
        //if (Mathf.Abs(thumbStickVector.x) > 0.5 || Mathf.Abs(thumbStickVector.y) > 0.5)
        if (thumbstickAxis2D.magnitude > this.sensitiveTriggerThreshold)
        {
            //transfer hand local movement to world movement
            Vector3 worldPoint = Quaternion.AngleAxis(controller.controllerObject.transform.eulerAngles.y, Vector3.up) * new Vector3(thumbstickAxis2D.x, 0, thumbstickAxis2D.y);
            Vector2 worldThumbStickVector = new Vector2(worldPoint.x, worldPoint.z);

            //transfer world thumbstick vector
            if (!this.isAccelerationOn)
            {
                worldThumbStickVector = this.TransferStickMovement(worldThumbStickVector);
            }

            //update indicator position
            this.UpdateIndicatorPosition(worldThumbStickVector);

            //move player
            this.MovePlayerPosition(worldThumbStickVector);

            //rotate player
            if (this.isControllerRotationOn)
            {
                this.RotatePlagyerDirection(controller);

            }


            //tunneling effect
            this.ShowTunneling();

        }
        else
        {

            if (this.isMoving)
            {
                this.ResetPlayer();
                //tunneling effect
                StartCoroutine(this.HideTunneling());
            }

        }

    }

    //helper
    Vector2 TransferStickMovement(Vector2 movement)
    {
        return new Vector2(movement.x / movement.magnitude, movement.y / movement.magnitude);

    }

    //Tunneling
    void ShowTunneling()
    {
        float deltaIntensity = (Time.deltaTime / this.tunnelingDuration) * 1;

        if (this.tunnelingIntensity + deltaIntensity >=1)
        {
            this.tunnelingIntensity = 1;
        }
        else
        {
            this.tunnelingIntensity = this.tunnelingIntensity + deltaIntensity;

        }
        this.tunnelingSphereMaterial.SetFloat("_Intensity", this.tunnelingIntensity);

    }

    IEnumerator HideTunneling()
    {
        float beginIntensity = this.tunnelingIntensity;

        for (float t = 0.0f; t <= this.tunnelingDuration; t = t + Time.deltaTime)
        {
            this.tunnelingIntensity = Mathf.Lerp(beginIntensity, 0, t / this.tunnelingDuration);
            this.tunnelingSphereMaterial.SetFloat("_Intensity", this.tunnelingIntensity);
            yield return null;
        }

        this.tunnelingIntensity = 0;
        this.tunnelingSphereMaterial.SetFloat("_Intensity", this.tunnelingIntensity);
    }

    /** indicator **/
    void UpdateIndicatorPosition(Vector2 movement)
    {

        //update indicator position
        if (!this.isMoving)
        {
            this.isMoving = true;
            this.indicatorPosition = new Vector2(this.player.transform.position.x, this.player.transform.position.z);


        }

        this.indicatorPosition = this.indicatorPosition + new Vector2(movement.x, movement.y) * speed * Time.deltaTime;

    }

    void ReUpdateIndicatorPosition(Vector2 movement)
    {

        //caculate box collider wall side normal
        Vector3 startPosition = this.player.transform.position + new Vector3(movement.x, 0, movement.y) * speed * Time.deltaTime;
        Vector3 endPosition = this.player.transform.position - this.indicatorObjectNormal * 0.00001f;
        Ray ray = new Ray(startPosition, endPosition - startPosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100, this.movementLayer))
        {
            Vector3 hitNormal = hit.normal;

            Vector3 updatedMovement = Vector3.ProjectOnPlane(new Vector3(movement.x, 0, movement.y), hitNormal);

            Vector2 updatedIndicatorMovement = new Vector2(updatedMovement.x, updatedMovement.z);
            this.indicatorPosition = this.indicatorPosition + updatedIndicatorMovement * speed * Time.deltaTime;

            //create indicator object
            Ray newray = new Ray(new Vector3(this.indicatorPosition.x, 2, this.indicatorPosition.y), Vector3.down);
            RaycastHit newhit;
            if (Physics.Raycast(newray, out newhit, 100, this.movementLayer))
            {

                this.player.transform.position = newhit.point;
                this.indicatorObjectNormal = newhit.normal;

                this.isFromValidPosition = true;

            }
            else
            {
                //if no collision then rollback to previous position
                this.indicatorPosition = this.indicatorPosition - updatedIndicatorMovement * speed * Time.deltaTime;

            }

        }


    }

    /** player **/
    void RotatePlagyerDirection(VRController controller)
    {
        float rotationAngle = controller.controllerObject.transform.eulerAngles.z;
        float refinedRotationAngle = 0;

        //create nagative angle
        if (true)
        {
            if (rotationAngle > 180)
            {
                rotationAngle = rotationAngle - 360;
            }
        }


        if (rotationAngle > 90)
        {
            refinedRotationAngle = 90;
        }
        else if (rotationAngle < -90 ) {

            refinedRotationAngle = -90;

        }
        else
        {
            refinedRotationAngle = rotationAngle;

        }

        float realRotationSpeedRatio = refinedRotationAngle / 90;

        if (Mathf.Abs(realRotationSpeedRatio) > 0.05)
        {
            this.player.transform.Rotate(new Vector3(0, -realRotationSpeedRatio * this.rotationSpeed * Time.deltaTime, 0));
        }


    }

    void MovePlayerPosition(Vector2 movement)
    {
        if (this.isMoving)
        {
            //create indicator object
            Ray ray = new Ray(new Vector3(this.indicatorPosition.x, 2, this.indicatorPosition.y), Vector3.down);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100, this.movementLayer))
            {

                this.player.transform.position = hit.point;
                this.indicatorObjectNormal = hit.normal;

                this.isFromValidPosition = true;

            }
            else
            {
                if (!this.isFromValidPosition)
                {
                    //in case start at invalid position
                    this.indicatorPosition = new Vector2(this.lastValidTeleportationPosition.x, this.lastValidTeleportationPosition.z);
                    this.indicatorObjectNormal = this.lastValidTeleportationNormal;
                    this.player.transform.position = this.lastValidTeleportationPosition;

                    this.isFromValidPosition = true;
                }
                else
                {
                    //if no collision then rollback to previous position
                    this.indicatorPosition = this.indicatorPosition - new Vector2(movement.x, movement.y) * speed * Time.deltaTime;
                    this.ReUpdateIndicatorPosition(movement);
                }

            }

        }

    }

    void ResetPlayer()
    {
        this.lastValidTeleportationPosition = this.player.transform.position;
        this.lastValidTeleportationNormal = this.indicatorObjectNormal;
        this.isMoving = false;
        this.isFromValidPosition = false;

    }


}
