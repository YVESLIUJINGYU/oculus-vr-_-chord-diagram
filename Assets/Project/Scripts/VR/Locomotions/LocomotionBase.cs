﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocomotionBase : MonoBehaviour {

    public GameObject player;
    [Range(0.1f, 0.95f)]
    public float sensitiveTriggerThreshold = 0.3f;//thumbstick threshold

    protected VRManagerBase vrManager;

    // Use this for initialization
    protected virtual void Start() {

        if (this.player == null)
        {
            throw new System.Exception("vrinput is null");
        }

        this.vrManager = GameManager.Shared.vrManager;

#if VR_OCULUS
        //this.vrManager = OculusManager.Shared;

#elif VR_HTCVIVE
        //this.vrManager = HTCViveManager.Shared;

        //htcvive need no sensitive release threshold
        this.sensitiveTriggerThreshold = 0f;
#endif
        /*
        //htcvive need no sensitive release threshold
        switch (VRHelper.GetHeadsetBrand())
        {
            case VRHelper.VRBrands.OculusRift:
                break;

            case VRHelper.VRBrands.HtcVive:
                this.sensitiveTriggerThreshold = 0f;
                break;

            default:
                break;
        }
        */

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void OnThumbstickTouched(VRController controller,bool isTouched)
    {


    }

    public virtual void OnThumbstickMoved(VRController controller,Vector2 thumbstickAxis2D)
    {


    }

}
