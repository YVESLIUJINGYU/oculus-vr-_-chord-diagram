﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporationScript : LocomotionTeleportation
{
    public float speed = 2;//the indicator speed

    //last valid teleportation position: in case user end up in a invalid postion
    private Vector3 lastValidTeleportationPosition = Vector3.zero;
    private Vector3 lastValidTeleportationNormal = Vector3.up;

    //indicator
    private Vector2 indicatorPosition;
    private bool isIndicatorShowed = false;

    //line render
    private readonly int indicatorLineRendererPositionCount = 31;
    private readonly float indicatorLineRendererHeight = 1.3f;

    // Use this for initialization
    protected override void Start() {

        base.Start();

	}
	
	// Update is called once per frame
	void Update () {

        

        //linerenderer animation
        this.AnimateLinerenderer();

    }

    public override void OnThumbstickTouched(VRController controller,bool isTouched)
    {
        base.OnThumbstickTouched(controller,isTouched);

        //teleport
        if (!isTouched || !this.isIndicatorShowed)
        {
            if (this.isIndicatorShowed && !this.isTeleporting)
            {
                this.isTeleporting = true;
                StartCoroutine(this.TeleportPlayer());

            }

        }
        //keep still, no moving. just update liner
        else
        {
            this.ShowIndicator(Vector2.zero, controller);
        }

    }

    public override void OnThumbstickMoved(VRController controller, Vector2 thumbstickAxis2D)
    {
        base.OnThumbstickMoved(controller, thumbstickAxis2D);

        //move indicator position
        if (thumbstickAxis2D.magnitude > this.sensitiveTriggerThreshold)
        {
            //transfer hand local movement to world movement
            //Vector3 worldPoint = this.leftHand.transform.TransformDirection(new Vector3(thumbStickVector.x, 0, thumbStickVector.y));
            //Vector2 worldThumbStickVector = new Vector2(worldPoint.x, worldPoint.z);
            Vector3 worldPoint = Quaternion.AngleAxis(controller.controllerObject.transform.eulerAngles.y, Vector3.up) * new Vector3(thumbstickAxis2D.x, 0, thumbstickAxis2D.y);
            Vector2 worldThumbStickVector = new Vector2(worldPoint.x, worldPoint.z);

            //update indicator position
            this.UpdateIndicatorPosition(worldThumbStickVector,controller);

            this.ShowIndicator(worldThumbStickVector,controller);

        }

    }

    /** move **/
    IEnumerator TeleportPlayer()
    {
        if (this.player != null && this.indicatorObject != null)
        {
            yield return StartCoroutine(this.TeleportFadeOut());
            this.player.transform.position = this.CaculatePlayerPositionWithHeadsetDelta(this.indicatorObject.transform.position);
            this.HideIndicator();
            yield return new WaitForSeconds(0.3f);
            yield return StartCoroutine(this.TeleportFadeIn());

            this.isTeleporting = false;
        }

    }

    /** indicator **/
    void UpdateIndicatorPosition(Vector2 movement,VRController controller)
    {

        //update indicator position
        if (!this.isIndicatorShowed)
        {
            this.isIndicatorShowed = true;
            this.indicatorPosition = new Vector2(controller.controllerObject.transform.position.x, controller.controllerObject.transform.position.z);


        }

        this.indicatorPosition = this.indicatorPosition + new Vector2(movement.x, movement.y) * speed * Time.deltaTime;

    }

    void ReUpdateIndicatorPosition(Vector2 movement)
    {
        if (this.indicatorObject == null)
        {
            return;
        }

        //caculate box collider wall side normal
        Vector3 startPosition = this.indicatorObject.transform.position + new Vector3(movement.x,0,movement.y) * speed * Time.deltaTime;
        Vector3 endPosition = this.indicatorObject.transform.position - this.indicatorObjectNormal * 0.00001f;
        Ray ray = new Ray(startPosition, endPosition - startPosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100, this.teleporationLayer))
        {
            Vector3 hitNormal = hit.normal;

            Vector3 updatedMovement = Vector3.ProjectOnPlane(new Vector3(movement.x,0,movement.y), hitNormal);

            Vector2 updatedIndicatorMovement = new Vector2(updatedMovement.x, updatedMovement.z);
            this.indicatorPosition = this.indicatorPosition + updatedIndicatorMovement * speed * Time.deltaTime;

            //create indicator object
            Ray newray = new Ray(new Vector3(this.indicatorPosition.x, 2, this.indicatorPosition.y), Vector3.down);
            RaycastHit newhit;
            if (Physics.Raycast(newray, out newhit, 100, this.teleporationLayer))
            {

                if (!this.indicatorObject.activeInHierarchy)
                {
                    this.indicatorObject.SetActive(true);
                }

                this.indicatorObject.transform.position = newhit.point;
                this.indicatorObjectNormal = newhit.normal;

            }
            else
            {
                //if no collision then rollback to previous position
                this.indicatorPosition = this.indicatorPosition - updatedIndicatorMovement * speed * Time.deltaTime;

            }

        }


     }

    void ShowIndicator(Vector2 movement,VRController controller)
    {
        if (this.isIndicatorShowed)
        {
            //create indicator object
            Ray ray = new Ray(new Vector3(this.indicatorPosition.x, 2, this.indicatorPosition.y), Vector3.down);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100,this.teleporationLayer))
            {

                if (!this.indicatorObject.activeInHierarchy)
                {
                    this.indicatorObject.SetActive(true);
                }

                this.indicatorObject.transform.position = hit.point;
                this.indicatorObjectNormal = hit.normal;

            }
            else
            {
                if (!this.indicatorObject.activeInHierarchy)
                {
                    this.indicatorObject.SetActive(true);

                    //in case start at invalid position
                    this.indicatorPosition = new Vector2(this.lastValidTeleportationPosition.x, this.lastValidTeleportationPosition.z);
                    this.indicatorObjectNormal = this.lastValidTeleportationNormal;
                    this.indicatorObject.transform.position = this.lastValidTeleportationPosition;
                }
                else
                {
                    //if no collision then rollback to previous position
                    this.indicatorPosition = this.indicatorPosition - new Vector2(movement.x, movement.y) * speed * Time.deltaTime;
                    this.ReUpdateIndicatorPosition(movement);
                }

            }

            //create line renderer
            Vector3 startPosition = controller.controllerObject.transform.position;
            Vector3 endPosition = this.indicatorObject.transform.position;
            this.ShowIndicatorLineRenderer(startPosition, endPosition);

        }

    }

    void HideIndicator()
    {
        if (this.indicatorObject != null && this.indicatorObject.activeInHierarchy)
        {
            this.lastValidTeleportationPosition = this.indicatorObject.transform.position;
            this.lastValidTeleportationNormal = this.indicatorObjectNormal;

            this.indicatorObject.SetActive(false);
            this.isIndicatorShowed = false;

        }

        this.HideIndicatorLineRenderer();

    }

    /** line renderer **/
    void ShowIndicatorLineRenderer(Vector3 startPosition,Vector3 endPosition)
    {
        if (this.indicatorObject != null)
        {
            if (this.indicatorLineRenderer == null)
            {
                this.indicatorLineRenderer = this.indicatorObject.AddComponent<LineRenderer>();

                this.indicatorLineRenderer.positionCount = this.indicatorLineRendererPositionCount;
                this.indicatorLineRenderer.material = Resources.Load<Material>("Materials/TeleportationLineMaterial");
                this.indicatorLineRenderer.textureMode = LineTextureMode.Tile;
                this.indicatorLineRenderer.startColor = Color.green;
                this.indicatorLineRenderer.endColor = Color.green;
                this.indicatorLineRenderer.startWidth = 0.01f;
                this.indicatorLineRenderer.endWidth = 0.01f;
            }


            //set positions
            Vector3[] positions = new Vector3[this.indicatorLineRendererPositionCount];
            Vector3 center = (startPosition + endPosition) * 0.5f;
            for (int i = 0; i < this.indicatorLineRendererPositionCount; i++)
            {
                /*
                if (i == 0)
                {
                    positions[i] = startPosition;
                }
                else if (i == this.indicatorLineRendererPositionCount-1)
                {
                    positions[i] = endPosition;
                }
                else
                {
                    
                    //positions[i] = Vector3.Slerp(startToCenter, endToCenter, (float)i / ((float)this.indicatorLineRendererPositionCount - (float)1));
                    
                    positions[i] = Vector3.Lerp(startPosition, endPosition, (float)i / (float)(this.indicatorLineRendererPositionCount - (float)1));
                    positions[i].y = Mathf.Lerp(2, 0, Mathf.Pow(Mathf.Abs((float)this.indicatorLineRendererPositionCount/ (float)2 - (float)i),2)/ Mathf.Pow(((float)this.indicatorLineRendererPositionCount / (float)2),2));
                }
                */
                positions[i] = Vector3.Lerp(startPosition, endPosition, (float)i / (float)(this.indicatorLineRendererPositionCount - 1));

                if (i< (this.indicatorLineRendererPositionCount + 1)/2)
                {
                    //first half
                    positions[i].y = Mathf.Lerp(startPosition.y + this.indicatorLineRendererHeight, startPosition.y,  Mathf.Pow(((this.indicatorLineRendererPositionCount + 1) / 2 - 1) - i, 2) / Mathf.Pow(((this.indicatorLineRendererPositionCount + 1) / 2 - 1), 2));
                }
                else
                {
                    //second half
                    positions[i].y = Mathf.Lerp(startPosition.y + this.indicatorLineRendererHeight, endPosition.y, Mathf.Pow(i - (this.indicatorLineRendererPositionCount + 1) / 2 + 1, 2) / Mathf.Pow((this.indicatorLineRendererPositionCount - 1) / 2, 2));

                }
                

            }

            this.indicatorLineRenderer.SetPositions(positions);

            //scale texture
            this.indicatorLineRenderer.material.SetTextureScale("_MainTex",new Vector2(5,0));

        }

        

    }

    void HideIndicatorLineRenderer()
    {
        if (this.indicatorLineRenderer != null)
        {
            Destroy(this.indicatorLineRenderer);
            this.indicatorLineRenderer = null;
            this.isIndicatorShowed = false;
        }

    }


}
