﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcTeleportationScript : LocomotionTeleportation {

    //teleportation
    public LayerMask collisionLayer;// layer that arc line will collide on
    private bool canTeleport = false;

    //collision simulation
    public float speed = 10;//the arc init speed
    public int maxArcCollisionSimulationTimes = 40;
    public float arcCollisionSimulationTimeUnit = 0.05f;

    //direction
    public bool shouldShowDirectionMark = true;
    public GameObject directionMarkObjectPrefab;
    private GameObject directionMarkObject = null;
    private Vector2 playerDirection = Vector2.zero;

    // Use this for initialization
    protected override void Start()
    {

        base.Start();

        this.GenerateDirectionMarkObject();

    }

    // Update is called once per frame
    void Update()
    {

        //linerenderer animation
        this.AnimateLinerenderer();

    }

    public override void OnThumbstickMoved(VRController controller, Vector2 thumbstickAxis2D)
    {
        base.OnThumbstickMoved(controller, thumbstickAxis2D);

        //move indicator position
        if (thumbstickAxis2D.magnitude > this.sensitiveTriggerThreshold && !this.isTeleporting)
        {

            //get arc init velocity
            Vector3 arcVelocity = controller.controllerObject.transform.forward * this.speed;

            this.canTeleport = this.SimulateArcCollision(arcVelocity,controller,thumbstickAxis2D);

        }
        else
        {

            if (!this.isTeleporting && this.indicatorLineRenderer != null && this.canTeleport)
            {
                this.isTeleporting = true;

                StartCoroutine(this.TeleportPlayer());

            }

            this.HideLineAndIndicator();

        }

    }

    

    /** move **/
    IEnumerator TeleportPlayer()
    {

        if (this.player != null && this.indicatorObject != null && this.indicatorObject.activeInHierarchy)
        {
            yield return StartCoroutine(this.TeleportFadeOut());


            //change rotation first
            if (this.shouldShowDirectionMark)
            {
                this.player.transform.RotateAround(new Vector3(this.vrManager.headset.transform.position.x, 0, this.vrManager.headset.transform.position.z), Vector3.up, this.CaculatePlayerRotationAngle(this.playerDirection));
            }
            
            //change position
            this.player.transform.position = this.CaculatePlayerPositionWithHeadsetDelta(this.indicatorObject.transform.position);

            //this.hideLineAndIndicator();
            yield return new WaitForSeconds(0.3f);
            yield return StartCoroutine(this.TeleportFadeIn());

            this.canTeleport = false;
            this.isTeleporting = false;
        }



    }

    protected float CaculatePlayerRotationAngle(Vector2 direction)
    {
        float deltaAngle = Vector2.SignedAngle( direction, new Vector2(this.vrManager.headset.transform.forward.x, this.vrManager.headset.transform.forward.z));

        return deltaAngle;
    }

    /** indicator **/
    bool SimulateArcCollision(Vector3 arcVelocity,VRController controller, Vector2 thumbstickAxis2D)
    {
        //velocity
        Vector2 initArcHorizontalVelocity = new Vector2(Vector3.ProjectOnPlane(arcVelocity, Vector3.up).x, Vector3.ProjectOnPlane(arcVelocity, Vector3.up).z);
        float initArcVerticalVelocity = arcVelocity.y;

        //hand position
        Vector2 horizontalHandPositiion = new Vector2(controller.controllerObject.transform.position.x, controller.controllerObject.transform.position.z);
        float verticalHandPosition = controller.controllerObject.transform.position.y;

        //vertical acceleration
        float g = 9.81f;

        //linerenderer
        List<Vector3> arcPositions = new List<Vector3>();

        //arc collision simulation
        int triedTimes = 0;
        bool isCollided = false;
        bool canTeleport = false;

        Vector2 previousArcHorizontalPositiion = horizontalHandPositiion;
        float previousArcVerticalPosition = verticalHandPosition;
        Vector3 previousArcPosition = controller.controllerObject.transform.position;

        arcPositions.Add(previousArcPosition);

        while (triedTimes<this.maxArcCollisionSimulationTimes && !isCollided)
        {
            Vector2 horizontalPosition = horizontalHandPositiion + (initArcHorizontalVelocity * triedTimes * this.arcCollisionSimulationTimeUnit);
            float verticalPosition = verticalHandPosition - 0.5f * g * (triedTimes * this.arcCollisionSimulationTimeUnit) * (triedTimes * this.arcCollisionSimulationTimeUnit) + initArcVerticalVelocity * (triedTimes * this.arcCollisionSimulationTimeUnit);
            Vector3 newPosition = new Vector3(horizontalPosition.x, verticalPosition, horizontalPosition.y);
            Vector3 newDirection = newPosition - previousArcPosition;

            Ray ray = new Ray(previousArcPosition, newDirection);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, newDirection.magnitude,this.collisionLayer))
            {
                
                if ( this.teleporationLayer == (this.teleporationLayer | (1<< hit.collider.gameObject.layer)))
                {
                    this.ShowIndicatorObject(true, hit.point, hit.normal,controller,thumbstickAxis2D);
                    canTeleport = true;

                }
                else
                {
                    this.ShowIndicatorObject(false, hit.point, hit.normal,controller,thumbstickAxis2D);
                    canTeleport = false;

                }

                arcPositions.Add(hit.point);
                isCollided = true;
                

            }
            else
            {
                this.HideIndicatorObject();
                canTeleport = false;
                arcPositions.Add(newPosition);

            }

            previousArcHorizontalPositiion = horizontalPosition;
            previousArcVerticalPosition = verticalPosition;
            previousArcPosition = newPosition;
            triedTimes = triedTimes + 1;
        }

        this.ShowIndicatorLineRenderer(arcPositions,canTeleport);

        return canTeleport;

    }

    void HideLineAndIndicator()
    {
        this.HideIndicatorObject();
        this.HideIndicatorLineRenderer();
    }

    void HideIndicatorObject()
    {
        if (this.indicatorObject != null && this.indicatorObject.activeInHierarchy)
        {
            this.indicatorObject.SetActive(false);
            this.directionMarkObject.SetActive(false);
        }
    }

    void ShowIndicatorObject(bool isValid,Vector3 position,Vector3 direction,VRController controller, Vector2 thumbstickAxis2D)
    {

        if (!this.indicatorObject.activeInHierarchy)
        {
            this.indicatorObject.SetActive(true);

            if (this.shouldShowDirectionMark)
            {
                this.directionMarkObject.SetActive(true);

            }

        }

        //set indicator position and normal
        this.indicatorObject.transform.position = position;
        this.indicatorObject.transform.up = direction;

        

        
        if (this.shouldShowDirectionMark)
        {

            //set direction mark object postion
            Vector3 worldPoint = Quaternion.AngleAxis(controller.controllerObject.transform.eulerAngles.y, Vector3.up) * new Vector3(thumbstickAxis2D.x, 0, thumbstickAxis2D.y);
            Vector2 worldThumbStickVector = new Vector2(worldPoint.x, worldPoint.z);
            Quaternion rotation = Quaternion.AngleAxis(Vector2.SignedAngle(worldThumbStickVector, Vector2.up), Vector3.up);
            this.indicatorObject.transform.rotation = this.indicatorObject.transform.rotation * rotation;

            //set player rotation angle
            Vector3 projectedDirection = Vector3.ProjectOnPlane(this.indicatorObject.transform.forward, Vector3.up);
            this.playerDirection = new Vector2(projectedDirection.x, projectedDirection.z);
        }
        

        if (isValid && this.GetIndicatorColor() == this.invalidColor)
        {

            this.SetIndicatorColor(this.validColor);//green;
        }
        else if (!isValid && this.GetIndicatorColor() == this.validColor)
        {
            this.SetIndicatorColor(this.invalidColor);//red;

        }

    }

    //Direction Object
    void GenerateDirectionMarkObject()
    {

        //this.indicatorObject = GameObject.Instantiate(this.indicatorObjectPrefab);
        if (this.directionMarkObject == null)
        {
            //setup direction object
            if (this.directionMarkObjectPrefab == null)
            {
                this.directionMarkObjectPrefab = Resources.Load<GameObject>("Prefab/TeleportationIndicator");
            }

            this.directionMarkObject = GameObject.Instantiate(this.directionMarkObjectPrefab);
            this.directionMarkObject.transform.localScale = Vector3.one * 0.3f;

            this.directionMarkObject.transform.parent = this.indicatorObject.transform;
            this.directionMarkObject.transform.localPosition = Vector3.forward * 0.3f;
            this.directionMarkObject.transform.localEulerAngles = Vector3.zero;

            //set color
            this.SetIndicatorColor(this.validColor);

            //set inactive
            this.directionMarkObject.SetActive(false);
        }


    }

    /** line renderer **/
    void ShowIndicatorLineRenderer(List<Vector3> positions,bool isValid)
    {
        if (this.indicatorLineRenderer == null)
        {
            this.indicatorLineRenderer = this.gameObject.AddComponent<LineRenderer>();
            this.indicatorLineRenderer.positionCount = positions.Count;
            this.indicatorLineRenderer.textureMode = LineTextureMode.Tile;

            if (isValid)
            {
                this.indicatorLineRenderer.material = Resources.Load<Material>("Materials/TeleportationLineMaterial");
                this.indicatorLineRenderer.startColor = this.validColor;
                this.indicatorLineRenderer.endColor = this.validColor;

            }
            else
            {
                this.indicatorLineRenderer.material = Resources.Load<Material>("Materials/ErrorTeleportationLineMaterial");
                this.indicatorLineRenderer.startColor = this.invalidColor;
                this.indicatorLineRenderer.endColor = this.invalidColor;

            }

            
            this.indicatorLineRenderer.startWidth = 0.01f;
            this.indicatorLineRenderer.endWidth = 0.01f;
        }
        else
        {
            if (isValid && this.indicatorLineRenderer.startColor == this.invalidColor)
            {
                this.indicatorLineRenderer.material = Resources.Load<Material>("Materials/TeleportationLineMaterial");
                this.indicatorLineRenderer.startColor = this.validColor;
            }
            else if (!isValid && this.indicatorLineRenderer.startColor == this.validColor)
            {
                this.indicatorLineRenderer.material = Resources.Load<Material>("Materials/ErrorTeleportationLineMaterial");
                this.indicatorLineRenderer.startColor = this.invalidColor;

            }

            this.indicatorLineRenderer.positionCount = positions.Count;
        }


        //set positions
        this.indicatorLineRenderer.SetPositions(positions.ToArray());

        //scale texture
        this.indicatorLineRenderer.material.SetTextureScale("_MainTex", new Vector2(5, 0));

    }

    void HideIndicatorLineRenderer()
    {
        if (this.indicatorLineRenderer != null)
        {
            Destroy(this.indicatorLineRenderer);
            this.indicatorLineRenderer = null;
        }

    }


}
