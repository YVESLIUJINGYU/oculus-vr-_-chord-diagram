﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocomotionTeleportation : LocomotionBase {

    public LayerMask teleporationLayer;//valid layer user can teleport to
    public Color fadeColor = Color.black;
    public float fadeDuration = 0.5f;
    public float lineRendererSpeed = 10.0f;
    public GameObject indicatorObjectPrefab;
    

    //teleportation
    protected bool isTeleporting = false;
    public GameObject teleportationSphere;

    //indicator
    protected GameObject indicatorObject = null;
    protected Vector3 indicatorObjectNormal = Vector3.up;
    protected Color validColor = new Color32(0x4C, 0xFF, 0x70, 0x8F);//green;
    protected Color invalidColor = new Color32(0xFF, 0x00, 0x00, 0x8F);//red;

    //line render
    protected LineRenderer indicatorLineRenderer = null;

    // Use this for initialization
    protected override void Start() {

        base.Start();

        this.GenerateIndicatorObject();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    /** Fading **/
    protected IEnumerator TeleportFadeOut()
    {
        Material fadeMaterial = this.teleportationSphere.GetComponent<Renderer>().sharedMaterials[0];
        fadeMaterial.color = Color.clear;
        Color beginColor = fadeMaterial.color;

        for (float t = 0.0f; t <= this.fadeDuration; t = t + Time.deltaTime)
        {
            fadeMaterial.color = Color.Lerp(Color.clear, Color.black, t / this.fadeDuration);
            yield return null;
        }

        fadeMaterial.color = Color.black;

    }

    protected IEnumerator TeleportFadeIn()
    {
        Material fadeMaterial = this.teleportationSphere.GetComponent<Renderer>().sharedMaterials[0];
        fadeMaterial.color = Color.clear;
        Color beginColor = fadeMaterial.color;

        for (float t = 0.0f; t <= this.fadeDuration; t = t + Time.deltaTime)
        {
            fadeMaterial.color = Color.Lerp(Color.black, Color.clear, t / this.fadeDuration);
            yield return null;
        }

        fadeMaterial.color = Color.clear;

    }

    /** teleportation **/
    protected Vector3 CaculatePlayerPositionWithHeadsetDelta(Vector3 indicatorPosition)
    {
        Vector3 headsetPositionDelta = this.vrManager.headset.transform.position - this.player.transform.position;

        return indicatorPosition - new Vector3(headsetPositionDelta.x,0, headsetPositionDelta.z);

        //return indicatorPosition - new Vector3(this.vrManager.headset.transform.localPosition.x,0, this.vrManager.headset.transform.localPosition.z);
    }


    /** Indicator **/
    protected void GenerateIndicatorObject()
    {

        if (this.indicatorObject == null)
        {
            //setup direction object
            if (this.indicatorObjectPrefab == null)
            {
                this.indicatorObjectPrefab = Resources.Load<GameObject>("Prefab/TeleportationIndicator");
            }

            this.indicatorObject = GameObject.Instantiate(this.indicatorObjectPrefab);

            //set color
            this.SetIndicatorColor(this.validColor);

            //set inactive
            this.indicatorObject.SetActive(false);

        }

    }

    protected void SetIndicatorColor(Color color)
    {
        //this.indicatorObject.GetComponentInChildren<Renderer>().material.color = color;

        foreach (var render in this.indicatorObject.GetComponentsInChildren<Renderer>())
        {
            render.material.color = color;
        }

        
    }

    protected Color GetIndicatorColor()
    {
        return this.indicatorObject.GetComponentInChildren<Renderer>().material.color;
    }

    /** line renderer **/
    protected void AnimateLinerenderer()
    {

        if (this.indicatorLineRenderer != null)
        {
            float offset = -Time.time * this.lineRendererSpeed;
            this.indicatorLineRenderer.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));

        }

    }
}
