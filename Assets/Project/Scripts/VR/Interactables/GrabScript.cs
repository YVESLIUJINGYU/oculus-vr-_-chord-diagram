﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum GrabFixType
{
    Parent,
    PositionMatching
}

public class GrabScript : InteractableOneHand {

    [Header("Grab Setup")]
    public bool isUsingPhysics = true;
    //for PositonMatching GrabType isTriggerWhenGrabbed should always set to False
    public bool isTriggerWhenGrabbed = true;//if true then will not collider with others,if false then can collide with others
    public GrabFixType grabFixType = GrabFixType.Parent;
    public bool shouldHoldButtonToKeepGrabbed = true;
    protected bool isUsed = false;//if this object has been grabbed,then set to true.if back to original,then set back to false
    protected bool isGrabbed = false;//if this object is truely grabbed in hand, not in the middle of air(isusing object can be in the middle of air when distence grab)
    protected Vector3 grabPosition = Vector3.zero;
    protected Vector3 grabAngel = Vector3.zero;


    //parent if false then release to world null
    public bool shouldReleaseToParentTransform = false;
    private Transform parentTransform = null;
    public bool shouldKeepTrigger = false;
    private bool isStartFromTrigger = false;

    //snap
    [Header("Snap Setup")]
    public bool isSnap = false;
    public OffsetConfiguration snapOffset;
    //public Vector3 snapPosition = Vector3.zero;//just for right hand
    //public Vector3 snapAngel = Vector3.zero;
    public float snapSpeed = 0;//if equal zero, it means instant snap
    public bool shouldStopVelocityWhenSnapStop = true;
    private float minDistanceToSnap = 0.3f;

    //grab item mark
    [Header("Grab Item Mark")]
    public bool shouldShowItemMark = false;
    public bool shouldShowItemMarkAtFirst = true;
    public Vector3 markOffset = Vector3.zero;
    public Color markColor = new Color(74f / 255f, 253f / 255f, 76f / 255f);
    public float markHeight = 0.8f;
    public float markWidth = 0.4f;
    private ItemMark itemMark;

    //speed
    private bool isStartPosition = false;
    private bool isStartAngular = false;
    private List<KeyValuePair<Vector3, float>> previousPositionList = new List<KeyValuePair<Vector3, float>>();
    private List<KeyValuePair<Quaternion, float>> previousAngularnList = new List<KeyValuePair<Quaternion, float>>();

    // Use this for initialization
    protected override void Start () {

        base.Start();

        //this.rig = gameObject.GetComponent<Rigidbody>();
        //this.col = gameObject.GetComponent<Collider>();

        this.parentTransform = this.transform.parent;
        this.isStartFromTrigger = this.col.isTrigger;

        if (this.snapOffset == null)
        {
            this.snapOffset = ScriptableObject.CreateInstance<OffsetConfiguration>();
            this.snapOffset.offsetPosition = Vector3.zero;
            this.snapOffset.offsetAngel = Vector3.zero;
        }

        if (this.shouldShowItemMark)
        {
            this.itemMark = GameObject.Instantiate(Resources.Load<GameObject>("Prefab/ItemMark")).GetComponent<ItemMark>();

            if (this.itemMark != null)
            {

                this.itemMark.ConfigureItemMark(this.markColor, this.markHeight, this.markWidth);

                this.itemMark.transform.parent = this.transform;

                //set offset
                this.itemMark.transform.localPosition = Vector3.zero;
                this.itemMark.transform.position = this.itemMark.transform.position + this.markOffset;

                this.itemMark.gameObject.SetActive(this.shouldShowItemMarkAtFirst);
            }

            

        }

    }

    protected virtual void FixedUpdate()
    {
        if (this.isGrabbed && this.grabFixType == GrabFixType.PositionMatching)
        {

            if (this.isSnap)
            {
                this.rig.MovePosition(this.CalculateSnapPosition(this.snapOffset.offsetPosition));
                this.rig.MoveRotation(Quaternion.Euler(this.CalculateSnapAngel(this.snapOffset.offsetAngel)));
            }
            else
            {
                this.rig.MovePosition(this.CalculateSnapPosition(this.grabPosition));
                this.rig.MoveRotation(Quaternion.Euler(this.CalculateSnapAngel(this.grabAngel)));
            }
            
        }
    }

    // Update is called once per frame
    protected virtual void Update() {

        //update position sample list
        this.UpdatePositionList(Time.deltaTime);

        //update rotation sample list
        this.UpdateRotationList(Time.deltaTime);

        //TODO :check this logic
        /*
        //not raycasted to release
        if (!this.isRayCastered && this.isDistanceToInteract && this.primaryController != null && !this.isTriggered && this.isUsing)
        {
            this.ReleaseObject(this.primaryController, this.ReleaseFunction,this.AfterChangePrimaryControllerFunction);
            this.ExitCollider(this.primaryController, this.ExitFunction, this.AfterChangePrimaryControllerFunction);


        }
        */

        //item mark
        if (this.itemMark != null && this.itemMark.gameObject.activeSelf)
        {
            this.itemMark.transform.LookAt(Vector3.forward);
        }

    }

    public override void OnInteractableIndexPressed(VRController controller, float indexAxis)
    {
        base.OnInteractableIndexPressed(controller, indexAxis);

        if (this.grabType == GrabType.IndexButton && this.shouldHoldButtonToKeepGrabbed)
        {
            this.RunGrabLogicForHolding(controller, indexAxis);
        }

    }


    public override void OnInteractableGrabPressed(VRController controller,float grabAxis)
    {
        base.OnInteractableGrabPressed(controller,grabAxis);

        if (this.grabType == GrabType.GrabButton && this.shouldHoldButtonToKeepGrabbed)
        {
            this.RunGrabLogicForHolding(controller, grabAxis);
        }

    }


    public override void OnInteractableIndexDown(VRController controller)
    {
        base.OnInteractableIndexDown(controller);

        if (this.grabType == GrabType.IndexButton && !this.shouldHoldButtonToKeepGrabbed)
        {
            this.RunGrabLogic(controller);
        }

    }

    public override void OnInteractableGrabDown(VRController controller)
    {
        base.OnInteractableGrabDown(controller);

        if (this.grabType == GrabType.GrabButton && !this.shouldHoldButtonToKeepGrabbed)
        {
            this.RunGrabLogic(controller);
        }

    }

    //override
    protected override void AfterChangePrimaryControllerFunction()
    {
        base.AfterChangePrimaryControllerFunction();

        this.isGrabbed = false;

    }

    protected override void ExitFunction()
    {
        base.ExitFunction();

        this.isGrabbed = false;

        if (this.isUsed)
        {
            if (this.shouldKeepTrigger)
            {
                this.col.isTrigger = this.isStartFromTrigger;
            }
            else
            {
                this.col.isTrigger = false;
            }

            

            if (this.shouldReleaseToParentTransform)
            {
                transform.parent = this.parentTransform;
            }
            else
            {
                transform.parent = null;
            }
            
            this.rig.useGravity = this.isUsingPhysics;

        }

    }

    protected override void ReleaseFunction()
    {
        base.ReleaseFunction();

        this.isGrabbed = false;

        if (this.shouldKeepTrigger)
        {
            this.col.isTrigger = this.isStartFromTrigger;
        }
        else
        {
            this.col.isTrigger = false;
        }

        if (this.shouldReleaseToParentTransform)
        {
            transform.parent = this.parentTransform;
        }
        else
        {
            transform.parent = null;
        }

        this.rig.useGravity = this.isUsingPhysics;

        if (this.isUsingPhysics)
        {

            //set speed
            Vector3 velocity = this.getAverageVelocity();

            Vector3 angularVelocity = this.getAverageAngularVelocity();


            /*
            var deltaRot = transform.rotation * Quaternion.Inverse(this.previousHandAngular);
            var eulerRot = new Vector3(Mathf.DeltaAngle(0, deltaRot.eulerAngles.x), Mathf.DeltaAngle(0, deltaRot.eulerAngles.y), Mathf.DeltaAngle(0, deltaRot.eulerAngles.z));
            Vector3 angularVelocity = eulerRot / Time.deltaTime;
            */
            //this.rigidbody.angularVelocity = angularVelocity;
            //this.rigidbody.velocity = velocity;
            this.rig.AddForce(velocity, ForceMode.VelocityChange);
            this.rig.AddTorque(angularVelocity, ForceMode.VelocityChange);
            //this.currentController = null;

        }

        //item mark
        if (this.itemMark != null)
        {
            this.itemMark.gameObject.SetActive(true);
        }

    }


    //helper
    void RunGrabLogic(VRController controller)
    {
        //release
        if ((this.primaryController != null && this.primaryController == controller) && this.isTriggered && this.isUsing)
        {
            this.ReleaseObject(controller, this.ReleaseFunction, this.AfterChangePrimaryControllerFunction);
            this.ExitCollider(controller, this.ExitFunction, this.AfterChangePrimaryControllerFunction);

        }

        //distance to release
        if (this.isDistanceToInteract && (this.primaryController != null && this.primaryController == controller) && this.isRayCastered && this.isUsing)
        {
            this.ReleaseObject(controller, this.ReleaseFunction, this.AfterChangePrimaryControllerFunction);
            this.ExitCollider(controller, this.ExitFunction, this.AfterChangePrimaryControllerFunction);

        }


        //grab
        if ((this.primaryController != null && this.primaryController == controller) && this.isTriggered && !this.isUsing)
        {
            this.GrabObject();

        }


        //distance to grab
        if (this.isDistanceToInteract && (this.primaryController != null && this.primaryController == controller) && this.isRayCastered && !this.isUsing)
        {

            this.GrabObject();

        }
    }

    void RunGrabLogicForHolding(VRController controller, float grabAxis)
    {
        //update position
        //this.UpdatePositionList(Time.deltaTime);

        //update rotation
        //this.UpdateRotationList(Time.deltaTime);

        //release
        if ((this.primaryController != null && this.primaryController == controller) && grabAxis <= this.sensitiveReleaseThreshold && this.isTriggered && this.isUsing)
        {
            this.ReleaseObject(controller, this.ReleaseFunction, this.AfterChangePrimaryControllerFunction);
            this.ExitCollider(controller, this.ExitFunction, this.AfterChangePrimaryControllerFunction);

        }

        //distance to release
        if (this.isDistanceToInteract && (this.primaryController != null && this.primaryController == controller) && grabAxis <= this.sensitiveReleaseThreshold && !this.isTriggered && this.isUsing)
        {
            this.ReleaseObject(controller, this.ReleaseFunction, this.AfterChangePrimaryControllerFunction);
            this.ExitCollider(controller, this.ExitFunction, this.AfterChangePrimaryControllerFunction);

        }


        //grab
        if ((this.primaryController != null && this.primaryController == controller) && this.isTriggered && grabAxis > this.sensitiveReleaseThreshold && !this.isUsing)
        {
            this.GrabObject();

        }


        //distance to grab
        if (this.isDistanceToInteract && (this.primaryController != null && this.primaryController == controller) && this.isRayCastered && grabAxis > this.sensitiveReleaseThreshold && !this.isUsing)
        {

            this.GrabObject();

        }
    }


    void FixGrabbedToController()
    {
        this.isGrabbed = true;

        //clear sample list
        this.previousAngularnList.Clear();
        this.previousPositionList.Clear();

        //parent
        if (this.grabFixType == GrabFixType.Parent && transform.parent != this.primaryController.controllerObject.transform)
        {
            transform.parent = this.primaryController.controllerObject.transform;
        }

        //snap
        if (this.grabFixType == GrabFixType.Parent && this.isSnap)
        {
            this.SetSnapPosition();
        }

        //Position Matching
        if (this.grabFixType == GrabFixType.PositionMatching)
        {
            transform.parent = this.primaryController.controllerObject.transform;

            //grab postion
            this.grabPosition = transform.localPosition;
            this.grabAngel = transform.localEulerAngles;

            transform.parent = null;

        }


    }


    protected virtual void GrabObject()
    {
        this.isUsed = true;

        this.isUsing = true;
        
        this.rig.isKinematic = true;

        this.col.isTrigger = this.isTriggerWhenGrabbed;

        if (this.isSnap)
        {
            if (this.snapSpeed == 0)
            {
                this.FixGrabbedToController();
                //this.SetSnapPosition();

            }
            else
            {
                StartCoroutine(this.SnapToController());
            }
            
        }
        else
        {
            this.FixGrabbedToController();
        }

        //item mark
        if (this.itemMark != null)
        {
            this.itemMark.gameObject.SetActive(false);
        }

    }

    
    //sample speed
    bool ShouldSampleSpeed()
    {
        return (!this.shouldStopVelocityWhenSnapStop || (this.primaryController != null && transform.parent == this.primaryController.controllerObject.transform));
    }

    void UpdatePositionList(float deltTime)
    {
        if (this.ShouldSampleSpeed())
        {
            if (this.previousPositionList.Count < 5)
            {
                this.previousPositionList.Add(new KeyValuePair<Vector3, float>(this.transform.position, deltTime));
            }
            else
            {

                this.previousPositionList.RemoveAt(0);
                this.previousPositionList.Add(new KeyValuePair<Vector3, float>(this.transform.position, deltTime));

            }
        }

        
        

    }

    void UpdateRotationList(float deltTime)
    {
        if (this.ShouldSampleSpeed())
        {
            if (this.previousAngularnList.Count < 2)
            {
                this.previousAngularnList.Add(new KeyValuePair<Quaternion, float>(this.transform.rotation, deltTime));
            }
            else
            {

                this.previousAngularnList.RemoveAt(0);
                this.previousAngularnList.Add(new KeyValuePair<Quaternion, float>(this.transform.rotation, deltTime));

            }

        }

        

    }


    //smooth the angular velocity
    Vector3 getAverageAngularVelocity()
    {
        if (this.previousAngularnList.Count <= 1)
        {
            return Vector3.zero;
        }
        else
        {
            Quaternion previousHandAngular = this.previousAngularnList[0].Key;
            Quaternion currentHandAngular = this.previousAngularnList[1].Key;

            float angleInDegrees;
            Vector3 rotationAxis;
            var deltaRot = currentHandAngular * Quaternion.Inverse(previousHandAngular);
            deltaRot.ToAngleAxis(out angleInDegrees, out rotationAxis);
            Vector3 angularVelocity = (rotationAxis * angleInDegrees) * Mathf.Deg2Rad / Time.deltaTime;

            return angularVelocity;

        }
    }

    //smooth the velocity
    Vector3 getAverageVelocity()
    {
        if (this.previousPositionList.Count <= 1)
        {
            return Vector3.zero;
        }
        else
        {
            float accumulatedDistance = 0;
            float accumulatedDeltaTime = 0;

            for (int i = 0; i < this.previousPositionList.Count; i++)
            {
                if (i < this.previousPositionList.Count - 1)
                {
                    var distance = Vector3.Distance(this.previousPositionList[i + 1].Key, this.previousPositionList[i].Key);
                    accumulatedDistance = accumulatedDistance + distance;
                    accumulatedDeltaTime = accumulatedDeltaTime + this.previousPositionList[i + 1].Value;

                }
                
            }

            //last direction * average velocity
            Vector3 velocity = (this.previousPositionList[this.previousPositionList.Count-1].Key - this.previousPositionList[this.previousPositionList.Count - 2].Key).normalized * accumulatedDistance / accumulatedDeltaTime;

            return velocity;

        }
        
    }

    //snap
    protected Vector3 CalculateSnapPosition(Vector3 snapPosition)
    {
        var tempObject = new GameObject();

        tempObject.transform.parent = this.primaryController.controllerObject.transform;

        //if is snap then left and right are different in x
        if (!this.isSnap || (this.primaryController != null && this.primaryController.controllerType == VRControllerTypes.Right))
        {
            tempObject.transform.localPosition = snapPosition;
        }
        else
        {
            tempObject.transform.localPosition = new Vector3(-snapPosition.x, snapPosition.y, snapPosition.z);
        }

        var tempPostion = tempObject.transform.position;

        Destroy(tempObject);

        return tempPostion;


    }

    protected Vector3 CalculateSnapAngel(Vector3 snapAngel)
    {
        var tempObject = new GameObject();

        tempObject.transform.parent = this.primaryController.controllerObject.transform;

        //if is snap then left and right are different in x
        if (!this.isSnap || (this.primaryController != null && this.primaryController.controllerType == VRControllerTypes.Right))
        {
            tempObject.transform.localEulerAngles = snapAngel;
        }
        else
        {
            tempObject.transform.localEulerAngles = new Vector3(snapAngel.x, -snapAngel.y, -snapAngel.z);
        }

        var tempAngle = tempObject.transform.eulerAngles;

        Destroy(tempObject);

        return tempAngle;


    }

    private void SetSnapPosition()
    {
        
        //left and right are different in x
        if (this.primaryController != null && this.primaryController.controllerType == VRControllerTypes.Right)
        {
            transform.localPosition = this.snapOffset.offsetPosition;
            transform.localEulerAngles = this.snapOffset.offsetAngel;
        }
        else
        {
            transform.localPosition = new Vector3(-this.snapOffset.offsetPosition.x, this.snapOffset.offsetPosition.y, this.snapOffset.offsetPosition.z);
            transform.localEulerAngles = new Vector3(this.snapOffset.offsetAngel.x, -this.snapOffset.offsetAngel.y, -this.snapOffset.offsetAngel.z);
        }
    }

    private IEnumerator SnapToController()
    {

        while (((this.snapSpeed * Time.deltaTime)< this.CurrentSnapDistance() || this.minDistanceToSnap < this.CurrentSnapDistance()) && this.isUsing)
        {
            
            this.gameObject.transform.position = this.gameObject.transform.position + this.CurrentSnapDirection() * this.snapSpeed * Time.deltaTime;
            this.gameObject.transform.eulerAngles = this.primaryController.gameObject.transform.eulerAngles + this.snapOffset.offsetAngel;
            yield return null;
        }

        //final step
        if (this.isUsing)
        {
            this.FixGrabbedToController();

            //this.SetSnapPosition();

        }

    }

    float CurrentSnapDistance()
    {
        //left hand and right hand is different
        if (this.primaryController != null && this.primaryController.controllerType == VRControllerTypes.Right)
        {
            return ((this.primaryController.gameObject.transform.position + this.snapOffset.offsetPosition) - this.gameObject.transform.position).magnitude;
        }
        else
        {
            return ((this.primaryController.gameObject.transform.position + new Vector3(-this.snapOffset.offsetPosition.x, this.snapOffset.offsetPosition.y, this.snapOffset.offsetPosition.z)) - this.gameObject.transform.position).magnitude;
        }

    }

    Vector3 CurrentSnapDirection()
    {

        //left hand and right hand is different
        if (this.primaryController != null && this.primaryController.controllerType == VRControllerTypes.Right)
        {
            return ((this.primaryController.gameObject.transform.position + this.snapOffset.offsetPosition) - this.gameObject.transform.position).normalized;
        }
        else
        {
            return ((this.primaryController.gameObject.transform.position + new Vector3(-this.snapOffset.offsetPosition.x, this.snapOffset.offsetPosition.y, this.snapOffset.offsetPosition.z)) - this.gameObject.transform.position).normalized;
        }

    }

}
