﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableTwoHands : InteractableBase {


    // Use this for initialization
    protected override void Start()
    {

        base.Start();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void OnInteractableEnter(VRController controller)
    {
        base.OnInteractableEnter(controller);

        this.isTriggered = true;

        if (this.primaryController == null)
        {
            this.primaryController = controller;
            this.VibrateController(controller);
        }
        else if (this.secondaryController == null)
        {

            this.secondaryController = controller;
            this.VibrateController(controller);

        }

    }
}
