﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : GrabScript {

    [HideInInspector]
    public VRPlayer shooter;
    [HideInInspector]
    public Gun gun;
    [Header("Bullet Setup")]
    public float maxFlyDistance = 30f;

    [HideInInspector]
    public bool isFlying = false;
    private float flyDistance = 0f;
    private float bulletSpeed = 0f;

    // Use this for initialization
    protected override void Start()
    {

        base.Start();

    }

    // Update is called once per frame
    protected override void Update()
    {

        base.Update();

        //update distance
        if (this.bulletSpeed > 0 && this.gameObject.activeSelf && this.isFlying && this.flyDistance < this.maxFlyDistance)
        {
            this.flyDistance = this.flyDistance + this.bulletSpeed * Time.deltaTime;

        }

        //when bullet to max distance
        if (this.gameObject.activeSelf && this.isFlying && this.flyDistance >= this.maxFlyDistance)
        {
            this.gameObject.SetActive(false);
            this.isFlying = false;

        }

    }

    //collid
    private void OnCollisionEnter(Collision collision)
    {
        GameObject hitObject = collision.gameObject;
        Vector3 hitPoint = collision.contacts[0].point;

        if (hitObject != this.gun.gameObject && hitObject != this.shooter.gameObject && this.isFlying)
        {
            this.Hit(hitObject);
            this.Explode(hitPoint);
        }
    }

    //override
    protected override void GrabObject()
    {
        base.GrabObject();

        this.isFlying = false;

    }


    //fly
    public void Fly(Vector3 shotPosition, Vector3 shotAngel, Vector3 shotDirection,Gun gun,VRPlayer shooter,float speed)
    {
        this.gameObject.SetActive(true);

        this.gun = gun;
        this.shooter = shooter;
        this.bulletSpeed = speed;
        this.gameObject.transform.position = shotPosition;
        this.gameObject.transform.eulerAngles = shotAngel;
        this.flyDistance = 0;
        this.isFlying = true;

        this.rig.AddForce(this.bulletSpeed*shotDirection, ForceMode.VelocityChange);
        this.rig.AddTorque(Vector3.zero, ForceMode.VelocityChange);

    }

    //helper
    public void Reset()
    {
        this.rig.isKinematic = false;
        this.rig.useGravity = false;
        this.col.isTrigger = false;


        this.isUsed = false;
        this.isFlying = false;

        this.transform.position = Vector3.zero;
        this.transform.eulerAngles = Vector3.zero;
        this.rig.velocity = Vector3.zero;
        this.rig.angularVelocity = Vector3.zero;

    }

    private void Hit(GameObject hitObject)
    {
        this.isFlying = false;

        this.isUsed = true;

        this.ExitFunction();
    }

    private void Explode(Vector3 explodePosition)
    {

    }



}
