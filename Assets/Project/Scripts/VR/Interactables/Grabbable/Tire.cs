﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tire : GrabScript {

    [Header("Tire Setup")]
    public Transform originalParent;
    private Vector3 originalPosition;
    private Quaternion originalRotation;

    // Use this for initialization
    protected override void Start()
    {

        base.Start();

        this.originalPosition = this.transform.position;
        this.originalRotation = this.transform.rotation;
        this.originalParent = this.transform.parent;

    }

    // Update is called once per frame
    protected override void Update()
    {

        base.Update();

    }

    public override void OnInteractableIndexDown(VRController controller)
    {
        base.OnInteractableGrabDown(controller);

        //back to position
        if (/*this.currentController == null*/ !this.isUsing && !this.isTriggered && this.isUsed && !isRayCastered)
        {
            this.BackToOriginalPosition();

        }
    }

    //override
    protected override void ReleaseFunction()
    {
        base.ReleaseFunction();

        /*
        this.col.isTrigger = false;

        if (this.isUsingPhysics)
        {
            this.rig.useGravity = true;
            transform.parent = null;

            //set speed
            Vector3 velocity = this.getAverageVelocity();

            Vector3 angularVelocity = this.getAverageAngularVelocity();



            //this.rigidbody.angularVelocity = angularVelocity;
            //this.rigidbody.velocity = velocity;
            this.rig.AddForce(velocity, ForceMode.VelocityChange);
            this.rig.AddTorque(angularVelocity, ForceMode.VelocityChange);
            //this.currentController = null;

        }
        else
        {
            this.BackToOriginalPosition();
        }
        */

        if (!this.isUsingPhysics)
        {
            this.BackToOriginalPosition();

        }

    }

    protected override void ExitFunction()
    {
        base.ExitFunction();

        /*
        if (this.isUsed)
        {
            this.col.isTrigger = false;

            if (this.isUsingPhysics)
            {
                this.rig.useGravity = true;
                transform.parent = null;

            }
            else
            {
                this.BackToOriginalPosition();

            }
        }
        */

        if (this.isUsed)
        {
            if (!this.isUsingPhysics)
            {
                this.BackToOriginalPosition();

            }

        }

    }

    //helper
    void BackToOriginalPosition()
    {
        this.isUsed = false;

        this.rig.isKinematic = true;
        this.col.isTrigger = false;
        this.rig.useGravity = false;
        this.transform.parent = this.originalParent;
        this.rig.MovePosition(this.originalPosition);
        this.rig.MoveRotation(this.originalRotation);
    }
}
