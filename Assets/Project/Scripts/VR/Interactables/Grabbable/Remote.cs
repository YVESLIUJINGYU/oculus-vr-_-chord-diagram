﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Remote : GrabScript {

    [Header("Remote Setup")]
    public VehicleBase vehicle;

    // Use this for initialization
    protected override void Start()
    {

        base.Start();

    }

    // Update is called once per frame
    protected override void Update() {

        base.Update();

	}

    //override
    public override void OnInteractableButtonOnePressed(VRController controller, bool isPressed)
    {
        base.OnInteractableButtonOnePressed(controller, isPressed);

        if (this.vehicle != null && isPressed)
        {
            this.vehicle.currentDescend = -1;
        }
        else
        {
            this.vehicle.currentDescend = 0;
        }
    }

    public override void OnInteractableButtonTwoPressed(VRController controller, bool isPressed)
    {
        base.OnInteractableButtonTwoPressed(controller,isPressed);

        if (this.vehicle != null && isPressed)
        {
            this.vehicle.currentAscend = 1;
        }
        else
        {
            this.vehicle.currentAscend = 0;
        }

    }

    public override void OnInteractableButtonThreePressed(VRController controller, bool isPressed)
    {
        base.OnInteractableButtonThreePressed(controller,isPressed);

        if (this.vehicle != null && isPressed)
        {
            this.vehicle.currentDescend = -1;
        }
        else
        {
            this.vehicle.currentDescend = 0;
        }
    }

    public override void OnInteractableButtonFourPressed(VRController controller, bool isPressed)
    {
        base.OnInteractableButtonFourPressed(controller, isPressed);

        if (this.vehicle != null && isPressed)
        {
            this.vehicle.currentAscend = 1;
        }
        else
        {
            this.vehicle.currentAscend = 0;
        }

    }


    //joystick
    public virtual void OnThumbstickTouched(VRController controller, bool isTouched)
    {


    }

    public virtual void OnThumbstickMoved(VRController controller, Vector2 thumbstickAxis2D)
    {

        if (this.isGrabbed && (controller == this.primaryController))
        {
            if (this.vehicle != null)
            {
                if (thumbstickAxis2D.magnitude > this.vehicle.thumbstickSensitiveThreshold)
                {
                    this.vehicle.currentAxis = thumbstickAxis2D;
                }
                else
                {
                    this.vehicle.currentAxis = Vector3.zero;
                }
 
            }

        }

    }

}
