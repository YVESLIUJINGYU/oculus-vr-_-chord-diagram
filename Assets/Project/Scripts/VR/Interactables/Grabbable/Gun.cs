﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : GrabScript {

    [Header("Gun Setup")]
    public GameObject bulletPrefab;
    public float speed = 1f;
    public float maxFlyDistance = 30f;
    public int maxBulletNumberInFlying = 10;
    public float minInterval = 0.5f;

    //bullet offset
    [Header("Bullet Offset")]
    public OffsetConfiguration bulletOffset;


    private List<GameObject> bulletsPool;
    private float shotInterval = 0f;

    // Use this for initialization
    protected override void Start () {

        base.Start();

        this.CreateBulletsPool();
		
	}
	
	// Update is called once per frame
	protected override void Update () {

        base.Update();

        this.shotInterval += Time.deltaTime;

	}

    //trigger the gun
    public override void OnInteractableIndexDown(VRController controller)
    {
        base.OnInteractableIndexDown(controller);

        //shot
        if (/*this.isUsing && this.isTriggered*/this.isGrabbed && (controller == this.primaryController) && this.shotInterval >= this.minInterval)
        {
            this.Shot();
            this.shotInterval = 0;

        }
    }

    //bullets pool munipulation
    Bullet GetBulletFromPool()
    {
        for (int i = 0; i < this.maxBulletNumberInFlying; i++)
        {
            var bulletObject = this.bulletsPool[i];
            var bullet = bulletObject.GetComponent<Bullet>();
            
            if (!bullet.gameObject.activeSelf)
            {
                bullet.Reset();
                bullet.maxFlyDistance = this.maxFlyDistance;
                return bullet;
            }

        }


        for (int i = 0; i < this.maxBulletNumberInFlying; i++)
        {
            var bulletObject = this.bulletsPool[i];
            var bullet = bulletObject.GetComponent<Bullet>();

            if (!bullet.isFlying && !bullet.isUsing && !bullet.isTriggered && !bullet.isRayCastered)
            {
                bullet.Reset();
                bullet.maxFlyDistance = this.maxFlyDistance;
                return bullet;
            }
            
        }

        return null;
    }

    void CreateBulletsPool()
    {
        this.bulletsPool = new List<GameObject>();

        for (int i = 0; i < this.maxBulletNumberInFlying; i++)
        {
            var bulletObject = Instantiate(this.bulletPrefab);
            bulletObject.SetActive(false);
            this.bulletsPool.Add(bulletObject);
        }

    }

    //helper
    void Shot()
    {
        var bullet = this.GetBulletFromPool();

        if (bullet != null)
        {
            bullet.gameObject.SetActive(true);
            bullet.isFlying = true;

            bullet.Fly(this.GetBulletPosition(bullet), this.GetBulletAngule(bullet), this.primaryController.transform.forward, this, this.GetShooter(), this.speed);

            this.VibrateController(this.primaryController, 40, 50);

        }

    }

    Vector3 GetBulletAngule(Bullet bullet)
    {
        bullet.transform.parent = this.primaryController.controllerObject.transform;
        bullet.transform.localEulerAngles = this.bulletOffset.offsetAngel;
        Vector3 angle = bullet.transform.eulerAngles;
        bullet.transform.parent = null;

        return angle;
    }

    Vector3 GetBulletPosition(Bullet bullet)
    {

        bullet.transform.parent = this.primaryController.controllerObject.transform;
        
        //right and left hands are different
        if (this.primaryController != null && this.primaryController.controllerType == VRControllerTypes.Right)
        {
            bullet.transform.localPosition = this.bulletOffset.offsetPosition;
        }
        else
        {
            bullet.transform.localPosition = new Vector3(-this.bulletOffset.offsetPosition.x, this.bulletOffset.offsetPosition.y, this.bulletOffset.offsetPosition.z);
        }
        
        Vector3 position = bullet.transform.position;
        bullet.transform.parent = null;

        return position;

    }

    VRPlayer GetShooter()
    {
        if (this.primaryController == null)
        {
            return null;
        }
        else
        {
            return this.primaryController.GetPlayer();
        }
    }

}
