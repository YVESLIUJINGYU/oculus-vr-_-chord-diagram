﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMark : MonoBehaviour {

    public ParticleSystem post;
    public ParticleSystem ball;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ConfigureItemMark(Color color,float height,float width)
    {
        if (this.post != null)
        {
            var main = this.post.main;

            main.startColor = color;

            main.startLifetime = height;

        }

        if (this.ball != null)
        {
            var main = this.ball.main;

            main.startColor = color;

            main.startSize = width;
        }

    }

}
