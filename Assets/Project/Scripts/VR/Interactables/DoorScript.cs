﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HingeJoint))]
public class DoorScript : InteractableOneHand {

    private float previousHandYAngel = 0;
    private float doorAngularVelocity = 0;
    private Vector3 hingeJointPosition;
    private Vector3 originalDoorPosition;
    private HingeJoint hinge;
    private float maxAngel;
    private float minAngel;


    // Use this for initialization
    protected override void Start()
    {

        base.Start();

        this.hinge = gameObject.GetComponent<HingeJoint>();
        //this.rig = gameObject.GetComponent<Rigidbody>();
        this.hingeJointPosition = transform.TransformPoint(this.hinge.anchor);
        this.originalDoorPosition = transform.position;
        this.maxAngel = this.hinge.limits.max;
        this.minAngel = this.hinge.limits.min;

    }

    // Update is called once per frame
    void Update() {


    }

    public override void OnInteractableIndexPressed(VRController controller, float indexAxis)
    {
        base.OnInteractableIndexPressed(controller, indexAxis);

        if (this.grabType == GrabType.IndexButton)
        {
            this.RunOpenDoorLogic(controller, indexAxis);
        }

    }

    public override void OnInteractableGrabPressed(VRController controller,float grabAxis)
    {
        base.OnInteractableGrabPressed(controller,grabAxis);

        if (this.grabType == GrabType.GrabButton)
        {
            this.RunOpenDoorLogic(controller, grabAxis);
        }

    }


    //override
    protected override void AfterChangePrimaryControllerFunction()
    {
        base.AfterChangePrimaryControllerFunction();

        this.previousHandYAngel = 0;

    }

    protected override void ExitFunction()
    {
        base.ExitFunction();

        this.previousHandYAngel = 0;

    }

    protected override void ReleaseFunction()
    {
        base.ReleaseFunction();

        this.previousHandYAngel = 0;

        this.rig.angularVelocity = new Vector3(0, this.doorAngularVelocity * Mathf.Deg2Rad, 0) * 4;
    }

    //helper
    void RunOpenDoorLogic(VRController controller, float grabAxis)
    {
        //release door
        if ((this.primaryController != null && this.primaryController == controller) && grabAxis <= this.sensitiveReleaseThreshold && this.isTriggered && this.isUsing)
        {
            this.ReleaseObject(controller, this.ReleaseFunction, this.AfterChangePrimaryControllerFunction);
            this.ExitCollider(controller, this.ExitFunction, this.AfterChangePrimaryControllerFunction);

        }

        //distance to release
        if (this.isDistanceToInteract && (this.primaryController != null && this.primaryController == controller) && grabAxis <= this.sensitiveReleaseThreshold && !this.isTriggered && this.isUsing)
        {
            this.ReleaseObject(controller, this.ReleaseFunction, this.AfterChangePrimaryControllerFunction);
            this.ExitCollider(controller, this.ExitFunction, this.AfterChangePrimaryControllerFunction);

        }

        //open door
        if ((this.primaryController != null && this.primaryController == controller) && this.isTriggered && grabAxis > this.sensitiveReleaseThreshold)
        {
            this.OpenDoor();
        }

        //distance open door
        if (this.isDistanceToInteract && (this.primaryController != null && this.primaryController == controller) && this.isRayCastered && grabAxis > this.sensitiveReleaseThreshold)
        {
            this.OpenDoor();
        }
    }

    void OpenDoor()
    {
        this.isUsing = true;

        this.rig.isKinematic = true;

        float handYAngel = Utility.CaculateAngel(Vector3.ProjectOnPlane(this.primaryController.controllerObject.transform.position - this.hingeJointPosition, transform.TransformDirection(this.hinge.axis)), Vector3.ProjectOnPlane(this.originalDoorPosition - this.hingeJointPosition, transform.TransformDirection(this.hinge.axis)));
        float objectAngel = Utility.CaculateAngel(Vector3.ProjectOnPlane(this.transform.position - this.hingeJointPosition, transform.TransformDirection(this.hinge.axis)), Vector3.ProjectOnPlane(this.originalDoorPosition - this.hingeJointPosition, transform.TransformDirection(this.hinge.axis)));



        if (this.previousHandYAngel == 0)
        {
            this.previousHandYAngel = handYAngel;

        }

        float deltAngel = handYAngel - this.previousHandYAngel;
        this.previousHandYAngel = handYAngel;

        if (objectAngel + deltAngel >= this.maxAngel)
        {
            deltAngel = this.maxAngel - objectAngel;
        }

        if (objectAngel + deltAngel <= this.minAngel)
        {
            deltAngel = this.minAngel - objectAngel;
        }

        this.doorAngularVelocity = deltAngel / Time.deltaTime;

        transform.RotateAround(hingeJointPosition, Vector3.up, deltAngel);
    }

}
