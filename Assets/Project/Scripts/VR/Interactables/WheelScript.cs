﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HingeJoint))]
[RequireComponent(typeof(Rigidbody))]
public class WheelScript : InteractableTwoHands
{

    private float previousHandYAngel = 0;
    private float wheelAngularVelocity = 0;
    private Vector3 hingeJointPosition;
    private Quaternion originalWheelRotation;
    private HingeJoint hinge;
    private float maxAngel;
    private float minAngel;


    // Use this for initialization
    protected override void Start()
    {

        base.Start();

        this.hinge = gameObject.GetComponent<HingeJoint>();
        this.rig = gameObject.GetComponent<Rigidbody>();
        this.hingeJointPosition = transform.TransformPoint(this.hinge.anchor);
        this.originalWheelRotation = transform.rotation;

        //set min max
        float objectAngel;
        Vector3 rotationAxis;
        var deltaRot = transform.rotation * Quaternion.Inverse(this.originalWheelRotation);
        deltaRot.ToAngleAxis(out objectAngel, out rotationAxis);
        this.maxAngel = this.hinge.limits.max;
        this.minAngel = this.hinge.limits.min;
    }

    // Update is called once per frame
    void Update()
    {


    }

    public override void OnInteractableGrabDown(VRController controller)
    {
        base.OnInteractableGrabDown(controller);

        this.ExchangePrimaryHand(controller);
    }

    public override void OnInteractableGrabPressed(VRController controller, float grabAxis)
    {
        base.OnInteractableGrabPressed(controller, grabAxis);

        //only care primary controller event
        if (controller != primaryController)
        {
            return;
        }

        this.ReleaseWheel(grabAxis);

        this.GrabWheel(grabAxis);
        
    }

    public override void OnInteractableExit(VRController controller)
    {
        base.OnInteractableExit(controller);

        //secondary hand become primary hand
        if (controller == this.primaryController && this.secondaryController != null)
        {
            this.primaryController = this.secondaryController;
            this.secondaryController = null;
            return;
        }

        //no secondary hand
        if (controller == this.primaryController && this.secondaryController == null)
        {
            this.isTriggered = false;

            this.rig.isKinematic = false;
            this.previousHandYAngel = 0;
            this.primaryController = null;
            return;
        }


        //secondary hand leave
        if (controller == this.secondaryController)
        {
            this.secondaryController = null;
            return;
        }

    }


    //helper
    private void ExchangePrimaryHand(VRController controller)
    {
        //assign primary hand (which hand should be primary hand)
        if (this.primaryController != null  && this.primaryController == controller && this.isTriggered)
        {

            //this.PrimaryHand = this.PrimaryHand;

        }
        else if ((this.secondaryController != null  && this.secondaryController == controller && this.isTriggered && this.primaryController != controller)
            || (this.secondaryController != null))
        {
            var tempHand = this.secondaryController;
            this.secondaryController = this.primaryController;
            this.primaryController = tempHand;

            this.previousHandYAngel = 0;

        }
    }

    private void GrabWheel(float grabAxis)
    {
        //grab wheel
        if (this.primaryController != null && this.isTriggered && grabAxis > this.sensitiveReleaseThreshold)
        {

            Debug.Log("trigger enter and button");
            this.rig.isKinematic = true;

            float handYAngel = this.CaculateAngelWithPosition(Vector3.ProjectOnPlane(this.primaryController.controllerObject.transform.position - this.hingeJointPosition, transform.TransformDirection(this.hinge.axis)), Vector3.ProjectOnPlane(Vector3.one, transform.TransformDirection(this.hinge.axis)));

            float objectAngel = this.CaculateAngelWithRotation(this.transform.rotation, this.originalWheelRotation, this.hinge);



            if (this.previousHandYAngel == 0)
            {
                this.previousHandYAngel = handYAngel;

            }

            float deltAngel = handYAngel - this.previousHandYAngel;

            //TODO: this is to fix wheel jumping when turn wheel more than the max angle too much, but this fix is elegent ,need to be improved
            if (deltAngel > 10 || deltAngel < -10)
            {
                deltAngel = 0;
                this.previousHandYAngel = 0;
            }
            else
            {

                this.previousHandYAngel = handYAngel;

            }
            //this.previousHandYAngel = handYAngel;

            if (objectAngel + deltAngel >= this.maxAngel)
            {
                deltAngel = this.maxAngel - objectAngel;
            }

            if (objectAngel + deltAngel <= this.minAngel)
            {
                deltAngel = this.minAngel - objectAngel;
            }

            this.wheelAngularVelocity = deltAngel / Time.deltaTime;

            transform.RotateAround(hingeJointPosition, transform.TransformDirection(this.hinge.axis), deltAngel);

        }
    }

    private void ReleaseWheel(float grabAxis)
    {
        //release wheel
        if (this.primaryController != null && grabAxis <= this.sensitiveReleaseThreshold && this.isTriggered && this.rig.isKinematic)
        {
            //secondary hand become primary hand
            if (this.secondaryController != null)
            {
                var tempHand = this.secondaryController;
                this.secondaryController = this.primaryController;
                this.primaryController = tempHand;

                this.previousHandYAngel = 0;
            }
            else
            {
                this.rig.isKinematic = false;
                this.previousHandYAngel = 0;

                this.rig.angularVelocity = new Vector3(0, this.wheelAngularVelocity * Mathf.Deg2Rad, 0);

            }

        }
    }

    private float CaculateAngelWithPosition(Vector3 from, Vector3 to)
    {
        float angel = Vector3.Angle(from, to);
        Vector3 cross = Vector3.Cross(from, to);
        Debug.Log("hand");
        Debug.Log(angel);
        if (cross.y > 0) angel = -angel;

        return angel;
    }

    private float CaculateAngelWithRotation(Quaternion from, Quaternion to,HingeJoint hingeJoint)
    {
        float angel;
        Vector3 rotationAxis;
        var deltaRot = from * Quaternion.Inverse(to);
        deltaRot.ToAngleAxis(out angel, out rotationAxis);
        Debug.Log("object");
        Debug.Log(angel);

        if (rotationAxis != transform.TransformDirection(hingeJoint.axis))
        {
            angel = -angel;
        }

        return angel;
    }

}
