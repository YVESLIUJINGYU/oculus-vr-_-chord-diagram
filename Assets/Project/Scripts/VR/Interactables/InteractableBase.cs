﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GrabType
{
    GrabButton,
    IndexButton,
    None
}

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class InteractableBase : MonoBehaviour {

    [Header("Basic Setup")]
    public GrabType grabType = GrabType.None;
    [Range(0.1f, 0.9f)]
    public float sensitiveReleaseThreshold = 0.3f;//thumbstick threshold
    public AudioClip clip;
    public bool isDistanceToInteract = true;//flag to set if object is allowed distance interaction

    [HideInInspector]
    public bool isRayCastered = false;//flag for checking if object has already be ray castered
    [HideInInspector]
    public bool isTriggered = false;//flag for checking if object trigger collider
    [HideInInspector]
    public bool isUsing = false;//flag for checking if object is interacted right now

    protected VRController primaryController;
    protected VRController secondaryController;

    public VRController PrimaryController { get { return this.primaryController; } }
    public VRController SecondaryController { get { return this.secondaryController; } }


    //haptic vibration
    protected IHaptic haptic;

    //rig
    [HideInInspector]
    public Rigidbody rig;
    [HideInInspector]
    public Collider col;

    // Use this for initialization
    protected virtual void Start () {

        this.haptic = GameManager.Shared.haptic;

#if VR_OCULUS
        //this.haptic = OculusManager.Shared;

#elif VR_HTCVIVE
        //this.haptic = HTCViveManager.Shared;

#endif

        //htcvive need no sensitive release threshold
        switch (VRHelper.GetHeadsetBrand())
        {
            case VRHelper.VRBrands.OculusRift:

                if (this.grabType == GrabType.None)
                {
                    this.grabType = GrabType.GrabButton;
                }

                break;

            case VRHelper.VRBrands.HtcVive:

                if (this.grabType == GrabType.None)
                {
                    this.grabType = GrabType.IndexButton;
                }

                if (this.grabType == GrabType.GrabButton)
                {
                    this.sensitiveReleaseThreshold = 0f;
                }

                break;

            default:
                break;
        }


    }

    private void Awake()
    {
        this.rig = gameObject.GetComponent<Rigidbody>();
        this.col = gameObject.GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update () {
		
	}

    protected void VibrateController(VRController controller, float strength = 0,int duration =0)
    {
        if (strength != 0)
        {
            this.haptic.HapticPaulse(controller, strength,duration);
        }
        else if (this.clip != null)
        {
            this.haptic.HapticPaulse(controller, clip);
        }

        
    }

    //collid
    public virtual void OnInteractableEnter(VRController controller)
    {


    }


    public virtual void OnInteractableExit(VRController controller)
    {


    }

    public virtual void OnInteractableStay(VRController controller)
    {


    }

    //input
    public virtual void OnInteractableGrabDown(VRController controller)
    {


    }

    public virtual void OnInteractableGrabUp(VRController controller)
    {


    }

    public virtual void OnInteractableGrabPressed(VRController controller,float grabAxis)
    {


    }

    public virtual void OnInteractableIndexDown(VRController controller)
    {


    }

    public virtual void OnInteractableIndexUp(VRController controller)
    {


    }

    public virtual void OnInteractableIndexPressed(VRController controller, float indexAxis)
    {


    }

    //button
    public virtual void OnInteractableButtonOnePressed(VRController controller, bool isPressed)
    {


    }

    public virtual void OnInteractableButtonTwoPressed(VRController controller, bool isPressed)
    {


    }

    public virtual void OnInteractableButtonThreePressed(VRController controller, bool isPressed)
    {


    }

    public virtual void OnInteractableButtonFourPressed(VRController controller, bool isPressed)
    {


    }

    //raycaster
    public virtual void OnInteractableRayCasterEnter(VRController controller)
    {


    }

    public virtual void OnInteractableRayCasterExit(VRController controller)
    {


    }

    public virtual void OnInteractableRayCasterStay(VRController controller)
    {


    }
}
