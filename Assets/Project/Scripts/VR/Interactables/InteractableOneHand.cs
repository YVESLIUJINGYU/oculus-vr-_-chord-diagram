﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class InteractableOneHand : InteractableBase
{

    // Use this for initialization
    protected override void Start()
    {

        base.Start();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    

    //collid
    public override void OnInteractableEnter(VRController controller)
    {
        base.OnInteractableEnter(controller);

        this.isTriggered = true;

        this.SetControllers(controller);

    }

    public override void OnInteractableStay(VRController controller)
    {
        base.OnInteractableStay(controller);

        this.isTriggered = true;

        this.SetControllers(controller);

    }

    public override void OnInteractableExit(VRController controller)
    {
        base.OnInteractableExit(controller);

        if (this.isTriggered)
        {
            this.ExitCollider(controller, this.ExitFunction, this.AfterChangePrimaryControllerFunction);
        }


    }



    //ray caster
    public override void OnInteractableRayCasterEnter(VRController controller)
    {
        base.OnInteractableRayCasterEnter(controller);

        this.isRayCastered = true;

        this.SetControllers(controller);


    }

    public override void OnInteractableRayCasterStay(VRController controller)
    {
        base.OnInteractableRayCasterStay(controller);

        this.isRayCastered = true;

        this.SetControllers(controller);

    }

    public override void OnInteractableRayCasterExit(VRController controller)
    {
        base.OnInteractableRayCasterExit(controller);

        if (this.isRayCastered)
        {
            this.ExitCollider(controller, this.ExitFunction, this.AfterChangePrimaryControllerFunction);
        }



    }

    //input
    public override void OnInteractableGrabDown(VRController controller)
    {
        base.OnInteractableGrabDown(controller);

        this.ChangePrimaryController(controller, AfterChangePrimaryControllerFunction);
    }

    //helper
    protected virtual void AfterChangePrimaryControllerFunction()
    {
        this.isUsing = false;
    }

    protected virtual void ExitFunction()
    {
        this.isUsing = false;

        this.isTriggered = false;
        this.isRayCastered = false;

        this.rig.isKinematic = false;

        this.SetPrimaryController(null);
        this.SetSecondaryController(null);
    }

    protected virtual void ReleaseFunction()
    {
        this.isUsing = false;
        this.rig.isKinematic = false;

    }

    protected void ReleaseObject(VRController controller, Action releaseFunction, Action exchangePrimaryHandFunction)
    {
        if (controller != this.primaryController)
        {
            return;
        }

        //secondary hand become primary hand
        if (this.secondaryController != null)
        {
            this.ExchangeTwoControllers();

            exchangePrimaryHandFunction();

            releaseFunction();
        }
        else
        {
            releaseFunction();

        }

    }

    protected void ExitCollider(VRController controller,Action exitFunction, Action exchangePrimaryHandFunction)
    {
        //secondary hand become primary hand
        if (controller == this.primaryController && this.secondaryController != null)
        {
            this.SetPrimaryController(this.secondaryController);
            this.SetSecondaryController(null);

            exchangePrimaryHandFunction();

            return;
        }

        //no secondary hand
        if (controller == this.primaryController && this.secondaryController == null)
        {
            exitFunction();
            return;
        }


        //secondary hand leave
        if (controller == this.secondaryController)
        {
            this.SetSecondaryController(null);
            return;
        }

    }

    //controllers
    protected void ChangePrimaryController(VRController controller,Action exchangePrimaryHandFunction)
    {
        /*
        //assign primary hand (which hand should be primary hand)
        if (this.primaryController != null && this.primaryController == controller && this.isTriggered)
        {

            //this.PrimaryHand = this.PrimaryHand;

        }
        else if ((this.secondaryController != null && this.secondaryController == controller && this.isTriggered && this.primaryController != controller)
            || (this.secondaryController != null))
        {
            this.ExchangeTwoControllers();

            exchangePrimaryHandFunction();

        }
        */

        if (controller == null)
        {
            return;
        }

        //reassign primary hand
        if (this.primaryController != null && this.primaryController == controller)
        {

            //this.PrimaryHand = this.PrimaryHand;

        }
        else if (this.primaryController != null && this.secondaryController == controller)
        {

            this.ExchangeTwoControllers();

            exchangePrimaryHandFunction();

        }
        else if (this.primaryController == null && this.secondaryController == controller)
        {
            this.ExchangeTwoControllers();

            exchangePrimaryHandFunction();

        }
        else if (this.primaryController == null && this.secondaryController != controller)
        {
            this.SetPrimaryController(controller);

            exchangePrimaryHandFunction();

        }

    }

    protected void ExchangeTwoControllers()
    {
        var tempHand = this.secondaryController;
        this.SetSecondaryController(this.primaryController);
        this.SetPrimaryController(tempHand);
    }

    protected void SetControllers(VRController controller)
    {
        if (this.primaryController == null && this.secondaryController == null)
        {
            this.SetPrimaryController(controller);
        }
        else if (this.primaryController == null && this.secondaryController != null && this.secondaryController != controller)
        {
            this.SetPrimaryController(this.secondaryController);
            this.SetSecondaryController(controller);

        }
        else if (this.primaryController == null && this.secondaryController != null && this.secondaryController == controller)
        {
            this.SetPrimaryController(this.secondaryController);
            this.SetSecondaryController(null);

        }
        else if (this.primaryController != null && this.secondaryController == null && this.primaryController != controller)
        {
            this.SetSecondaryController(controller);

        }
    }

    protected void SetPrimaryController(VRController controller)
    {
        this.primaryController = controller;

        if (controller != null)
        {
            this.VibrateController(controller);
        }

        
    }

    protected void SetSecondaryController(VRController controller)
    {
        this.secondaryController = controller;

        /*
        if (controller != null)
        {
            this.VibrateController(controller);
        }
        */

    }

}
