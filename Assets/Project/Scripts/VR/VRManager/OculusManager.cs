﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if VR_OCULUS

public class OculusManager : VRManagerBase, IInput,IHaptic {

    //singleton
    private static volatile OculusManager sharedInstance;
    private static object syncRoot = new System.Object();

    private OculusManager() { }

    public static OculusManager Shared
    {
        get
        {
            if (sharedInstance == null)
            {
                lock (syncRoot)
                {
                    if (sharedInstance == null)
                        sharedInstance = new OculusManager();
                }
            }

            return sharedInstance;
        }
    }

    /// <summary>
    /// To set controllers for manager, so can reference these controllers later
    /// </summary>
    public override void SetControllersAndHeadsetAndPlayer()
    {
        if (this.leftController == null)
        {
            this.leftController = GameObject.Find("LeftHandAnchor").GetComponent<VRController>();
        }
        if (this.rightController == null)
        {
            this.rightController = GameObject.Find("RightHandAnchor").GetComponent<VRController>();
        }
        if (this.headset == null)
        {
            this.headset = GameObject.Find("CenterEyeAnchor").GetComponent<VRHeadset>();
        }
        if (this.player == null)
        {
            this.player = GameObject.Find("OculusPlayer").GetComponent<VRPlayer>();
        }

    }

    /**IHaptic**/
    void IHaptic.HapticPaulse(VRController controller, AudioClip clip)
    {
        OVRHapticsClip haptic = new OVRHapticsClip(clip);

        if (controller.controllerType == VRControllerTypes.Right)
        {
            OVRHaptics.RightChannel.Preempt(haptic);

        }

        if (controller.controllerType == VRControllerTypes.Left)
        {
            OVRHaptics.LeftChannel.Preempt(haptic);

        }
    }

    //strength  40, 100 180
    void IHaptic.HapticPaulse(VRController controller, float strength,int duration)
    {
        
        //OVRHaptics.LeftChannel.Queue
        if (controller.controllerType == VRControllerTypes.Right)
        {
            if (duration == 0)
            {
                OVRHapticsClip haptic = new OVRHapticsClip();
                haptic.Reset();
                haptic.WriteSample((byte)(strength));
                OVRHaptics.RightChannel.Preempt(haptic);
            }
            else
            {
                byte[] samples = new byte[duration];
                for (int i = 0; i < duration; i++)
                {
                    samples[i] = i % 2 == 0 ? (byte)0 : (byte)(strength);
                }
                OVRHapticsClip haptic = new OVRHapticsClip(samples,duration);
                OVRHaptics.RightChannel.Preempt(haptic);
            }

        }

        if (controller.controllerType == VRControllerTypes.Left)
        {
            if (duration == 0)
            {
                OVRHapticsClip haptic = new OVRHapticsClip();
                haptic.Reset();
                haptic.WriteSample((byte)(strength));
                OVRHaptics.LeftChannel.Preempt(haptic);

            }
            else
            {
                byte[] samples = new byte[duration];
                for (int i = 0; i < duration; i++)
                {
                    samples[i] = i % 2 == 0 ? (byte)0 : (byte)(strength);
                }
                OVRHapticsClip haptic = new OVRHapticsClip(samples, duration);
                OVRHaptics.LeftChannel.Preempt(haptic);
            }
                

        }
    }

    /**IInput**/
    bool IInput.GetMenu()
    {
        return OVRInput.GetDown(OVRInput.Button.Start);
    }

    bool IInput.LGrabDown()
    {
        return OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger,OVRInput.Controller.LTouch);
    }

    float IInput.LGrabPressed()
    {
        return OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.LTouch);
    }

    bool IInput.LGrabUp()
    {
        return OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.LTouch);
    }

    bool IInput.LIndexDown()
    {
        return OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch);
    }

    float IInput.LIndexPressed()
    {
        return OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch);
    }

    bool IInput.LIndexUp()
    {
        return OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch);
    }

    Vector2 IInput.LThumbstickMoved()
    {
        return OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch);
    }

    bool IInput.LThumbstickTouched()
    {
        return OVRInput.Get(OVRInput.Touch.PrimaryThumbstick, OVRInput.Controller.LTouch);
    }

    bool IInput.RGrabDown()
    {
        return OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.RTouch);
    }

    float IInput.RGrabPressed()
    {
        return OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch);
    }

    bool IInput.RGrabUp()
    {
        return OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.RTouch);
    }

    bool IInput.RIndexDown()
    {
        return OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch);
    }

    float IInput.RIndexPressed()
    {
        return OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch);
    }

    bool IInput.RIndexUp()
    {
        return OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch);
    }

    Vector2 IInput.RThumbstickMoved()
    {
        return OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch);
    }

    bool IInput.RThumbstickTouched()
    {
        return OVRInput.Get(OVRInput.Touch.PrimaryThumbstick, OVRInput.Controller.RTouch);
    }

    bool IInput.ButtonOneDown()
    {
        return OVRInput.GetDown(OVRInput.Button.One);
    }

    bool IInput.ButtonTwoDown()
    {
        return OVRInput.GetDown(OVRInput.Button.Two);
    }

    bool IInput.ButtonThreeDown()
    {
        return OVRInput.GetDown(OVRInput.Button.Three);
    }

    bool IInput.ButtonFourDown()
    {
        return OVRInput.GetDown(OVRInput.Button.Four);
    }

    bool IInput.ButtonOnePressed()
    {
        return OVRInput.Get(OVRInput.Button.One);
    }

    bool IInput.ButtonTwoPressed()
    {
        return OVRInput.Get(OVRInput.Button.Two);
    }

    bool IInput.ButtonThreePressed()
    {
        return OVRInput.Get(OVRInput.Button.Three);
    }

    bool IInput.ButtonFourPressed()
    {
        return OVRInput.Get(OVRInput.Button.Four);
    }

    bool IInput.ButtonOneUp()
    {
        return OVRInput.GetUp(OVRInput.Button.One);
    }

    bool IInput.ButtonTwoUp()
    {
        return OVRInput.GetUp(OVRInput.Button.Two);
    }

    bool IInput.ButtonThreeUp()
    {
        return OVRInput.GetUp(OVRInput.Button.Three);
    }

    bool IInput.ButtonFourUp()
    {
        return OVRInput.GetUp(OVRInput.Button.Four);
    }

    //only oculus
    bool IInput.LIndexTouched()
    {
        return OVRInput.Get(OVRInput.NearTouch.PrimaryIndexTrigger, OVRInput.Controller.LTouch);
    }

    bool IInput.RIndexTouched()
    {
        return OVRInput.Get(OVRInput.NearTouch.PrimaryIndexTrigger, OVRInput.Controller.RTouch);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    

}

#endif