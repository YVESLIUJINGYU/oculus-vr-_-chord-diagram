﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if VR_HTCVIVE

public class HTCViveManager : VRManagerBase, IInput,IHaptic
{

    private SteamVR_ControllerManager steamVR;

    int maxHapticVibration = 3999;

    //singleton
    private static volatile HTCViveManager sharedInstance;
    private static object syncRoot = new System.Object();

    private HTCViveManager(){ }

    public static HTCViveManager Shared
    {
        get
        {
            if (sharedInstance == null)
            {
                lock (syncRoot)
                {
                    if (sharedInstance == null)
                        sharedInstance = new HTCViveManager();
                }
            }

            return sharedInstance;
        }
    }

    /// <summary>
    /// To set controllers for manager, so can reference these controllers later
    /// </summary>
    public override void SetControllersAndHeadsetAndPlayer()
    {
        if (this.headset == null)
        {
            this.headset = GameObject.Find("Camera (eye)").GetComponent<VRHeadset>();;
        }

        if (this.leftController == null || this.rightController == null)
        {
            if (this.steamVR == null)
            {
                this.steamVR = GameObject.Find("[CameraRig]").GetComponent<SteamVR_ControllerManager>();

                if (this.steamVR == null)
                {
                    throw new System.Exception("please drag a steamvr prefab to scene");
                }

            }

            if (this.leftController == null)
            {
                this.leftController = this.steamVR.left.GetComponent<VRController>();
            }
            if (this.rightController == null)
            {
                this.rightController = this.steamVR.right.GetComponent<VRController>();
            }

        }

        if (this.player == null)
        {
            this.player = GameObject.Find("VivePlayer").GetComponent<VRPlayer>();
        }

    }

    /**IHaptic**/
    void IHaptic.HapticPaulse(VRController controller, AudioClip clip)
    {

        ((IHaptic)this).HapticPaulse(controller, 0.9f, 1);
    }

    void IHaptic.HapticPaulse(VRController controller, float strength,int duration)
    {
        float convertedStrength = maxHapticVibration * strength;

        if (controller.controllerType == VRControllerTypes.Right)
        {
            SteamVR_Controller.Input((int)this.rightController.GetComponent<SteamVR_TrackedObject>().index).TriggerHapticPulse((ushort)convertedStrength);

        }

        if (controller.controllerType == VRControllerTypes.Left)
        {
            SteamVR_Controller.Input((int)this.leftController.GetComponent<SteamVR_TrackedObject>().index).TriggerHapticPulse((ushort)convertedStrength);
        }

    }

    /**IInput**/
    bool IInput.GetMenu()
    {
        return SteamVR_Controller.Input((int)this.leftController.GetComponent<SteamVR_TrackedObject>().index).GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu);
    }

    bool IInput.LGrabDown()
    {
        return SteamVR_Controller.Input((int)this.leftController.GetComponent<SteamVR_TrackedObject>().index).GetPressDown(SteamVR_Controller.ButtonMask.Grip);
    }

    float IInput.LGrabPressed()
    {

        return SteamVR_Controller.Input((int)this.leftController.GetComponent<SteamVR_TrackedObject>().index).GetPress(SteamVR_Controller.ButtonMask.Grip) ? 1.0f:0.0f;
    }

    bool IInput.LGrabUp()
    {
        return SteamVR_Controller.Input((int)this.leftController.GetComponent<SteamVR_TrackedObject>().index).GetPressUp(SteamVR_Controller.ButtonMask.Grip);
    }

    bool IInput.LIndexDown()
    {
        return SteamVR_Controller.Input((int)this.leftController.GetComponent<SteamVR_TrackedObject>().index).GetPressDown(SteamVR_Controller.ButtonMask.Trigger);
    }

    float IInput.LIndexPressed()
    {
        return SteamVR_Controller.Input((int)this.leftController.GetComponent<SteamVR_TrackedObject>().index).GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x;
    }

    bool IInput.LIndexUp()
    {
        return SteamVR_Controller.Input((int)this.leftController.GetComponent<SteamVR_TrackedObject>().index).GetPressUp(SteamVR_Controller.ButtonMask.Trigger);
    }

    Vector2 IInput.LThumbstickMoved()
    {
        return SteamVR_Controller.Input((int)this.leftController.GetComponent<SteamVR_TrackedObject>().index).GetPress(SteamVR_Controller.ButtonMask.Touchpad) ? SteamVR_Controller.Input((int)this.leftController.GetComponent<SteamVR_TrackedObject>().index).GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad):Vector2.zero;
    }

    bool IInput.LThumbstickTouched()
    {
        return SteamVR_Controller.Input((int)this.leftController.GetComponent<SteamVR_TrackedObject>().index).GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad).magnitude > 0;
    }

    bool IInput.RGrabDown()
    {
        return SteamVR_Controller.Input((int)this.rightController.GetComponent<SteamVR_TrackedObject>().index).GetPressDown(SteamVR_Controller.ButtonMask.Grip);
    }

    float IInput.RGrabPressed()
    {
        return SteamVR_Controller.Input((int)this.rightController.GetComponent<SteamVR_TrackedObject>().index).GetPress(SteamVR_Controller.ButtonMask.Grip) ? 1.0f : 0.0f;
    }

    bool IInput.RGrabUp()
    {
        return SteamVR_Controller.Input((int)this.rightController.GetComponent<SteamVR_TrackedObject>().index).GetPressUp(SteamVR_Controller.ButtonMask.Grip);
    }

    bool IInput.RIndexDown()
    {
        return SteamVR_Controller.Input((int)this.rightController.GetComponent<SteamVR_TrackedObject>().index).GetPressDown(SteamVR_Controller.ButtonMask.Trigger);
    }

    float IInput.RIndexPressed()
    {
        return SteamVR_Controller.Input((int)this.rightController.GetComponent<SteamVR_TrackedObject>().index).GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x;
    }

    bool IInput.RIndexUp()
    {
        return SteamVR_Controller.Input((int)this.rightController.GetComponent<SteamVR_TrackedObject>().index).GetPressUp(SteamVR_Controller.ButtonMask.Trigger);
    }

    Vector2 IInput.RThumbstickMoved()
    {
        return SteamVR_Controller.Input((int)this.rightController.GetComponent<SteamVR_TrackedObject>().index).GetPress(SteamVR_Controller.ButtonMask.Touchpad) ? SteamVR_Controller.Input((int)this.rightController.GetComponent<SteamVR_TrackedObject>().index).GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad):Vector2.zero;
    }

    bool IInput.RThumbstickTouched()
    {
        return SteamVR_Controller.Input((int)this.rightController.GetComponent<SteamVR_TrackedObject>().index).GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad).magnitude > 0;
    }

    //only oculus
    bool IInput.ButtonOneDown()
    {
        return false;
    }

    bool IInput.ButtonTwoDown()
    {
        return false;
    }

    bool IInput.ButtonThreeDown()
    {
        return false;
    }

    bool IInput.ButtonFourDown()
    {
        return false;
    }

    bool IInput.ButtonOnePressed()
    {
        return false;
    }

    bool IInput.ButtonTwoPressed()
    {
        return false;
    }

    bool IInput.ButtonThreePressed()
    {
        return false;
    }

    bool IInput.ButtonFourPressed()
    {
        return false;
    }

    bool IInput.ButtonOneUp()
    {
        return false;
    }

    bool IInput.ButtonTwoUp()
    {
        return false;
    }

    bool IInput.ButtonThreeUp()
    {
        return false;
    }

    bool IInput.ButtonFourUp()
    {
        return false;
    }

    bool IInput.LIndexTouched()
    {
        return false;
    }

    bool IInput.RIndexTouched()
    {
        return false;
    }

}

#endif