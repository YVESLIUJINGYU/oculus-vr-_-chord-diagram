﻿#if VR_MR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA.Input;

public class WindowsMixedRealityManager : VRManagerBase, IInput, IHaptic
{
    //singleton
    private static volatile WindowsMixedRealityManager sharedInstance;
    private static object syncRoot = new System.Object();

    private WindowsMixedRealityManager() { }

    public static WindowsMixedRealityManager Shared
    {
        get
        {
            if (sharedInstance == null)
            {
                lock (syncRoot)
                {
                    if (sharedInstance == null)
                        sharedInstance = new WindowsMixedRealityManager();
                }
            }

            return sharedInstance;
        }
    }

    /// <summary>
    /// To set controllers for manager, so can reference these controllers later
    /// </summary>
    public override void SetControllersAndHeadsetAndPlayer()
    {

        if (this.leftController == null && GameObject.Find("ControllerCubeLeft(Clone)"))
        {
            this.leftController = GameObject.Find("ControllerCubeLeft(Clone)").GetComponent<VRController>();
        }
        if (this.rightController == null && GameObject.Find("ControllerCubeRight(Clone)"))
        {
            this.rightController = GameObject.Find("ControllerCubeRight(Clone)").GetComponent<VRController>();
        }
        if (this.headset == null)
        {
            this.headset = GameObject.Find("CenterEyeAnchor");
        }
        if (this.player == null)
        {
            this.player = GameObject.Find("MRPlayer").GetComponent<VRPlayer>();
        }

    }

    /**IHaptic**/
    void IHaptic.HapticPaulse(VRController controller, AudioClip clip)
    {
        OVRHapticsClip haptic = new OVRHapticsClip(clip);

        if (controller.controllerType == VRControllerTypes.Right)
        {
            OVRHaptics.RightChannel.Preempt(haptic);

        }

        if (controller.controllerType == VRControllerTypes.Left)
        {
            OVRHaptics.LeftChannel.Preempt(haptic);

        }
    }

    //strength  40, 100 180
    void IHaptic.HapticPaulse(VRController controller, float strength, int duration)
    {

        //OVRHaptics.LeftChannel.Queue
        if (controller.controllerType == VRControllerTypes.Right)
        {
            if (duration == 0)
            {
                OVRHapticsClip haptic = new OVRHapticsClip();
                haptic.Reset();
                haptic.WriteSample((byte)(strength));
                OVRHaptics.RightChannel.Preempt(haptic);
            }
            else
            {
                byte[] samples = new byte[duration];
                for (int i = 0; i < duration; i++)
                {
                    samples[i] = i % 2 == 0 ? (byte)0 : (byte)(strength);
                }
                OVRHapticsClip haptic = new OVRHapticsClip(samples, duration);
                OVRHaptics.RightChannel.Preempt(haptic);
            }

        }

        if (controller.controllerType == VRControllerTypes.Left)
        {
            if (duration == 0)
            {
                OVRHapticsClip haptic = new OVRHapticsClip();
                haptic.Reset();
                haptic.WriteSample((byte)(strength));
                OVRHaptics.LeftChannel.Preempt(haptic);

            }
            else
            {
                byte[] samples = new byte[duration];
                for (int i = 0; i < duration; i++)
                {
                    samples[i] = i % 2 == 0 ? (byte)0 : (byte)(strength);
                }
                OVRHapticsClip haptic = new OVRHapticsClip(samples, duration);
                OVRHaptics.LeftChannel.Preempt(haptic);
            }


        }
    }

    /**IInput**/
    bool IInput.GetMenu()
    {

        return Input.GetKeyDown("joystick button 8");
    }

    bool IInput.LGrabDown()
    {
        return Input.GetKeyDown("joystick button 4");
    }

    float IInput.LGrabPressed()
    {
        return Input.GetAxis("joystick button 11");
    }

    bool IInput.LGrabUp()
    {
        return Input.GetKeyUp("joystick button 4");
    }

    bool IInput.LIndexDown()
    {
        return Input.GetKeyDown("joystick button 14");
    }

    float IInput.LIndexPressed()
    {
        return Input.GetAxis("joystick button 9");
    }

    bool IInput.LIndexUp()
    {
        return Input.GetKeyUp("joystick button 14");
    }

    Vector2 IInput.LThumbstickMoved()
    {
        return new Vector2(Input.GetAxis("joystick button 1"), Input.GetAxis("joystick button 2"));
    }

    bool IInput.LThumbstickTouched()
    {
        return Input.GetAxis("joystick button 1") != 0 || Input.GetAxis("joystick button 2") != 0;
    }

    bool IInput.RGrabDown()
    {
        return Input.GetKeyDown("joystick button 5");
    }

    float IInput.RGrabPressed()
    {
        return Input.GetAxis("joystick button 12");
    }

    bool IInput.RGrabUp()
    {
        return Input.GetKeyUp("joystick button 5");
    }

    bool IInput.RIndexDown()
    {
        return Input.GetKeyDown("joystick button 15");
    }

    float IInput.RIndexPressed()
    {
        return Input.GetAxis("joystick button 10");
    }

    bool IInput.RIndexUp()
    {
        return Input.GetKeyUp("joystick button 15");
    }

    Vector2 IInput.RThumbstickMoved()
    {
        return new Vector2(Input.GetAxis("joystick button 4"), Input.GetAxis("joystick button 5"));
    }

    bool IInput.RThumbstickTouched()
    {
        return Input.GetAxis("joystick button 4") != 0 || Input.GetAxis("joystick button 5") != 0;
    }

    bool IInput.ButtonOneDown()
    {
        return false;
    }

    bool IInput.ButtonTwoDown()
    {
        return false;
    }

    bool IInput.ButtonThreeDown()
    {
        return false;
    }

    bool IInput.ButtonFourDown()
    {
        return false;
    }

    bool IInput.ButtonOnePressed()
    {
        return false;
    }

    bool IInput.ButtonTwoPressed()
    {
        return false;
    }

    bool IInput.ButtonThreePressed()
    {
        return false;
    }

    bool IInput.ButtonFourPressed()
    {
        return false;
    }

    bool IInput.ButtonOneUp()
    {
        return false;
    }

    bool IInput.ButtonTwoUp()
    {
        return false;
    }

    bool IInput.ButtonThreeUp()
    {
        return false;
    }

    bool IInput.ButtonFourUp()
    {
        return false;
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }



}
#endif