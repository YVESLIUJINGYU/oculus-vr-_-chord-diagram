﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class VRManagerBase {

    public VRController leftController;
    public VRController rightController;
    public VRPlayer player;
    public VRHeadset headset;

    public abstract void SetControllersAndHeadsetAndPlayer();

}
