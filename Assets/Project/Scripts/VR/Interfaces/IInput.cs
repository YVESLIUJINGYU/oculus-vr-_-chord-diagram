﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInput {

    bool LGrabUp();
    bool LGrabDown();
    float LGrabPressed();

    bool RGrabUp();
    bool RGrabDown();
    float RGrabPressed();

    bool LIndexUp();
    bool LIndexDown();
    float LIndexPressed();

    bool RIndexUp();
    bool RIndexDown();
    float RIndexPressed();

    Vector2 LThumbstickMoved();
    Vector2 RThumbstickMoved();
    bool LThumbstickTouched();
    bool RThumbstickTouched();

    //button
    bool ButtonOneDown();
    bool ButtonTwoDown();
    bool ButtonThreeDown();
    bool ButtonFourDown();
    bool ButtonOnePressed();
    bool ButtonTwoPressed();
    bool ButtonThreePressed();
    bool ButtonFourPressed();
    bool ButtonOneUp();
    bool ButtonTwoUp();
    bool ButtonThreeUp();
    bool ButtonFourUp();

    bool GetMenu();
    // touch only for oculus
    bool RIndexTouched();
    bool LIndexTouched();

}
