﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHaptic
{

    void HapticPaulse(VRController controller,AudioClip clip);
    void HapticPaulse(VRController controller,float strength,int duration);
}
