﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using HighlightingSystem;

public enum VRRayCasterType
{
    Ray,
    Sphere //Sphere can have a bigger hit area
}

public class VRRayCaster : MonoBehaviour {

    public LayerMask rayCastingLayer;//valid layer that user can ray cast to
	public Vector3 rayCasterOffset = Vector3.zero;
    public float distance = 2f;//raycast distance
    public float rayWidth = 0.005f;
    public bool isRayCasterOn = true;
    public bool shouldDrawRay = true;
    public bool shouldDrawRealRayLength = true;//the length will be controller to hig point
    public bool shouldDrawRayWhenInteractableUsedByRayCaster = false;
    public Color rayColor = Color.red;
    public VRRayCasterType casterType = VRRayCasterType.Ray;
    public float sphereRayRadius = 0.2f; //only useful when casterType is sphere
    
    [HideInInspector]
    public InteractableBase currentRaycasterInteractable = null;
    [HideInInspector]
    public Vector3 hitPoint;

    private bool isHit = false;
    private Highlighter highlighter;
    private LineRenderer casterLine;
    private float realRayLength;

    //event
    public event Action OnRayCasterEnter;
    public event Action OnRayCasterStay;
    public event Action OnRayCasterExit;

    /*
    public Highlighter Highlighter
    {
        get
        {
            return highlighter;
        }

    }
    */

    // Use this for initialization
    void Start () {

        this.realRayLength = distance;

    }
	
	// Update is called once per frame
	void Update () {

        this.isHit = false;

        //if this raycaster are using interactable  interactable then should not interepted it
        if (this.isRayCasterOn && (this.currentRaycasterInteractable == null || !this.IsInteractableUsedByRayCaster()))
        {

            //ray from hand or head,direction is forward
			Ray ray = new Ray(this.gameObject.transform.parent.TransformPoint(this.gameObject.transform.localPosition + this.rayCasterOffset), this.gameObject.transform.forward);

            RaycastHit hit;

            if (this.casterType == VRRayCasterType.Ray)
            {
                if (Physics.Raycast(ray, out hit, this.distance, this.rayCastingLayer))
                {
                    this.isHit = true;
                    this.AfterHitBehaviour(hit);

                }
                else
                {
                    this.AfterUnhitBehiviour();

                }
            }
            else if (this.casterType == VRRayCasterType.Sphere)
            {
                if (Physics.SphereCast(ray, this.sphereRayRadius, out hit, this.distance, this.rayCastingLayer))
                {

                    this.AfterHitBehaviour(hit);

                }
                else
                {
                    this.AfterUnhitBehiviour();

                }
            }

            

        }

        //draw ray
        if (this.shouldDrawRay)
        {
            if (((this.shouldDrawRayWhenInteractableUsedByRayCaster && this.IsInteractableUsedByRayCaster()) || !this.IsInteractableUsedByRayCaster()))
            {
                if (this.isHit)
                {
                    this.DrawCastedRay(this.gameObject.transform.parent.TransformPoint(this.gameObject.transform.localPosition + this.rayCasterOffset), this.hitPoint, this.rayColor);
                }
                else
                {
                    this.DrawCastedRay(this.gameObject.transform.parent.TransformPoint(this.gameObject.transform.localPosition + this.rayCasterOffset), (this.gameObject.transform.parent.TransformPoint(this.gameObject.transform.localPosition + this.rayCasterOffset) + this.gameObject.transform.forward * realRayLength), this.rayColor);
                }
                
                //this.DrawCastedRay(this.gameObject.transform.position, (this.gameObject.transform.position + this.gameObject.transform.forward * realRayLength), this.rayColor);
            }
            else
            {
                this.HideCasterRay();
            }

        }

    }

    //TODO: maybe TurnOnHighlighter and TurnOffHighlighter is not useful anymore
    public void TurnOnHighlighter()
    {
        if (this.highlighter != null)
        {
            this.highlighter.ConstantOn();
        }
    }

    public void TurnOffHighlighter()
    {
        if (this.highlighter != null)
        {
            this.highlighter.ConstantOff();
        }
    }

    //helper
    /*
    bool IsHit()
    {
        return this.currentRaycasterInteractable != null;
    }
    */
    void AfterUnhitBehiviour()
    {
        //set ray real length
        realRayLength = this.distance;

        //unhighlight
        if (this.highlighter != null)
        {
            this.highlighter.Off();
            //this.highlighter.ConstantOff();
            this.highlighter = null;
        }

        if (this.currentRaycasterInteractable != null)
        {
            if (this.OnRayCasterExit != null)
            {
                this.OnRayCasterExit();
            }

            this.currentRaycasterInteractable.isRayCastered = false;
            this.currentRaycasterInteractable = null;
        }
    }

    void AfterHitBehaviour(RaycastHit hit)
    {
        if (this.currentRaycasterInteractable == null || hit.collider.gameObject != this.currentRaycasterInteractable.gameObject)
        {
            //unhighlight
            if (this.highlighter != null)
            {
                this.highlighter.Off();
                //this.highlighter.ConstantOff();
            }

            //set currentRaycasterInteractable
            if (this.currentRaycasterInteractable != null)
            {
                if (this.OnRayCasterExit != null)
                {
                    this.OnRayCasterExit();
                }

                this.currentRaycasterInteractable.isRayCastered = false;
            }

            this.currentRaycasterInteractable = hit.collider.gameObject.GetComponent<InteractableBase>();

            /*
            //will not raycast using,triggered or already raycasted object
            if (this.currentRaycasterInteractable != null && (this.currentRaycasterInteractable.isRayCastered || this.currentRaycasterInteractable.isUsing || this.currentRaycasterInteractable.isTriggered))
            {
                this.currentRaycasterInteractable = null;
            }
            */

            //set ray real length
            if (this.currentRaycasterInteractable != null)
            {
                this.realRayLength = (hit.point - this.gameObject.transform.position).magnitude;
                this.hitPoint = hit.point;
            }
            else
            {
                //this.realRayLength = this.distance;
                this.realRayLength = (hit.point - this.gameObject.transform.position).magnitude;
                this.hitPoint = hit.point;
            }


            //set new highlight
            if (this.currentRaycasterInteractable != null && this.currentRaycasterInteractable.gameObject.GetComponentInChildren<Highlighter>() != null)
            {
                this.highlighter = this.currentRaycasterInteractable.GetComponentInChildren<Highlighter>();

                if (this.currentRaycasterInteractable.isUsing)
                {
                    this.highlighter.Off();
                }
                else
                {
                    this.highlighter.On();
                }

                

                this.currentRaycasterInteractable.isRayCastered = true;

                if (this.OnRayCasterEnter != null)
                {
                    this.OnRayCasterEnter();
                }

            }
            else if (this.currentRaycasterInteractable != null && this.currentRaycasterInteractable.gameObject.GetComponentInChildren<Highlighter>() == null)
            {

                this.highlighter = null;
                this.currentRaycasterInteractable.isRayCastered = true;

                if (this.OnRayCasterEnter != null)
                {
                    this.OnRayCasterEnter();
                }

            }
            else
            {
                this.highlighter = null;

            }

        }
        else
        {

            //set highlight
            if (this.currentRaycasterInteractable != null && this.highlighter != null)
            {
                if (this.currentRaycasterInteractable.isUsing)
                {
                    this.highlighter.Off();
                    //this.highlighter.ConstantOff();

                }
                else
                {
                    this.highlighter.On();
                    //this.highlighter.ConstantOn();
                }

            }

            //set ray real length
            realRayLength = (hit.point - this.gameObject.transform.position).magnitude;
            this.hitPoint = hit.point;

            if (this.OnRayCasterStay != null)
            {
                this.OnRayCasterStay();
            }
        }
    }

    void DrawCastedRay(Vector3 start, Vector3 end, Color color)
    {
        if (this.casterLine == null)
        {
            var casterLineGameObject = new GameObject();
            casterLineGameObject.transform.parent = this.gameObject.transform;

            this.casterLine = casterLineGameObject.AddComponent<LineRenderer>();
            casterLine.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
            casterLine.startColor = color;
            casterLine.endColor = color;
            casterLine.startWidth = this.rayWidth;
            casterLine.endWidth = this.rayWidth;
        }

        if ( !this.casterLine.gameObject.activeInHierarchy)
        {
            this.casterLine.gameObject.SetActive(true);
            casterLine.startWidth = this.rayWidth;
            casterLine.endWidth = this.rayWidth;
        }

        casterLine.SetPosition(0, start);
        casterLine.SetPosition(1, end);

    }

    public void HideCasterRay()
    {
        if (this.casterLine != null && this.casterLine.gameObject.activeInHierarchy)
        {
            this.casterLine.gameObject.SetActive(false);
        }
    }

    bool IsInteractableUsedByRayCaster()
    {
        return this.currentRaycasterInteractable != null && this.currentRaycasterInteractable.isUsing && this.currentRaycasterInteractable.PrimaryController.vrRayCaster == this;
    }

}
