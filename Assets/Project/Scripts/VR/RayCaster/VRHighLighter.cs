﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class VRHighLighter : MonoBehaviour {

    public Color highLighterColor;

    [Tooltip("The thickness of the outline effect")]
    public float thickness = 1f;
    [Tooltip("The GameObjects to use as the model to outline. If one isn't provided then the first GameObject with a valid Renderer in the current GameObject hierarchy will be used.")]
    public GameObject[] customOutlineModels;


    protected Material stencilOutline;
    protected GameObject[] highlightModels;
    protected string[] copyComponents = new string[] { "UnityEngine.MeshFilter", "UnityEngine.MeshRenderer" };


    // Use this for initialization
    void Start()
    {
        if (stencilOutline == null)
        {
            stencilOutline = Instantiate((Material)Resources.Load("Materials/HighLightMaterial"));
        }

        ResetHighlighter();
    }


    /// <summary>
    /// The ResetHighlighter method creates the additional model to use as the outline highlighted object.
    /// </summary>
    public void ResetHighlighter()
    {
        DeleteExistingHighlightModels();
        //If the custom models have been set then use these to override any set paths.
        ResetHighlighterWithCustomModels();
        //if no highlights set then try falling back
        ResetHighlightersWithCurrentGameObject();
    }

    /// <summary>
    /// The Highlight method initiates the outline object to be enabled and display the outline colour.
    /// </summary>
    /// <param name="color">not used</param>
    /// <param name="duration">Not used.</param>
    public void Highlight(Color? color = null, float duration = 0f)
    {
        if (highlightModels != null && highlightModels.Length > 0)
        {
            stencilOutline.SetFloat("_Thickness", thickness);
            stencilOutline.SetColor("_OutlineColor", highLighterColor);

            for (int i = 0; i < highlightModels.Length; i++)
            {
                if (highlightModels[i])
                {
                    highlightModels[i].SetActive(true);
                }
            }
        }
    }

    /// <summary>
    /// The Unhighlight method hides the outline object and removes the outline colour.
    /// </summary>
    /// <param name="color">Not used.</param>
    /// <param name="duration">Not used.</param>
    public void Unhighlight(Color? color = null, float duration = 0f)
    {
        if (highlightModels != null)
        {
            for (int i = 0; i < highlightModels.Length; i++)
            {
                if (highlightModels[i])
                {
                    highlightModels[i].SetActive(false);
                }
            }
        }
    }

    protected virtual void OnEnable()
    {
        if (customOutlineModels == null)
        {
            customOutlineModels = new GameObject[0];
        }

    }

    protected virtual void OnDestroy()
    {
        if (highlightModels != null)
        {
            for (int i = 0; i < highlightModels.Length; i++)
            {
                if (highlightModels[i])
                {
                    Destroy(highlightModels[i]);
                }
            }
        }
        Destroy(stencilOutline);
    }

    protected virtual void ResetHighlighterWithCustomModels()
    {
        if (customOutlineModels != null && customOutlineModels.Length > 0)
        {
            highlightModels = new GameObject[customOutlineModels.Length];
            for (int i = 0; i < customOutlineModels.Length; i++)
            {
                highlightModels[i] = CreateHighlightModel(customOutlineModels[i]);
            }
        }
    }


    protected virtual void ResetHighlightersWithCurrentGameObject()
    {
        if (highlightModels == null || highlightModels.Length == 0)
        {
            highlightModels = new GameObject[1];
            highlightModels[0] = CreateHighlightModel(null);
        }
    }

    protected virtual void DeleteExistingHighlightModels()
    {
        var meshs = GetComponentsInChildren<MeshFilter>();

        for (int i = 0; i < meshs.Length; i++)
        {
            if (meshs[i].name.Contains("_HighlightModel"))
            {
                Destroy(meshs[i].gameObject);
            }
        }

        /*
        var existingHighlighterObjects = GetComponentsInChildren<VRTK_PlayerObject>(true);
        for (int i = 0; i < existingHighlighterObjects.Length; i++)
        {
            if (existingHighlighterObjects[i].objectType == VRTK_PlayerObject.ObjectTypes.Highlighter)
            {
                Destroy(existingHighlighterObjects[i].gameObject);
            }
        }
        */
    }

    protected virtual GameObject CreateHighlightModel(GameObject givenOutlineModel)
    {
        if (givenOutlineModel != null)
        {
            givenOutlineModel = (givenOutlineModel.GetComponent<Renderer>() ? givenOutlineModel : givenOutlineModel.GetComponentInChildren<Renderer>().gameObject);
        }

        GameObject copyModel = givenOutlineModel;
        if (copyModel == null)
        {
            copyModel = (GetComponent<Renderer>() ? gameObject : GetComponentInChildren<Renderer>().gameObject);
        }

        if (copyModel == null)
        {
            throw new System.Exception("to add the highlighter to");
        }

        GameObject highlightModel = new GameObject(name + "_HighlightModel");
        highlightModel.transform.SetParent(copyModel.transform.parent, false);
        highlightModel.transform.localPosition = copyModel.transform.localPosition;
        highlightModel.transform.localRotation = copyModel.transform.localRotation;
        highlightModel.transform.localScale = copyModel.transform.localScale;
        highlightModel.transform.SetParent(transform);

        foreach (var component in copyModel.GetComponents<Component>())
        {
            
            if (Array.IndexOf(copyComponents, component.GetType().ToString()) >= 0)
            {
                Utility.CloneComponent(component, highlightModel);
            }
        }

        var copyMesh = copyModel.GetComponent<MeshFilter>();
        var highlightMesh = highlightModel.GetComponent<MeshFilter>();
        if (highlightMesh)
        {
            List<CombineInstance> combine = new List<CombineInstance>();

            for (int i = 0; i < copyMesh.mesh.subMeshCount; i++)
            {
                CombineInstance ci = new CombineInstance();
                ci.mesh = copyMesh.mesh;
                ci.subMeshIndex = i;
                ci.transform = copyMesh.transform.localToWorldMatrix;
                combine.Add(ci);
            }

            highlightMesh.mesh = new Mesh();
            highlightMesh.mesh.CombineMeshes(combine.ToArray(), true, false);

            highlightModel.GetComponent<Renderer>().material = stencilOutline;
        }
        highlightModel.SetActive(false);

        return highlightModel;
    }

    
	
	// Update is called once per frame
	void Update () {
		
	}

}
