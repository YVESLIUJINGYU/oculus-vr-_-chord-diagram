﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class GameStageTrigger : MonoBehaviour {

    public GameStageBasic gameStage;
    public bool isOneTimeTrigger = false;


    protected GameManager gameManager = GameManager.Shared;
    private bool isTriggered = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (this.gameManager.IsRightOrLeftController(other.gameObject)
            && (!this.isOneTimeTrigger || (this.isOneTimeTrigger && !this.isTriggered)) )
        {
            if (this.gameStage != null)
            {
                this.gameStage.NextStage();
                this.isTriggered = true;

                this.AfterTrigger();

            }
        }
    }

    protected virtual void AfterTrigger()
    {

    }

}
