﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Assertions;

public class GameStageBasic : MonoBehaviour {

    //event will trigger to start this stage
    public UnityEvent gameStageStartEvent;
    public UnityEvent gameStageEndEvent;
    public GameStage thisGameStage = 0;
    public GameStage nextGameStage = 0;
    
    protected GameManager gameManager = GameManager.Shared;


    // Use this for initialization
    protected virtual void Start () {

        this.AssertCheck();

        this.gameManager.OnGameStageChanged += this.HandleGameStageChanged;

    }

    protected virtual void OnDestroy()
    {
        this.gameManager.OnGameStageChanged -= this.HandleGameStageChanged;
    }

    // Update is called once per frame
    void Update () {
		
	}

    void HandleGameStageChanged(GameStage gameStage)
    {
        if (gameStage == this.thisGameStage)
        {
            this.gameStageStartEvent.Invoke();
        }
    }

    public void NextStage()
    {
        if (this.thisGameStage == this.gameManager.currentGameStage)
        {
            this.gameManager.ChangeGameStage(this.nextGameStage);
            this.gameStageEndEvent.Invoke();
        }
        
    }


    //helper
    void AssertCheck()
    {
        Assert.IsFalse(this.thisGameStage == GameStage.None, "missing this game stage");
        Assert.IsFalse(this.nextGameStage == GameStage.None, "missing next game stage");
    }

}
