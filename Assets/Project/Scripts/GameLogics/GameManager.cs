﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public enum GameStage
{
    None = 0,
    TutorialTeleportation,
    TutorialOpenDoor,
    TutorialGrab,
    EnterHall,
    ConversationWithAI,
    MiniGameStart,

    //fire game
    FireGameStart,
    FireGameStage1,
    FireGameStage2

}

public class GameManager {

    //game stage
    public GameStage currentGameStage = (GameStage)1;
    public event Action<GameStage> OnGameStageChanged;

    //managers
    public VRManagerBase vrManager;
    public IInput inputManager;
    public IHaptic haptic;
    public IndicatorsManager indicatorsManager;
    public SystemAudoManager systemAudioManager;
    public VRInput vrInput;


    //singleton
    private static volatile GameManager sharedInstance;
    private static object syncRoot = new System.Object();

    private GameManager()
    {

#if VR_OCULUS
        this.vrManager = OculusManager.Shared;
        this.inputManager = OculusManager.Shared;
        this.haptic = OculusManager.Shared;

#elif VR_HTCVIVE
        this.vrManager = HTCViveManager.Shared;
        this.inputManager = HTCViveManager.Shared;
        this.haptic = HTCViveManager.Shared;

#endif


    }

    public static GameManager Shared
    {
        get
        {
            if (sharedInstance == null)
            {
                lock (syncRoot)
                {
                    if (sharedInstance == null)
                        sharedInstance = new GameManager();
                }
            }

            return sharedInstance;
        }
    }

    //game stage change
    public void ChangeGameStage(GameStage gameStage)
    {
        this.OnGameStageChanged(gameStage);
        this.currentGameStage = gameStage;
    }

    //helper
    public bool IsHeadset(GameObject someGameObject)
    {
        return (this.vrManager.headset != null && (someGameObject == this.vrManager.headset.gameObject));
    }

    public bool IsLeftController(GameObject someGameObject)
    {
        return (this.vrManager.leftController != null && (someGameObject == this.vrManager.leftController.gameObject));
    }

    public bool IsRightController(GameObject someGameObject)
    {
        return (this.vrManager.rightController != null && (someGameObject == this.vrManager.rightController.gameObject));
    }

    public bool IsRightOrLeftController(GameObject someGameObject)
    {
        return this.IsLeftController(someGameObject) || this.IsRightController(someGameObject);
    }

}
