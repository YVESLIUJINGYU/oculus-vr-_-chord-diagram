﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SuperPowerTypes
{
    None,
    ChangeSize,
    SuperJump
}

public class VRPlayer : MonoBehaviour {

    [HideInInspector]
    public int playerID;

    [Header("Super Power")]
    public SuperPowerTypes superPowerType = SuperPowerTypes.None;
    [HideInInspector]
    public SuperPowerBase superPower;

	// Use this for initialization
	void Start () {

        if (this.superPowerType == SuperPowerTypes.ChangeSize)
        {
            this.superPower = this.gameObject.AddComponent<ChangeSize>();
        }
        else if (this.superPowerType == SuperPowerTypes.SuperJump)
        {
            this.superPower = this.gameObject.AddComponent<SuperJump>();
        }

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void ChangeSuperPower(SuperPowerTypes spType)
    {
        if (this.superPowerType != SuperPowerTypes.None && this.superPower != null)
        {
            Destroy(this.superPower);
            this.superPower = null;
        }

        this.superPowerType = spType;

        if (this.superPowerType == SuperPowerTypes.ChangeSize)
        {
            this.superPower = this.gameObject.AddComponent<ChangeSize>();
        }
        else if (this.superPowerType == SuperPowerTypes.SuperJump)
        {
            this.superPower = this.gameObject.AddComponent<SuperJump>();
        }
    }

}
