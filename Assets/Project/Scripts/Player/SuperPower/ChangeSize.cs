﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSize : SuperPowerBase {

    public float sizeRatio = 1;
    public GameObject player;

    private bool isLefthandTrigged = false;
    private bool isRightHandTrigged = false;
    private float originalNearPlane = 0.1f;


	// Use this for initialization
	void Start () {

        if (Camera.main != null)
        {
            this.originalNearPlane = Camera.main.nearClipPlane;
        }

	}
	
	// Update is called once per frame
	void Update () {

        if (this.isLefthandTrigged && !this.isRightHandTrigged)
        {
            this.EnlargePlayer();
        }
        else if (!this.isLefthandTrigged && this.isRightHandTrigged)
        {
            this.ShrinkPlayer();
        }

	}

    public override void OnSuperPowerIndexDown(VRController controller)
    {
        base.OnSuperPowerIndexDown(controller);

        if (this.player == null)
        {
            this.player = controller.GetPlayer().gameObject;
        }

        if (controller.controllerType == VRControllerTypes.Left)
        {
            this.isLefthandTrigged = true;
        }
        else if (controller.controllerType == VRControllerTypes.Right)
        {
            this.isRightHandTrigged = true;
        }

    }

    public override void OnSuperPowerIndexUp(VRController controller)
    {
        base.OnSuperPowerIndexUp(controller);

        if (controller.controllerType == VRControllerTypes.Left)
        {
            this.isLefthandTrigged = false;
        }
        else if (controller.controllerType == VRControllerTypes.Right)
        {
            this.isRightHandTrigged = false;
        }
    }

    //helper
    private void EnlargePlayer()
    {
        var enlargeSpeed = 0.5f;

        this.sizeRatio = this.sizeRatio * (1 + enlargeSpeed * Time.deltaTime);

        this.player.transform.localScale = Vector3.one * this.sizeRatio;
        Camera.main.nearClipPlane = this.originalNearPlane * this.sizeRatio;

    }

    private void ShrinkPlayer()
    {
        var shrinkSpeed = 0.5f;

        this.sizeRatio = this.sizeRatio * Mathf.Pow(shrinkSpeed,Time.deltaTime);

        this.player.transform.localScale = Vector3.one * this.sizeRatio;
        Camera.main.nearClipPlane = this.originalNearPlane * this.sizeRatio;
    }

}
