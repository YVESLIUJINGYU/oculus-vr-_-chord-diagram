﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class SuperJump : SuperPowerBase {

    [Header("Super Jump Setup")]
    public bool isJumping = false;
    public float gravity = -2.0f;

    //jump hight based on user real jump height
    public float heightRatio = 60f;

    //jump hight is fixed
    public bool isJumpHeightFixed = false;
    public float jumpHeight = 10f;//only valid when isJumpHeightFixed equal true

    private VRHeadset headSet;

    //rig
    [HideInInspector]
    public Rigidbody rig;

    // Use this for initialization
    void Start () {

        this.headSet = this.gameObject.GetComponentInChildren<VRHeadset>();
        this.rig = this.gameObject.GetComponent<Rigidbody>();

    }
	
	// Update is called once per frame
	void Update () {

        

	}

    private void FixedUpdate()
    {
        if (this.IsStartToJump())
        {
            if (this.isJumpHeightFixed)
            {
                this.rig.velocity = new Vector3(0, Mathf.Abs(this.GetJumpInitalVelocity(0)),0);
            }
            else
            {
                this.rig.velocity = new Vector3(0, Mathf.Abs(this.GetJumpInitalVelocity(this.headSet.velocity.y)), 0);
            }

            this.isJumping = true;

        }

        if (this.isJumping)
        {
            if (!this.isJumpHeightFixed)
            {
                this.rig.AddForce(this.ExtraGravity() * this.rig.mass);
            }

        }

        if (this.isJumping && this.headSet.velocity.y <= 0.5  && this.headSet.acceleration.y >= -0.5)
        {
            this.isJumping = false;
        }

    }

    //collider
    private void OnCollisionEnter(Collision collision)
    {
        if (this.isJumping)
        {
            this.isJumping = false;
        }
    }

    //helper
    private float GetJumpInitalVelocity(float realInitalVelocity = 0)
    {
        if (!this.isJumpHeightFixed)
        {
            return this.heightRatio * realInitalVelocity;
        }
        else
        {
            var gravity = Mathf.Abs(Physics.gravity.y);

            return Mathf.Sqrt(2 * gravity * this.jumpHeight);
        }
    }

    private Vector3 ExtraGravity()
    {
        if (!this.isJumpHeightFixed)
        {
            return (this.heightRatio - 1) * Physics.gravity;
        }
        else
        {
            return Physics.gravity;
        }
    }

    private bool IsStartToJump()
    {
        return !this.isJumping && this.headSet.acceleration.y <= this.gravity && Mathf.Abs(this.headSet.acceleration.y) <= 12 && this.headSet.velocity.y >=1;
    }


}
