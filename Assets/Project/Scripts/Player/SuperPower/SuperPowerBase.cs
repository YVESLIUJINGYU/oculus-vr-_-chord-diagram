﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperPowerBase : MonoBehaviour {

    [Header("Basic Setup")]
    public AudioClip hapticClip;

    [HideInInspector]
    public bool isUsing = false;//flag for checking if superpower is using right now

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //input
    public virtual void OnSuperPowerGrabDown(VRController controller)
    {


    }

    public virtual void OnSuperPowerGrabUp(VRController controller)
    {


    }

    public virtual void OnSuperPowerGrabPressed(VRController controller, float grabAxis)
    {


    }

    public virtual void OnSuperPowerIndexDown(VRController controller)
    {


    }

    public virtual void OnSuperPowerIndexUp(VRController controller)
    {


    }

    public virtual void OnSuperPowerIndexPressed(VRController controller, float indexAxis)
    {


    }

    //button
    public virtual void OnSuperPowerButtonOnePressed(VRController controller, bool isPressed)
    {


    }

    public virtual void OnSuperPowerButtonTwoPressed(VRController controller, bool isPressed)
    {


    }

    public virtual void OnSuperPowerButtonThreePressed(VRController controller, bool isPressed)
    {


    }

    public virtual void OnSuperPowerButtonFourPressed(VRController controller, bool isPressed)
    {


    }

}
