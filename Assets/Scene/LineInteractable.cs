﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineInteractable : InteractableBase {

    Material originalMaterial;
    Material selectedLineRenderer;
    GameObject panelPrefab;
    private bool isRascasted;
    private bool hasEntered; 

    // Use this for initialization
    void Start () 
    {
        base.Start();
        base.rig.isKinematic = true;
        // Get the original material of selected line
        originalMaterial = this.col.transform.parent.gameObject.GetComponent<LineRenderer>().material;
        //Load material from Asset
        selectedLineRenderer = Resources.Load("Materials/SelectedLineRenderer", typeof(Material)) as Material;
        //Load information panel prefab
        panelPrefab = Resources.Load("Prefab/InfoPanel", typeof(GameObject)) as GameObject;

        isRascasted = false;
        hasEntered = false;
    }

    void Update()
    {
        if (isRascasted)
        {
            // Get the line rascasted according to the collider
            GameObject selectedLine = this.col.transform.parent.gameObject;
            // Draw infomation panel if not exists
            if (!selectedLine.transform.Find("InfoPanel(Clone)"))
            {
                selectedLine.GetComponent<LineRenderer>().material = selectedLineRenderer;
                Transform startPoint = selectedLine.GetComponent<ChordDiagramLine>().startPoint;
                Transform endPoint = selectedLine.GetComponent<ChordDiagramLine>().endPoint;
                // Draw Infomation panel and set its position, parent and text
                GameObject infoPanel = Instantiate(panelPrefab, this.col.transform.position * 3, Quaternion.identity);
                infoPanel.transform.SetParent(selectedLine.transform);
                Transform infoText = infoPanel.transform.Find("Panel/Information");
                infoText.GetComponent<Text>().text = "Line From " + startPoint.name + " To " + endPoint.name;
                // Set positions of the line renderer between infomation panel and gate ground
                LineRenderer panelLine = infoPanel.GetComponent<LineRenderer>();
                panelLine.SetPosition(0, this.col.transform.position);
                panelLine.SetPosition(1, this.col.transform.position * 2.5f);
            }
            else { }
        }
        else
        {
            if (hasEntered)
            {
                GameObject selectedLine = this.col.transform.parent.gameObject;
                selectedLine.GetComponent<LineRenderer>().material = originalMaterial;
                Destroy(selectedLine.transform.Find("InfoPanel(Clone)").gameObject);
                hasEntered = false;
            }
            else { }
        }
    }

    public override void OnInteractableRayCasterEnter(VRController controller)
    {
        base.OnInteractableRayCasterEnter(controller);
        isRascasted = true;
        hasEntered = true;
    }

    public override void OnInteractableRayCasterExit(VRController controller)
    {
        base.OnInteractableRayCasterExit(controller);
        isRascasted = false;
    }

}