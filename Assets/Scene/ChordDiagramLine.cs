﻿using UnityEngine;
using System.Collections;

public class ChordDiagramLine : MonoBehaviour
{

    // Parent of line renderer
    GameObject gateRound;

    // Parent of each sphere collider
    GameObject sphereParent;

    // The start point and end point of line renderer
    public Transform startPoint;
    public Transform endPoint;

    // Parameter of the material of line renderer
    public float _metallic = 1.0f;

    // Time to move from sunrise to sunset position, in seconds.
    float journeyTime = 5.0f;

    // The number of segment of line renderer
    int segment = 300;
    int countSegment = 1;

    // The time at which the animation started.
    private float startTime;

    void Start()
    {
        gateRound = GameObject.Find("GateRound");

        // Note the time at the start of the animation.
        startTime = Time.time;

        //Load material from Asset
        Material line = Resources.Load("Materials/LineRenderer", typeof(Material)) as Material;

        //Clone material for each line renderer and set up its parameters
        Material newLine = Instantiate(line);
        LineRenderer lineRenderer = this.gameObject.AddComponent<LineRenderer>();
        newLine.SetFloat("_Metallic", _metallic);
        lineRenderer.material = newLine;
        lineRenderer.positionCount = segment;
        lineRenderer.SetPosition(0, startPoint.position);
        lineRenderer.startWidth = 0.1f;
        lineRenderer.endWidth = 0.1f;

        // Create new game object if not exists for holding sphere colliders
        if (GameObject.Find("LineFrom" + startPoint.name + "To" + endPoint.name))
        {
            sphereParent = GameObject.Find("LineFrom" + startPoint.name + "To" + endPoint.name);
        }
        else
        {
            sphereParent = new GameObject("LineFrom" + startPoint.name + "To" + endPoint.name);
        }
        sphereParent.transform.SetParent(gateRound.transform);
        sphereParent.layer = 10;

    }

    void Update()
    {
        LineRenderer lineRenderer = GetComponent<LineRenderer>();

        if (countSegment < segment)
        {

            //Creat sphere as the traject of the arc
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.SetParent(sphereParent.transform);
            sphere.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            sphere.gameObject.GetComponent<Renderer>().enabled = false;
            sphere.gameObject.GetComponent<Collider>().isTrigger = true;
            sphere.layer = 10;
            sphere.gameObject.AddComponent<LineInteractable>();

            // The center of the arc
            Vector3 center = (startPoint.position + endPoint.position - new Vector3(0, 0, -0.7f));

            // Interpolate over the arc relative to center
            Vector3 startRelCenter = startPoint.position - center;
            Vector3 endRelCenter = endPoint.position - center;

            // The fraction of the animation that has happened so far is
            // equal to the elapsed time divided by the desired time for
            // the total journey.
            float fracComplete = (Time.time - startTime) / journeyTime;
            
            Vector3 position = Vector3.Slerp(startRelCenter, endRelCenter, fracComplete);
            position += center;
            position = new Vector3(position.x, position.y, -0.1f);
            lineRenderer.SetPosition(countSegment, position);
            sphere.transform.position = position;

            //Count Segment
            countSegment += 1;

        }

    }
}
