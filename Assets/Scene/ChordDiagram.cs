﻿using UnityEngine;
using System.Collections;

public class ChordDiagram : MonoBehaviour
{
    public Transform startPoint;
    public Transform endPoint;

    // Time to move from sunrise to sunset position, in seconds.
    float journeyTime = 3.0f;

    // Count Time to stop animation
    float countTime = 0;

    // The time at which the animation started.
    private float startTime;

    void Start()
    {
        // Note the time at the start of the animation.
        startTime = Time.time;
    }

    void Update()
    {
        if (countTime <= journeyTime)
        {
            //Creat sphere as the traject of the arc
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

            // The center of the arc
            Vector3 center = (startPoint.position + endPoint.position - new Vector3(0, 0, -0.7f));

            // Interpolate over the arc relative to center
            Vector3 startRelCenter = startPoint.position - center;
            Vector3 endRelCenter = endPoint.position - center;

            // The fraction of the animation that has happened so far is
            // equal to the elapsed time divided by the desired time for
            // the total journey.
            float fracComplete = (Time.time - startTime) / journeyTime;

            sphere.transform.position = Vector3.Slerp(startRelCenter, endRelCenter, fracComplete);

            sphere.transform.position += center;

            sphere.transform.position = new Vector3(sphere.transform.position.x, sphere.transform.position.y, -0.1f);

            //Count Time
            countTime += Time.deltaTime;

        }
    }
}
