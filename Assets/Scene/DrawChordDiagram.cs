﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawChordDiagram : MonoBehaviour {

    List<Vector3> disease = new List<Vector3>();
    GameObject lineR;
    GameObject gateRound;

	// Use this for initialization
	void Start () {

        // Add script "LineInteractable" to GateRound
        //this.gameObject.AddComponent<LineInteractable>();

        gateRound = GameObject.Find("GateRound");

        disease.Add(new Vector3(1, 3, 1));
        //disease.Add(new Vector3(1, 5, 1));
        //disease.Add(new Vector3(1, 7, 2));
        //disease.Add(new Vector3(2, 4, 2));
        //disease.Add(new Vector3(3, 6, 3));
        //disease.Add(new Vector3(4, 9, 3));
        //disease.Add(new Vector3(1, 10, 4));
        disease.Add(new Vector3(5, 12, 4));
        disease.Add(new Vector3(6, 11, 5));
        disease.Add(new Vector3(8, 10, 5));
        disease.Add(new Vector3(9, 11, 6));

        Vector3[] dis = disease.ToArray();

        for (int i = 0; i < dis.Length; i++)
        {

            // Convert Float to Int
            int numStart = (int)dis[i].x;
            int numEnd = (int)dis[i].y;

            //Create new empty game object for each line renderer and set its parent
            if (GameObject.Find("LineFromSphere" + numStart + "ToSphere" + numEnd))
            {
                lineR = GameObject.Find("LineFromSphere" + numStart + "ToSphere" + numEnd);
            }
            else
            {
                lineR = new GameObject("LineFromSphere" + numStart + "ToSphere" + numEnd);
            }
            lineR.transform.SetParent(gateRound.transform);
            lineR.layer = 10;

            //Get the start point and end point of line renderer
            GameObject startPoint = GameObject.Find("Sphere" + numStart);
            GameObject endPoint = GameObject.Find("Sphere" + numEnd);

            //Add script "ChordDiagramLine" to the new created game object
            ChordDiagramLine script =  lineR.AddComponent<ChordDiagramLine>();

            //Set the start and end point of line renderer 
            script.startPoint = startPoint.transform;
            script.endPoint = endPoint.transform;

            //Set the Matallic of material 
            script._metallic = dis[i].z / 6;

        }
    }
	
}
