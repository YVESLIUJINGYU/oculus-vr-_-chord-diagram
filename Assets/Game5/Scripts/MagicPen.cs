﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicPen : MonoBehaviour {

    public float pushTime = 0.3f;
    public float moveDistance = 0.1f;

    private Vector3 originalLocalPosition;
    private Vector3 finalLocalPosition;

    // Use this for initialization
    void Start () {

        this.originalLocalPosition = this.gameObject.transform.localPosition;
        this.finalLocalPosition = this.gameObject.transform.localPosition - Vector3.right * this.moveDistance * 10;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ExtendPen()
    {
        StartCoroutine(this.PushUp());
    }

    public void DeExtendPen()
    {
        StartCoroutine(this.PullDown());
    }

    private IEnumerator PushUp()
    {
        float time = 0;
        Vector3 startPosition = this.gameObject.transform.localPosition;
        Vector3 endPosition = this.finalLocalPosition;

        while (time <= this.pushTime)
        {
            var newPosition = Vector3.Lerp(startPosition, endPosition, Mathf.SmoothStep(0.0f, 1.0f, time / this.pushTime));
            this.gameObject.transform.localPosition = newPosition;
            time = time + Time.deltaTime;
            yield return null;

        }

        this.gameObject.transform.localPosition = endPosition;

    }

    private IEnumerator PullDown()
    {
        float time = 0;
        Vector3 startPosition = this.gameObject.transform.localPosition;
        Vector3 endPosition = this.originalLocalPosition;

        while (time <= this.pushTime)
        {
            var newPosition = Vector3.Lerp(startPosition, endPosition, Mathf.SmoothStep(0.0f, 1.0f, time / this.pushTime));
            this.gameObject.transform.localPosition = newPosition;
            time = time + Time.deltaTime;
            yield return null;

        }

        this.gameObject.transform.localPosition = endPosition;
    }

}
