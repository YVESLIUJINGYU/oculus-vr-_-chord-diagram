﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(MeshRenderer))]
public class Datafield : MonoBehaviour {

    public GameObject dataWave;
    public GameObject dataWaveGroup;
    public UnityEvent dataFieldStartEvent;
    public UnityEvent dataFieldEndEvent;
    public UnityEvent leftHandEndEvent;
    public UnityEvent rightHandEndEvent;

    [HideInInspector]
    public bool isActivated = false;
    [HideInInspector]
    public bool isLeftControllerActivated = false;
    [HideInInspector]
    public bool isRightControllerActivated = false;

    private GameManager gameManager = GameManager.Shared;
    


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if ((!this.isRightControllerActivated || !this.isLeftControllerActivated) && this.gameManager.IsRightOrLeftController(other.gameObject) && this.dataFieldStartEvent != null)
        {
            if (!this.isLeftControllerActivated && !this.isRightControllerActivated)
            {
                this.dataFieldStartEvent.Invoke();
                this.isActivated = true;

                var controlPanel = this.gameManager.vrManager.rightController.GetComponent<WristControlPanelController>();
                if (controlPanel != null)
                {
                    controlPanel.ShowControlPanel();
                }

            }

            if (this.gameManager.IsRightController(other.gameObject))
            {
                this.isRightControllerActivated = true;
            }

            if (this.gameManager.IsLeftController(other.gameObject))
            {
                this.isLeftControllerActivated = true;
            }

        }

        

    }

    private void OnTriggerExit(Collider other)
    {
        if ((this.isRightControllerActivated || this.isLeftControllerActivated) && this.gameManager.IsRightOrLeftController(other.gameObject) && this.dataFieldEndEvent != null)
        {

            if (this.gameManager.IsRightController(other.gameObject))
            {
                this.isRightControllerActivated = false;

                if (this.rightHandEndEvent != null)
                {
                    this.rightHandEndEvent.Invoke();
                }

            }

            if (this.gameManager.IsLeftController(other.gameObject))
            {
                this.isLeftControllerActivated = false;

                if (this.leftHandEndEvent != null)
                {
                    this.leftHandEndEvent.Invoke();
                }
            }

            if (!this.isLeftControllerActivated && !this.isRightControllerActivated)
            {
                this.dataFieldEndEvent.Invoke();
                this.isActivated = false;

                var controlPanel = this.gameManager.vrManager.rightController.GetComponent<WristControlPanelController>();
                if (controlPanel != null)
                {
                    controlPanel.HideControlPanel();
                }
            }

            
        }

        

    }

}
