﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatafieldResize : MonoBehaviour {

    public Datafield dataField;
    public GameObject fullArea;
    public float resizeRatioPerUnit = 3f;

    private GameManager gameManager = GameManager.Shared;
    private VRInput vrInput;
    private bool isLeftGrabbed = false;
    private bool isRightGrabbed = false;
    private float? originalControllerDistance;
    private Vector3? originalFullAreaScale;
    [HideInInspector]
    public bool isResizerOn = false;

    private void OnDestroy()
    {
        vrInput.OnLGrabDown -= this.HandleLeftGrabDown;
        vrInput.OnLGrabUp -= this.HandleLeftGrabUp;
        vrInput.OnRGrabDown -= this.HandleRightGrabDown;
        vrInput.OnRGrabUp -= this.HandleRightGrabUp;

    }

    // Use this for initialization
    void Start () {

        this.vrInput = this.gameManager.vrInput;


        if (this.vrInput == null)
        {
            throw new System.Exception("vrinput is null");
        }

        this.vrInput.OnLGrabDown += this.HandleLeftGrabDown;
        this.vrInput.OnLGrabUp += this.HandleLeftGrabUp;
        this.vrInput.OnRGrabDown += this.HandleRightGrabDown;
        this.vrInput.OnRGrabUp += this.HandleRightGrabUp;

    }
	
	// Update is called once per frame
	void Update () {

        if (this.isLeftGrabbed && this.isRightGrabbed && this.isResizerOn)
        {
            var currentControllerDistance = (this.gameManager.vrManager.leftController.transform.position - this.gameManager.vrManager.rightController.transform.position).magnitude;


            if (currentControllerDistance <= this.originalControllerDistance)
            {
                var ratio = 1 / (1 + (this.originalControllerDistance - currentControllerDistance).Value * (this.resizeRatioPerUnit));

                var currentScale = this.originalFullAreaScale * ratio;

                this.fullArea.transform.localScale = currentScale.Value;
                /*
                if (ratio <= 0.3)
                {
                    var color = this.fullArea.GetComponent<MeshRenderer>().material.color;
                    var newColor = new Color(color.r, color.g, color.b, 0.3f);
                    this.fullArea.GetComponent<MeshRenderer>().material.color = newColor;

                }
                else
                {
                    var color = this.fullArea.GetComponent<MeshRenderer>().material.color;
                    var newColor = new Color(color.r, color.g, color.b, 0);
                    this.fullArea.GetComponent<MeshRenderer>().material.color = newColor;
                }
                */

            }
            else if (currentControllerDistance > this.originalControllerDistance)
            {
                var ratio = 1 + (currentControllerDistance -this.originalControllerDistance).Value * (this.resizeRatioPerUnit);

                var currentScale = this.originalFullAreaScale * ratio;

                this.fullArea.transform.localScale = currentScale.Value;
                /*
                if (ratio <= 0.3)
                {
                    var color = this.fullArea.GetComponent<MeshRenderer>().material.color;
                    var newColor = new Color(color.r, color.g, color.b, 0.3f);
                    this.fullArea.GetComponent<MeshRenderer>().material.color = newColor;

                }
                else
                {
                    var color = this.fullArea.GetComponent<MeshRenderer>().material.color;
                    var newColor = new Color(color.r, color.g, color.b, 0);
                    this.fullArea.GetComponent<MeshRenderer>().material.color = newColor;
                }
                */
            }

        }

	}

    public void TurnOnResize()
    {
        this.isResizerOn = true;
    }

    public void TurnOffResize()
    {
        this.isResizerOn = false;
    }

    //grab
    public void HandleLeftGrabDown()
    {
        this.isLeftGrabbed = true;

        if (this.isRightGrabbed && this.originalControllerDistance == null)
        {
            this.originalControllerDistance = (this.gameManager.vrManager.leftController.transform.position - this.gameManager.vrManager.rightController.transform.position).magnitude;
            this.originalFullAreaScale = this.fullArea.transform.localScale;
        }

    }

    public void HandleLeftGrabUp()
    {
        this.isLeftGrabbed = false;
        this.originalControllerDistance = null;
        this.originalFullAreaScale = null;
    }

    public void HandleRightGrabDown()
    {
        this.isRightGrabbed = true;

        if (this.isLeftGrabbed && this.originalControllerDistance == null)
        {
            this.originalControllerDistance = (this.gameManager.vrManager.leftController.transform.position - this.gameManager.vrManager.rightController.transform.position).magnitude;
            this.originalFullAreaScale = this.fullArea.transform.localScale;
        }

    }

    public void HandleRightGrabUp()
    {
        this.isRightGrabbed = false;
        this.originalControllerDistance = null;
        this.originalFullAreaScale = null;

    }

}
