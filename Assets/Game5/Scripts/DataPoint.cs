﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataPoint : MonoBehaviour {

    public LayerMask dataFieldLayer;
    public DataPointsManager dataPointsManager;
    public float movingTime = 2f;
    public GameObject linePrefab;

    [HideInInspector]
    public bool isMoving = false;

    [SerializeField]
    private float animationTime = 0.3f;

    private Vector3 originalSize = new Vector3(0.4f, 0.4f, 0.4f);
    private Vector3 originalPosition;
    private GameManager gameManager = GameManager.Shared;
    private List<LineRenderer> lineList = new List<LineRenderer>();
    private LineRenderer currentLine;

	// Use this for initialization
	void Start () {

        if (this.dataPointsManager == null)
        {
            throw new System.Exception("dataPointsManager is null");
        }

        if (this.linePrefab == null)
        {
            throw new System.Exception("line prefab is null");
        }

        Material material = this.linePrefab.GetComponent<LineRenderer>().sharedMaterial;

        if (material.GetFloat("_MaxX") <= 0.001f)
        {
            material.SetFloat("_MaxX", this.dataPointsManager.maxX);
            material.SetFloat("_MinX", this.dataPointsManager.minX);
            material.SetFloat("_MaxY", this.dataPointsManager.maxY);
            material.SetFloat("_MinY", this.dataPointsManager.minY);
            material.SetFloat("_MaxZ", this.dataPointsManager.maxZ);
            material.SetFloat("_MinZ", this.dataPointsManager.minZ);
        }

        this.originalPosition = this.gameObject.transform.position;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        var dataField = other.gameObject.GetComponent<Datafield>();

        if (dataField != null)
        {
            this.gameObject.transform.localScale = Vector3.one * 0.01f;
            this.CreateWave(dataField,other,false);

            StartCoroutine(this.MakePointAppear());

        }

    }


    private void OnTriggerStay(Collider other)
    {
        /*
        if (other.gameObject.GetComponent<Datafield>() != null)
        {
            if (!this.gameObject.activeSelf)
            {
                this.gameObject.SetActive(true);
            }
        }
        */
    }

    private void OnTriggerExit(Collider other)
    {
        var dataField = other.gameObject.GetComponent<Datafield>();

        if (dataField != null)
        {
            this.CreateWave(dataField, other,true);
            StartCoroutine(this.MakePointDisappear());
        }

    }

    //helper
    private void CreateLine()
    {
        var line = GameObject.Instantiate(this.linePrefab,
                    this.gameObject.transform.position,
                    this.gameObject.transform.rotation,
                    this.dataPointsManager.transform);

        this.currentLine = line.GetComponent<LineRenderer>();
        this.currentLine.positionCount = 2;
        this.currentLine.SetPosition(0, this.currentLine.transform.InverseTransformPoint(this.gameObject.transform.position));
        this.lineList.Add(this.currentLine);

    }

    public void DestoryAllLines()
    {
        if (!this.isMoving)
        {
            foreach (var line in this.lineList)
            {
                if (line != null)
                {
                    Destroy(line.gameObject);
                }
                
            }

            this.lineList.Clear();
        }
        
    }

    public void RandomMove()
    {
        if (!this.isMoving)
        {
            this.DestoryAllLines();

            this.isMoving = true;
            var endPoint = this.gameObject.transform.position + 0.5f * Random.insideUnitSphere;

            if (this.gameObject.transform.localScale.magnitude >= 0.0001f)
            {
                this.CreateLine();
                StartCoroutine(this.MovePoint(endPoint,true));
            }
            else
            {
                StartCoroutine(this.MovePoint(endPoint, false));
            }

        }
        
    }

    private IEnumerator MovePoint(Vector3 endPoint,bool isInsideDataField)
    {
        float time = 0;
        Vector3 startPosition = this.gameObject.transform.position;
        Vector3 startLinePosition = Vector3.zero;
        Vector3 endPosition = endPoint;
        Vector3 endLinePosition = Vector3.zero;

        if (isInsideDataField)
        {
            startLinePosition = this.currentLine.GetPosition(0);
            endLinePosition = this.currentLine.transform.InverseTransformPoint(endPoint);
        }

        while (time <= this.movingTime)
        {
            var newPosition = Vector3.Lerp(startPosition, endPosition, time / this.movingTime);
            this.gameObject.transform.position = newPosition;

            if (isInsideDataField)
            {
                var newLinePosition = Vector3.Lerp(startLinePosition, endLinePosition, time / this.movingTime);
                this.currentLine.SetPosition(this.currentLine.positionCount - 1, newLinePosition);
            }
            

            time = time + Time.deltaTime;
            yield return null;

        }

        this.gameObject.transform.position = endPosition;

        if (isInsideDataField)
        {
            this.currentLine.SetPosition(this.currentLine.positionCount - 1, endLinePosition);
        }
        

        yield return new WaitForSeconds(1);

        this.isMoving = false;
        this.currentLine = null;
    }

    private void CreateWave(Datafield dataField, Collider other,bool isExit)
    {
        var dataWave = GameObject.Instantiate(dataField.dataWave, dataField.dataWaveGroup.transform);
        dataWave.transform.position = other.ClosestPointOnBounds(this.gameObject.transform.position);

        RaycastHit hit;
        Ray raycast = new Ray(other.gameObject.transform.position, other.gameObject.transform.position - dataWave.transform.position);
        if (Physics.Raycast(raycast,out hit,100f,this.dataFieldLayer))
        {
            dataWave.transform.forward = hit.normal;

            this.gameManager.haptic.HapticPaulse(this.gameManager.vrManager.leftController, 40, 30);
            this.gameManager.haptic.HapticPaulse(this.gameManager.vrManager.rightController, 40, 30);

        }



    }

    private IEnumerator MakePointDisappear()
    {
        float time = 0;
        Vector3 startSize = this.gameObject.transform.localScale;

        while (time <= this.animationTime)
        {
            var newSize = Vector3.Lerp(startSize, Vector3.zero, time / this.animationTime);
            this.gameObject.transform.localScale = newSize;
            time = time + Time.deltaTime;
            yield return null;

        }

        var finalSize = Vector3.zero;
        this.gameObject.transform.localScale = finalSize;

    }

    private IEnumerator MakePointAppear()
    {
        float time = 0;
        Vector3 startSize = this.gameObject.transform.localScale;

        while (time <= this.animationTime)
        {
            var newSize = Vector3.Lerp(startSize, this.originalSize, time / this.animationTime);
            this.gameObject.transform.localScale = newSize;
            time = time + Time.deltaTime;
            yield return null;

        }

        var finalSize = this.originalSize;
        this.gameObject.transform.localScale = finalSize;
    }


}
