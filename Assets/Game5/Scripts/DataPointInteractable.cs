﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DataPoint))]
public class DataPointInteractable : GrabScript {

    [Header("info ui")]
    public UIFlotingInformatiion infoUI;

    private DataPoint dataPoint;
    private DataPointsManager dataPointsManager;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        if (this.infoUI == null)
        {
            var ui = this.gameObject.GetComponentInChildren<UIFlotingInformatiion>(true);

            if (ui != null)
            {
                this.infoUI = ui;
            }
        }

        if (this.infoUI != null)
        {
            this.infoUI.targetGameobject = this.gameObject.transform;
        }

        this.dataPoint = this.gameObject.GetComponentInChildren<DataPoint>();
        this.dataPointsManager = this.dataPoint.dataPointsManager;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
	}

    public override void OnInteractableIndexPressed(VRController controller, float indexAxis)
    {
        if (controller.controllerType == VRControllerTypes.Right)
        {
            base.OnInteractableIndexPressed(controller, indexAxis);
        }

    }


    public override void OnInteractableGrabPressed(VRController controller, float grabAxis)
    {

        if (controller.controllerType == VRControllerTypes.Right)
        {
            base.OnInteractableGrabPressed(controller, grabAxis);
        }

    }


    public override void OnInteractableIndexDown(VRController controller)
    {
        if (controller.controllerType == VRControllerTypes.Right)
        {
            base.OnInteractableIndexDown(controller);
        }

    }

    public override void OnInteractableGrabDown(VRController controller)
    {
        if (controller.controllerType == VRControllerTypes.Right)
        {
            base.OnInteractableGrabDown(controller);
        }
        //base.OnInteractableGrabDown(controller);

    }

    //raycaster
    public override void OnInteractableRayCasterEnter(VRController controller)
    {
        if (controller.controllerType == VRControllerTypes.Right)
        {
            base.OnInteractableRayCasterEnter(controller);

            if (this.infoUI != null)
            {
                this.infoUI.Fadein();
                this.VibrateController(controller);
                this.dataPointsManager.FadeOutAllDataInfo(this.infoUI);
            }
        }

    }

    public override void OnInteractableRayCasterStay(VRController controller)
    {
        if (controller.controllerType == VRControllerTypes.Right)
        {
            base.OnInteractableRayCasterStay(controller);
        }

    }

    public override void OnInteractableRayCasterExit(VRController controller)
    {
        if (controller.controllerType == VRControllerTypes.Right)
        {
            base.OnInteractableRayCasterExit(controller);

        }



    }
}
