﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class DataWave : MonoBehaviour {

    private float animationTime = 0.5f;
    private SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Start () {

        this.transform.localScale = Vector3.zero;
        this.spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();

        StartCoroutine(this.ExplodeWave());


    }
	
	// Update is called once per frame
	void Update () {
        

    }

    private IEnumerator ExplodeWave()
    {
        float time = 0;
        var finalSize = Vector3.one * 5;
        var startColor = this.spriteRenderer.color;

        while (time <= this.animationTime)
        {
            var newSize = Vector3.Lerp(Vector3.zero, finalSize, time / this.animationTime);
            this.gameObject.transform.localScale = newSize;

            var newAlpha = Mathf.Lerp(1, 0, time / this.animationTime);
            this.spriteRenderer.color = new Color(this.spriteRenderer.color.r, this.spriteRenderer.color.g, this.spriteRenderer.color.b, newAlpha);

            time = time + Time.deltaTime;
            yield return null;

        }
        this.gameObject.transform.localScale = finalSize;
        this.spriteRenderer.color = new Color(this.spriteRenderer.color.r, this.spriteRenderer.color.g, this.spriteRenderer.color.b, 0);

        Destroy(this.gameObject);

    }

}
