﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WristControlPanelController : MonoBehaviour {

    public GameObject controlPanel;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowControlPanel()
    {
        this.controlPanel.SetActive(true);
    }

    public void HideControlPanel()
    {
        this.controlPanel.SetActive(false);
    }

}
