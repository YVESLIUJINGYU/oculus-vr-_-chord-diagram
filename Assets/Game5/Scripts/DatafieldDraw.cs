﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatafieldDraw : MonoBehaviour {

    public Datafield dataField;
    public DataPointsManager dataPointsManager;
    public GameObject drawingPrefab;
    public float linePointGap = 0.01f;

    private GameManager gameManager = GameManager.Shared;
    private VRInput vrInput;
    private bool isDrawing = false;
    private bool isTurnOn = false;
    private List<LineRenderer> drawingList = new List<LineRenderer>();
    private LineRenderer currentDrawing = null;
    private MagicPen pen;


    // Use this for initialization
    private void OnDestroy()
    {
        vrInput.OnRIndexDown -= this.DrawingDown;
        vrInput.OnRIndexUp -= this.DrawingUp;

    }

    // Use this for initialization
    void Start()
    {
        this.vrInput = this.gameManager.vrInput;


        if (this.vrInput == null)
        {
            throw new System.Exception("vrinput is null");
        }

        this.vrInput.OnRIndexDown += this.DrawingDown;
        this.vrInput.OnRIndexUp += this.DrawingUp;

        if (this.drawingPrefab == null)
        {
            throw new System.Exception("drawing prefab is null");
        }

        //set pen
        this.pen = this.gameManager.vrManager.rightController.gameObject.GetComponentInChildren<MagicPen>(true);

        if (this.pen == null)
        {
            throw new System.Exception("pen is null");
        }

        //set line shader parameters
        Material material = this.drawingPrefab.GetComponent<LineRenderer>().sharedMaterial;

        if (material.GetFloat("_MaxX") <= 0.001f)
        {
            material.SetFloat("_MaxX", this.dataPointsManager.maxX);
            material.SetFloat("_MinX", this.dataPointsManager.minX);
            material.SetFloat("_MaxY", this.dataPointsManager.maxY);
            material.SetFloat("_MinY", this.dataPointsManager.minY);
            material.SetFloat("_MaxZ", this.dataPointsManager.maxZ);
            material.SetFloat("_MinZ", this.dataPointsManager.minZ);
        }

        

    }

    // Update is called once per frame
    void Update () {

        if (this.isDrawing && this.isTurnOn)
        {
            if (this.currentDrawing == null)
            {
                var newGameObject = GameObject.Instantiate(this.drawingPrefab, 
                    this.gameManager.vrManager.rightController.gameObject.transform.position, 
                    this.gameManager.vrManager.rightController.gameObject.transform.rotation, 
                    this.dataPointsManager.transform);
                this.currentDrawing = newGameObject.GetComponent<LineRenderer>();
                this.currentDrawing.positionCount = 2;
                this.currentDrawing.SetPosition(0, this.currentDrawing.transform.InverseTransformPoint(this.pen.gameObject.transform.position));
                this.drawingList.Add(this.currentDrawing);
                
            }


            this.currentDrawing.SetPosition(this.currentDrawing.positionCount -1, this.currentDrawing.transform.InverseTransformPoint(this.pen.gameObject.transform.position));
            var lastPoint = this.currentDrawing.GetPosition(this.currentDrawing.positionCount - 2);
            var currentPoint = this.currentDrawing.GetPosition(this.currentDrawing.positionCount - 1);

            if (Vector3.Distance(currentPoint, lastPoint) >= this.linePointGap)
            {
                this.currentDrawing.positionCount = this.currentDrawing.positionCount + 1;
                this.currentDrawing.SetPosition(this.currentDrawing.positionCount - 1, this.currentDrawing.transform.InverseTransformPoint(this.pen.gameObject.transform.position));
            }

        }

    }

    //helper
    private void CreateLinerCollider()
    {

    }

    public void DrawingDown()
    {

        if (this.dataField.isRightControllerActivated)
        {
            this.isDrawing = true;
        }


    }

    public void DrawingUp()
    {

        this.EndDrawing();


    }

    public void EndDrawing()
    {
        this.isDrawing = false;
        this.currentDrawing = null;
    }

    public void TurnOn()
    {
        this.isTurnOn = true;
    }

    public void TurnOff()
    {
        this.isTurnOn = false;

        foreach (var drawingline in this.drawingList)
        {
            if (drawingline != null)
            {
                Destroy(drawingline.gameObject);

            }
            
        }

        this.drawingList.Clear();

    }

}
