﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatafieldRay : MonoBehaviour {

    public Vector3 rayCasterOffset = Vector3.zero;
    private Vector3 originalRayCastingOffset;
    public float rayWidth = 0.01f;
    private float originalRayWidth;
    public LayerMask dataPointRayCastingLayer;//data point layer
    private LayerMask originalRayCastingLayer;
    private bool isRayCasterOn = false;
    private GameManager gameManager = GameManager.Shared;
    private VRInput vrInput;
    private VRRayCaster raycaster;
    private bool isIndexFingerTouched = false;

    private void OnDestroy()
    {

        this.vrInput.OnRIndexTouched -= this.HandleIndexTouched;


    }

    // Use this for initialization
    void Start () {

        if (this.dataPointRayCastingLayer == 0)
        {
            throw new System.Exception("datapoint layer is null");
        }


        this.vrInput = this.gameManager.vrInput;


        if (this.vrInput == null)
        {
            throw new System.Exception("vrinput is null");
        }

        this.vrInput.OnRIndexTouched += this.HandleIndexTouched;
        this.raycaster = this.gameManager.vrManager.rightController.vrRayCaster;

        if (this.raycaster == null)
        {
            throw new System.Exception("raycaster is null");
        }

        this.originalRayCastingLayer = this.raycaster.rayCastingLayer;
        this.originalRayCastingOffset = this.raycaster.rayCasterOffset;
        this.originalRayWidth = this.raycaster.rayWidth;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //raycaster
    public void TurnOnRayCaster()
    {
        this.isRayCasterOn = true;
    }

    public void TurnOffRayCaster()
    {
        this.isRayCasterOn = false;
    }

    private void HandleIndexTouched(bool isTouched)
    {
        if (!isTouched && this.isRayCasterOn)
        {
            this.isIndexFingerTouched = true;

            StartCoroutine(this.PlayRay());

        }
        else
        {
            this.isIndexFingerTouched = false;

            this.raycaster.shouldDrawRay = false;
            this.raycaster.isRayCasterOn = false;
            this.raycaster.HideCasterRay();
            this.raycaster.rayCastingLayer = this.originalRayCastingLayer;
            this.raycaster.rayCasterOffset = this.originalRayCastingOffset;
            this.raycaster.rayWidth = this.originalRayWidth;
        }
    }

    private IEnumerator PlayRay()
    {
        yield return new WaitForSeconds(0.1f);

        if (this.isIndexFingerTouched && this.isRayCasterOn)
        {
            this.raycaster.shouldDrawRay = true;
            this.raycaster.isRayCasterOn = true;
            this.raycaster.rayCastingLayer = this.dataPointRayCastingLayer;
            this.raycaster.rayCasterOffset = this.rayCasterOffset;
            this.raycaster.rayWidth = this.rayWidth;

        }
    }

}
