﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatafieldGrab : MonoBehaviour
{
    public Datafield dataField;
    public GameObject fullArea;
    public DataPointsManager dataPointsManager;
    public float movingRatio = 4;
    public DatafieldResize datafieldResize;
    public bool shouldRestrictData = true;
    public bool shouldRestrictSpaceship = true;
    public GameObject spaceship;

    private GameManager gameManager = GameManager.Shared;
    private VRInput vrInput;
    private bool isGrabbed = false;
    private Vector3? previousControllerPosition;
    private VRController currentVRController; //this variable maybe not necessary

    // Use this for initialization
    private void OnDestroy()
    {
        vrInput.OnLIndexDown -= this.HandleIndexDown;
        vrInput.OnLIndexUp -= this.HandleIndexUp;

    }

    // Use this for initialization
    void Start()
    {
        this.vrInput = this.gameManager.vrInput;

        if (this.dataPointsManager == null)
        {
            throw new System.Exception("datapointsmanager is null");
        }

        if (this.vrInput == null)
        {
            throw new System.Exception("vrinput is null");
        }

        this.vrInput.OnLIndexDown += this.HandleIndexDown;
        this.vrInput.OnLIndexUp += this.HandleIndexUp;


    }

    void Update()
    {
        if (this.isGrabbed /*&& !this.datafieldResize.isResizerOn*/ && !this.dataPointsManager.AreDataPointsMoving())
        {
            if (this.previousControllerPosition == null)
            {
                this.previousControllerPosition = this.currentVRController.gameObject.transform.position;
            }
            else
            {
                var deltaPosition = this.currentVRController.gameObject.transform.position - this.previousControllerPosition.Value;
                this.fullArea.transform.position = this.fullArea.transform.position + this.movingRatio *deltaPosition;
                this.previousControllerPosition = this.currentVRController.gameObject.transform.position;

                if (this.shouldRestrictData)
                {
                    if (!this.shouldRestrictSpaceship)
                    {
                        this.fullArea.transform.position = this.RestrictedObjectInsideField(this.fullArea.GetComponent<MeshRenderer>().bounds, this.dataField.GetComponent<MeshRenderer>().bounds);
                    }
                    else if (this.shouldRestrictSpaceship && this.spaceship != null)
                    {
                        var originalFullAreaPosition = this.fullArea.transform.position;
                        var originalSpaceshipPosition = this.spaceship.transform.position;
                        var restrictedSpaceshipPosition = this.RestrictedObjectInsideField(this.spaceship.GetComponent<MeshRenderer>().bounds, this.dataField.GetComponent<MeshRenderer>().bounds);
                        var deltaPos = restrictedSpaceshipPosition - originalSpaceshipPosition;

                        //this.spaceship.transform.position = restrictedSpaceshipPosition;
                        this.fullArea.transform.position = originalFullAreaPosition + deltaPos;

                    }

                    
                }

            }

        }
    }

    public void HandleIndexDown()
    {

        if (this.currentVRController == null && this.dataField.isLeftControllerActivated)
        {
            this.currentVRController = this.gameManager.vrManager.leftController;
            this.isGrabbed = true;
        }

        
    }

    public void HandleIndexUp()
    {

        this.EndGrab();


    }

    public void EndGrab()
    {
        this.isGrabbed = false;
        this.currentVRController = null;
        this.previousControllerPosition = null;
    }

    //helper
    Vector3 RestrictedObjectInsideField(Bounds restrictedObjectBounds,Bounds fieldBounds)
    {
        Vector3 newCenter = restrictedObjectBounds.center;

        Bounds validAreaBounds = new Bounds(fieldBounds.center, fieldBounds.size - restrictedObjectBounds.size);

        if ((fieldBounds.center.x - restrictedObjectBounds.center.x) > validAreaBounds.size.x/2)
        {
            newCenter.x = fieldBounds.center.x - validAreaBounds.size.x / 2;
        }
        else if ((fieldBounds.center.x - restrictedObjectBounds.center.x) < -validAreaBounds.size.x / 2)
        {
            newCenter.x = fieldBounds.center.x + validAreaBounds.size.x / 2;
        }

        if ((fieldBounds.center.y - restrictedObjectBounds.center.y) > validAreaBounds.size.y / 2)
        {
            newCenter.y = fieldBounds.center.y - validAreaBounds.size.y / 2;
        }
        else if ((fieldBounds.center.y - restrictedObjectBounds.center.y) < -validAreaBounds.size.y / 2)
        {
            newCenter.y = fieldBounds.center.y + validAreaBounds.size.y / 2;
        }

        if ((fieldBounds.center.z - restrictedObjectBounds.center.z) > validAreaBounds.size.z / 2)
        {
            newCenter.z = fieldBounds.center.z - validAreaBounds.size.z / 2;
        }
        else if ((fieldBounds.center.z - restrictedObjectBounds.center.z) < -validAreaBounds.size.z / 2)
        {
            newCenter.z = fieldBounds.center.z + validAreaBounds.size.z / 2;
        }


        return newCenter;

    }

}
