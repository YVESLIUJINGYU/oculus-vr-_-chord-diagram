﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DataPointsManager : MonoBehaviour {

    public Datafield dataField;

    private List<DataPoint> dataPointsList;
    [HideInInspector]
    public float maxX = 0;
    [HideInInspector]
    public float minX = 0;
    [HideInInspector]
    public float maxY = 0;
    [HideInInspector]
    public float minY = 0;
    [HideInInspector]
    public float maxZ = 0;
    [HideInInspector]
    public float minZ = 0;

    private void Awake()
    {

        if (this.dataField == null)
        {
            throw new System.Exception("datafield is null");
        }


        //datafield size
        var size = this.dataField.GetComponent<MeshRenderer>().bounds.size;
        var centerPoint = this.dataField.gameObject.transform.position;

        this.maxX = centerPoint.x + size.x / 2;
        this.minX = centerPoint.x - size.x / 2;

        this.maxY = centerPoint.y + size.y / 2;
        this.minY = centerPoint.y - size.y / 2;

        this.maxZ = centerPoint.z + size.z / 2;
        this.minZ = centerPoint.z - size.z / 2;


    }

    // Use this for initialization
    void Start () {

        

        this.dataPointsList = this.gameObject.GetComponentsInChildren<DataPoint>().ToList();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void MovePoints()
    {
        if (this.dataField.isActivated)
        {
            foreach (var dataPoint in this.dataPointsList)
            {
                dataPoint.RandomMove();
            }
        }

    }

    public void DestoryPoints()
    {
        if (this.dataField.isActivated)
        {
            foreach (var dataPoint in this.dataPointsList)
            {
                dataPoint.DestoryAllLines();
            }
        }

    }

    public bool AreDataPointsMoving()
    {
        foreach (var dataPoint in this.dataPointsList)
        {
            return dataPoint.isMoving;
        }

        return false;
    }

    public void FadeOutAllDataInfo(UIFlotingInformatiion uiInfo)
    {
        foreach (var dataPoint in this.dataPointsList)
        {
            var ui = dataPoint.GetComponentInChildren<UIFlotingInformatiion>();

            if (ui!= null && ui != uiInfo)
            {
                ui.Fadeout();
            }

        }
    }

    public void FadeOutAllDataInfo()
    {
        foreach (var dataPoint in this.dataPointsList)
        {
            var ui = dataPoint.GetComponentInChildren<UIFlotingInformatiion>();

            if (ui != null)
            {
                ui.Fadeout();
            }

        }
    }
}
