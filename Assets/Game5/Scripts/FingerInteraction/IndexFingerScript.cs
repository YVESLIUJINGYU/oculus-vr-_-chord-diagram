﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndexFingerScript : MonoBehaviour {

    [SerializeField]
    GameObject fingerTipPrefab;

    [SerializeField]
    Vector3 offset = new Vector3(-0.037f, -0.0011f, 0.067f);
    [SerializeField]
    float size = 0.02f;
    [SerializeField]
    VRControllerTypes controllerType = VRControllerTypes.Left;

    private GameManager gameManager = GameManager.Shared;
    private GameObject fingerTip;
    private VRInput vrInput;

    private void OnDestroy()
    {

        if (this.controllerType == VRControllerTypes.Right)
        {
            vrInput.OnRIndexTouched -= this.HandleIndexTouched;
        }
        else
        {
            vrInput.OnLIndexTouched -= this.HandleIndexTouched;
        }
        

    }

    // Use this for initialization
    void Start () {

        this.vrInput = this.gameManager.vrInput;


        if (this.vrInput == null)
        {
            throw new System.Exception("vrinput is null");
        }

        
        if (this.controllerType == VRControllerTypes.Right)
        {
            this.vrInput.OnRIndexTouched += this.HandleIndexTouched;
        }
        else
        {
            this.vrInput.OnLIndexTouched += this.HandleIndexTouched;
        }



        if (this.controllerType == VRControllerTypes.Right)
        {
            this.fingerTip = GameObject.Instantiate(this.fingerTipPrefab, this.gameManager.vrManager.rightController.gameObject.transform);
        }
        else
        {
            this.fingerTip = GameObject.Instantiate(this.fingerTipPrefab, this.gameManager.vrManager.leftController.gameObject.transform);
        }

        this.fingerTip.transform.localPosition = this.offset;
        this.fingerTip.transform.localScale = this.size * Vector3.one;
        this.fingerTip.SetActive(false);


	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void HandleIndexTouched(bool isTouched)
    {
        if (isTouched)
        {
            this.fingerTip.SetActive(false);

        }
        else
        {
            this.fingerTip.SetActive(true);
        }
    }
}
