﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FingerButton : MonoBehaviour {

    protected enum FingerButtonStatus
    {
        Unpushed,
        Pushed,
        Pushing
    }
    public UnityEvent pushDownEvent;
    public UnityEvent pushUpEvent;
    public float pushDistance = 0.01f;
    public float pushTime = 0.2f;
    public SpriteRenderer buttonIconSpriterenderer;
    public Sprite unpushedSprite;
    public Sprite pushedSprite;
    public SpriteRenderer waveSpriterenderer;
    [SerializeField]
    private bool isPushable = true;

    private GameManager gameManager = GameManager.Shared;
    private Vector3 originalLocalPosition;
    private FingerButtonStatus currentStatus = FingerButtonStatus.Unpushed;
    private Vector3 originalWaveScale;

    // Use this for initialization
    void Start () {

        if (this.buttonIconSpriterenderer == null)
        {
            this.buttonIconSpriterenderer = this.gameObject.GetComponentInChildren<SpriteRenderer>();
        }

        if (this.buttonIconSpriterenderer == null)
        {
            throw new System.Exception("icon sprite renderer is null");
        }

        if (this.pushedSprite == null)
        {
            throw new System.Exception("pushed Sprite is null");
        }

        if (this.unpushedSprite == null)
        {
            throw new System.Exception("unpushed Sprite is null");
        }

        this.buttonIconSpriterenderer.sprite = this.unpushedSprite;

        //set wave
        Color color = this.waveSpriterenderer.color;
        color.a = 0;
        this.waveSpriterenderer.color = color;
        this.originalWaveScale = this.waveSpriterenderer.transform.localScale;

        this.originalLocalPosition = this.gameObject.transform.localPosition;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<LeftFinger>() != null)
        {
            if (this.ValidPush(other))
            {
                if (this.currentStatus == FingerButtonStatus.Unpushed)
                {
                    this.currentStatus = FingerButtonStatus.Pushing;

                    this.gameManager.haptic.HapticPaulse(this.gameManager.vrManager.leftController, 40, 30);
                    StartCoroutine(this.PushDownButton());
                    this.buttonIconSpriterenderer.sprite = this.pushedSprite;
                    StartCoroutine(this.PushWave());

                }

                if (this.currentStatus == FingerButtonStatus.Pushed && this.isPushable)
                {
                    this.currentStatus = FingerButtonStatus.Pushing;

                    this.gameManager.haptic.HapticPaulse(this.gameManager.vrManager.leftController, 40, 30);
                    StartCoroutine(this.PushUpButton());
                    this.buttonIconSpriterenderer.sprite = this.unpushedSprite;
                    StartCoroutine(this.PushWave());

                }
            }

            

        }
    }

    //helper
    /// <summary>
    /// check if push is from upward
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    private bool ValidPush(Collider other)
    {
        var hitPosition = other.ClosestPointOnBounds(this.gameObject.transform.position);

        RaycastHit hit;
        Ray raycast = new Ray(other.gameObject.transform.position, this.gameObject.transform.position - other.gameObject.transform.position);
        if (Physics.Raycast(raycast, out hit, 100f))
        {
            if (this.transform.up == hit.normal)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        else
        {
            return false;
        }



    }
    //animation
    private IEnumerator PushWave()
    {
        float time = 0;

        while (time <= this.pushTime)
        {
            var newScale = Vector3.Lerp(this.originalWaveScale * 0.0f, this.originalWaveScale, time / this.pushTime);
            this.waveSpriterenderer.transform.localScale = newScale;

            var newAlpha = Mathf.Lerp(1, 0, time / this.pushTime);
            Color newcolor = this.waveSpriterenderer.color;
            newcolor.a = newAlpha;
            this.waveSpriterenderer.color = newcolor;

            time = time + Time.deltaTime;
            yield return null;

        }

        this.waveSpriterenderer.transform.localScale = this.originalWaveScale;
        Color color = this.waveSpriterenderer.color;
        color.a = 0;
        this.waveSpriterenderer.color = color;
    }


    private IEnumerator PushDownButton()
    {
        float time = 0;
        Vector3 startPosition = this.originalLocalPosition;
        Vector3 endPosition = this.originalLocalPosition - new Vector3(0, this.pushDistance / this.transform.lossyScale.y, 0);

        while (time <= this.pushTime)
        {
            var newPosition = Vector3.Lerp(startPosition, endPosition, time / this.pushTime);
            this.gameObject.transform.localPosition = newPosition;
            time = time + Time.deltaTime;
            yield return null;

        }

        this.gameObject.transform.localPosition = endPosition;
        this.currentStatus = FingerButtonStatus.Pushed;

        if (this.pushDownEvent!=null)
        {
            this.pushDownEvent.Invoke();
        }

        if (!this.isPushable)
        {
            this.buttonIconSpriterenderer.sprite = this.unpushedSprite;
            yield return this.PushUpButton();
        }

    }

    

    
    private IEnumerator PushUpButton()
    {
        float time = 0;
        Vector3 startPosition = this.originalLocalPosition - new Vector3(0, this.pushDistance / this.transform.lossyScale.y, 0) ;
        Vector3 endPosition = this.originalLocalPosition;

        while (time <= this.pushTime)
        {
            var newPosition = Vector3.Lerp(startPosition, endPosition, time / this.pushTime);
            this.gameObject.transform.localPosition = newPosition;
            time = time + Time.deltaTime;
            yield return null;

        }

        this.gameObject.transform.localPosition = endPosition;
        this.currentStatus = FingerButtonStatus.Unpushed;

        if (this.pushUpEvent != null)
        {
            this.pushUpEvent.Invoke();
        }

    }

}
