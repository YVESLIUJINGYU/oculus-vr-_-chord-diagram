﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//refrence: https://forum.unity.com/threads/evenly-distributed-points-on-a-surface-of-a-sphere.26138/

public class DataDistribution : MonoBehaviour {

    public GameObject dataPrefab;
    public float initalAnimationTime = 2;

    [HideInInspector]
    public List<GameObject> uspheres = new List<GameObject>();

    void Start()
    {
        /*
        float scaling = 13;
        Vector3[] pts = PointsOnSphere(1024);
        int i = 0;

        foreach (Vector3 value in pts)
        {
            uspheres.Add(GameObject.Instantiate(this.dataPrefab));
            uspheres[i].transform.parent = transform;
            uspheres[i].transform.localPosition = value * scaling;
            uspheres[i].transform.parent = null;
            i++;
        }
        */

        StartCoroutine(this.GenerateData());
    }

    Vector3[] PointsOnSphere(int n)
    {
        List<Vector3> upts = new List<Vector3>();
        float inc = Mathf.PI * (3 - Mathf.Sqrt(5));
        float off = 2.0f / n;
        float x = 0;
        float y = 0;
        float z = 0;
        float r = 0;
        float phi = 0;

        for (var k = 0; k < n; k++)
        {
            y = k * off - 1 + (off / 2);
            r = Mathf.Sqrt(1 - y * y);
            phi = k * inc;
            x = Mathf.Cos(phi) * r;
            z = Mathf.Sin(phi) * r;

            upts.Add(new Vector3(x, y, z));
        }
        Vector3[] pts = upts.ToArray();
        return pts;
    }

    //animation
    IEnumerator GenerateData()
    {
        float scaling = 13;
        Vector3[] pts = PointsOnSphere(1024);

        for (int i = 0; i < pts.Length; i++)
        {
            var value = pts[pts.Length - i - 1];

            uspheres.Add(GameObject.Instantiate(this.dataPrefab));
            uspheres[i].transform.parent = transform;
            //uspheres[i].transform.localPosition = value * scaling;
            //uspheres[i].transform.parent = null;
            StartCoroutine(this.InitDataPosition(uspheres[i], Vector3.zero, value * scaling));
            yield return new WaitForSeconds(0.04f);
        }
    }

    IEnumerator InitDataPosition(GameObject dataObject,Vector3 initalPosition,Vector3 finalPosition) {

        float time = 0f;

        while (time <= this.initalAnimationTime)
        {
            var newPosition = Vector3.Lerp(initalPosition, finalPosition, Mathf.SmoothStep(0.0f,1.0f, time / initalAnimationTime));
            dataObject.transform.localPosition = newPosition;

            time = time + Time.deltaTime;
            yield return null;

        }

        dataObject.transform.localPosition = finalPosition;
        dataObject.transform.parent = null;
    }
}
