﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptainAnimationController : MonoBehaviour {

    public GameObject lookAtGameObject;

    protected Animator animator;

    // Use this for initialization
    void Start () {

        animator = GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnAnimatorIK()
    {
        if (this.animator != null && this.lookAtGameObject != null)
        {

            //if the IK is active, set the position and rotation directly to the goal. 
            animator.SetLookAtWeight(1,0,1,0,0.8f);
            animator.SetLookAtPosition(this.lookAtGameObject.transform.position);

        }
    }

}
